const DOMAIN = 'http://localhost:3000'
const COMMON_API_PATH = '/api';
const USER_API_PATH = '/DlctUsers';
const CITY_API_PATH = '/Cities';
const DISTRICT_API_PATH = '/Districts';
const SALON_API_PATH = '/Salons';
const REVIEW_API_PATH = '/Reviews';
const SERVICE_API_PATH = '/SalonServices';
const SCHEDULE_API_PATH = '/Schedules';
const BOOKING_API_PATH = '/Bookings';
const BOOKING_SERVICE_API_PATH = '/BookingServices';
const CUSTOMER_API_PATH = '/Customers';
const UNAVAILABLE_SLOTS_API_PATH = '/Unavailable-slots';
const IMAGE_API_PATH = '/Images'
const AUTHENTICATE_API_PATH = '/authenticate';
const ADMIN_AUTHENTICATE_API_PATH = '/admin-authenticate';
const BASE_API_URL = 'http://localhost:3000/api';
const SEARCH_SALONS_PATH = '/Salons/search';

// City APIs
export const getAllCities = `${DOMAIN}${COMMON_API_PATH}${CITY_API_PATH}?size=100000`;
export const getCitiesHasSalon = `${DOMAIN}${COMMON_API_PATH}${CITY_API_PATH}?hasSalon.equals=1`;
export const getCityById = cityId => `${DOMAIN}${COMMON_API_PATH}${CITY_API_PATH}?filter[where][id]=${cityId}`;

// District APIs
export const getAllDistrictsByCityId = cityId => `${DOMAIN}${COMMON_API_PATH}${DISTRICT_API_PATH}?cityId.equals=${cityId}&size=100000`;
export const getDistrictsHasSalonByCityId = cityId => `${DOMAIN}${COMMON_API_PATH}${DISTRICT_API_PATH}?cityId.equals=${cityId}&hasSalon.equals=1`;
export const getDistrictByDistrictId = districtId => `${DOMAIN}${COMMON_API_PATH}${DISTRICT_API_PATH}?filter[where][id]=${districtId}`;

// Salon APIs
export const getSalonsByCityIdAndDistrictId = (districtId, cityId) => `${DOMAIN}${COMMON_API_PATH}${SALON_API_PATH}?districtId.equals=${districtId}&cityId.equals=${cityId}&status.equals=1`;
export const getAllSalons = `${DOMAIN}${COMMON_API_PATH}${SALON_API_PATH}`;
export const requestToBeSalon = `${DOMAIN}${COMMON_API_PATH}${SALON_API_PATH}`;
export const getSalonById = salonId => `${DOMAIN}${COMMON_API_PATH}${SALON_API_PATH}?id.equals=${salonId}`;
export const getSalonByPhoneNumber = phoneNumber => `${DOMAIN}${COMMON_API_PATH}${SALON_API_PATH}?phoneNumber.equals=${phoneNumber}`;
export const getSalonByNormalizedName = normalizedName => `${DOMAIN}${COMMON_API_PATH}${SALON_API_PATH}?filter[include][district]&filter[where][normalizedName]=${normalizedName}`;
export const authenticate = `${DOMAIN}${COMMON_API_PATH}${AUTHENTICATE_API_PATH}`;

// Service APIs
export const getServicesBySalonId = salonId => `${DOMAIN}${COMMON_API_PATH}${SERVICE_API_PATH}?filter[include][service]&filter[where][salonId]=${salonId}&filter[where][status]=1`;

//Images APIs
export const uploadImage = `${DOMAIN}${COMMON_API_PATH}${IMAGE_API_PATH}/upload`;

// User APIs
export const registerNewUser = `${DOMAIN}${COMMON_API_PATH}${USER_API_PATH}`;
export const loginUser = `${DOMAIN}${COMMON_API_PATH}${USER_API_PATH}/login`;
export const getUserById = (id, token) => `${DOMAIN}${COMMON_API_PATH}${USER_API_PATH}/${id}?access_token=${token}`;
export const updateUser = (token) => `${DOMAIN}${COMMON_API_PATH}${USER_API_PATH}?access_token=${token}`;

// Review APIs
export const getReviewsBySalonId = salonId => `${DOMAIN}${COMMON_API_PATH}${REVIEW_API_PATH}?filter[include][dlctUser]&filter[where][salonId]=${salonId}&filter[where][status][gt]=0`;
export const createNewReview = `${DOMAIN}${COMMON_API_PATH}${REVIEW_API_PATH}`;

// Schedule APIs
export const getSchedulesBySalonIdAndWeekDay = (salonId, weekDay) => `${DOMAIN}${COMMON_API_PATH}${SCHEDULE_API_PATH}?filter[where][salonId]=${salonId}&filter[where][weekDay]=${weekDay}`;
export const getSchedulesBySalonId = (salonId) => `${DOMAIN}${COMMON_API_PATH}${SCHEDULE_API_PATH}?salonId.equals=${salonId}`;
export const createSchedule = `${DOMAIN}${COMMON_API_PATH}${SCHEDULE_API_PATH}`;

// Booking APIs
export const getBookingsBySalonId = (salonId, dateStart, dateEnd) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?salonId.equals=${salonId}&time.greaterOrEqualThan=${dateStart}&time.lessOrEqualThan=${dateEnd}&sort=time,asc`;
export const getAllBookingsByCustomerId = (customerId) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?filter[include][salon]&filter[where][dlctUserId]=${customerId}`;
export const getOpenBookingsByCustomerId = (customerId) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?customerId.equals=${customerId}&status.equals=0`;
// export const getAllBookingsByCustomerId = (customerId) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?customerId.equals=${customerId}`;
export const getOpenBookingsBySalonIdAndDate = (salonId, dateStart, dateEnd) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?salonId.equals=${salonId}&status.equals=0&time.greaterOrEqualThan=${dateStart}&time.lessOrEqualThan=${dateEnd}`;
export const getAllBookingsBySalonIdAndSlotTime = (salonId, slotTime) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?salonId.equals=${salonId}&time.equals=${slotTime}`;
export const getAllBookingsSpecifiedByCustomerAndSalonId = (salonId) => `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}?customerId.specified&salonId.equals=${salonId}`;
export const createBooking = `${DOMAIN}${COMMON_API_PATH}${BOOKING_API_PATH}`;

export const createBookingService = `${DOMAIN}${COMMON_API_PATH}${BOOKING_SERVICE_API_PATH}`;
// customer management
export const createAndSendPinCode = `${DOMAIN}${COMMON_API_PATH}/otp/pin-code`
export const verifyPinCode = (phone, pin_code) => `${DOMAIN}${COMMON_API_PATH}/otp/verify?phoneNumber=${phone}&pinCode=${pin_code}`
export const createCustomer = `${DOMAIN}${COMMON_API_PATH}${CUSTOMER_API_PATH}`
export const getCustomerByPhoneNumber = (phoneNumber) => `${DOMAIN}${COMMON_API_PATH}${CUSTOMER_API_PATH}?phoneNumber.equals=${phoneNumber}`

//unavailable slots management
export const getUnavailableSlotsBySalonId = (salonId) => `${DOMAIN}${COMMON_API_PATH}${UNAVAILABLE_SLOTS_API_PATH}?salonId.equals=${salonId}`;
export const getUnavailableSlotBySalonIdAndSlotTime = (salonId, slotTime) => `${DOMAIN}${COMMON_API_PATH}${UNAVAILABLE_SLOTS_API_PATH}?salonId.equals=${salonId}&time.equals=${slotTime}`;
export const createUnavailableSlot = `${DOMAIN}${COMMON_API_PATH}${UNAVAILABLE_SLOTS_API_PATH}`;
export const deleteUnavailableSlot = (id) => `${DOMAIN}${COMMON_API_PATH}${UNAVAILABLE_SLOTS_API_PATH}/${id}`;

//admin api
export const adminAuthenticate = `${DOMAIN}${COMMON_API_PATH}${ADMIN_AUTHENTICATE_API_PATH}`;
export const SEARCH_SALONS_URL = `${BASE_API_URL}${SEARCH_SALONS_PATH}`;