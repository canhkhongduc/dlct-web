import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import SideNavBar from '../layout/SideNavBar';
import { ToastContainer, toast } from 'mdbreact';
import {getSalonByPhoneNumber} from '../../actions/salon';
import {getSchedulesBySalonIdAndWeekDay, createSchedule, updateSchedule} from '../../actions/schedule';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const weekDays = [
    "Thứ Hai",
    "Thứ Ba",
    "Thứ Tư",
    "Thứ Năm",
    "Thứ Sáu",
    "Thứ Bảy",
    "Chủ Nhật"
];
const slotDurations = [15, 30, 45, 60];
const slotCapacities = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const morningHours = [
    "07:00",
    "07:30",
    "08:00",
    "08:30",
    "09:00",
    "09:30",
    "10:00",
    "10:30",
    "11:00",
    "11:30",
    "12:00"
];
const afternoonHours = [
    "12:30",
    "13:00",
    "13:30",
    "14:00",
    "14:30",
    "15:00",
    "15:30",
    "16:00",
    "16:30",
    "17:00",
    "17:30",
    "18:00",
    "18:30",
    "19:00",
    "19:30",
    "20:00",
    "20:30",
    "21:00"
];

class SalonSetting extends Component {
    constructor(props) {
        super(props);
        this.sideNavId = 1;
        this.state = {
            sideNavId: 1,
            schedules: [],
            scheduleEmpty: true,

        };

    }

    componentWillMount = async(e) => {
        let phoneNumber = cookies.get('phoneNumber');
        let role = cookies.get('role');
        let token = cookies.get('token');
        if(!token || role !== "salon"){
            window.location.href = `/salon/login`;
        } else {
            await this.props.handleFetchSalonByPhoneNumber(phoneNumber);
            await this.props.handleFetchSchedulesBySalonId(this.props.salonReducer.salon[0].id);
            if(this.props.scheduleReducer.schedules[0].length !== 0){
                this.setState({
                    schedules: this.props.scheduleReducer.schedules,
                    scheduleEmpty: false
                });
                let schedules = this.state.schedules[0];
                for(let i = 0; i < schedules.length; i++){
                    document.getElementById(`morning-start-${schedules[i].weekDay - 2}`).value = schedules[i].startAM;
                    document.getElementById(`morning-finish-${schedules[i].weekDay - 2}`).value = schedules[i].finishAM;
                    document.getElementById(`afternoon-start-${schedules[i].weekDay - 2}`).value = schedules[i].startPM;
                    document.getElementById(`afternoon-finish-${schedules[i].weekDay - 2}`).value = schedules[i].finishPM;
                    document.getElementById(`duration-${schedules[i].weekDay - 2}`).value = schedules[i].slotDuration;
                    document.getElementById(`capacity-${schedules[i].weekDay - 2}`).value = schedules[i].slotCapacity;
                    document.getElementById(`cb-working-status-${schedules[i].weekDay - 2}`).checked = !schedules[i].workingStatus;
                }
            } else {
                for(let i = 0; i < 7; i++){
                    document.getElementById(`morning-start-${i}`).value = "07:00";
                    document.getElementById(`morning-finish-${i}`).value = "12:00";
                    document.getElementById(`afternoon-start-${i}`).value = "13:00";
                    document.getElementById(`afternoon-finish-${i}`).value = "18:00";
                    document.getElementById(`duration-${i}`).value = 30;
                    document.getElementById(`capacity-${i}`).value = 1;
                    document.getElementById(`cb-working-status-${i}`).checked = false;
                }
            } 
        }
    }
   
    displayHours = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <option value={element}>
                    {element}
                </option>
            ))
        }
        return result;
    }

    displayCapacities = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <option value={element}>
                    {element} người
                </option>
            ))
        }
        return result;
    }

    displayDurations = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <option value={element}>
                    {element} phút
                </option>
            ))
        }
        return result;
    }
    handleSaveSchedules = async(event) => {
        event.preventDefault();
        let schedules = [];
        for(let i = 0 ; i < 7; i++) {
            let morningStartId = "morning-start-" + i; 
            let morningFinishId = "morning-finish-" + i; 
            let afternoonStartId = "afternoon-start-" + i; 
            let afternoonFinishId = "afternoon-finish-" + i; 
            let slotDuration = "duration-" + i; 
            let slotCapacity = "capacity-" + i; 
            let workingStatus = "cb-working-status-" + i;
            schedules[i] = {
                salonId: this.props.salonReducer.salon[0].id,
                startAM: event.target[morningStartId].value,
                startPM: event.target[afternoonStartId].value,
                finishAM: event.target[morningFinishId].value,
                finishPM: event.target[afternoonFinishId].value,
                slotDuration: event.target[slotDuration].value,
                slotCapacity: event.target[slotCapacity].value,
                weekDay: i+2,
                workingStatus: !event.target[workingStatus].checked,
            }
            
        }
        for(let i = 0; i< schedules.length; i++) {
            if(!this.state.scheduleEmpty) {
                schedules[i].id = this.state.schedules[0][i].id;
                await this.props.handleUpdateSchedule(schedules[i]);
            } else {
                await this.props.handleCreateSchedule(schedules[i]);
            }
        }
        if(this.props.scheduleReducer.schedule.id) {
            toast.success("Lưu lịch làm việc thành công.");
        } else {
            toast.error("Lưu thất bại. Vui lòng kiểm tra kết nối mạng.");
        }
        
    }

    handleOnChange = async(time, action, weekDay) => {
        if(time === "morning") {
            if(action === "start"){
                let morningStart = document.getElementById(`morning-start-${weekDay}`).value;
                let options = document.getElementById(`morning-finish-${weekDay}`).options;
                for(let i = 0; i <= morningHours.indexOf(morningStart); i++){
                    options[i].disabled = true;
                }
                for(let i = morningHours.indexOf(morningStart) + 1; i < morningHours.length; i++){
                    options[i].disabled = false;
                }
            } else {
                let morningFinish = document.getElementById(`morning-finish-${weekDay}`).value;
                let options = document.getElementById(`morning-start-${weekDay}`).options;
                for(let i = morningHours.indexOf(morningFinish); i < morningHours.length; i++){
                    options[i].disabled = true;
                }
                for(let i = 0; i < morningHours.indexOf(morningFinish); i++){
                    options[i].disabled = false;
                }
            }
        } else {
            if(action === "start"){
                let afternoonStart = document.getElementById(`afternoon-start-${weekDay}`).value;
                let options = document.getElementById(`afternoon-finish-${weekDay}`).options;
                for(let i = 0; i <= afternoonHours.indexOf(afternoonStart); i++){
                    options[i].disabled = true;
                }
                for(let i = afternoonHours.indexOf(afternoonStart) + 1; i < afternoonHours.length; i++){
                    options[i].disabled = false;
                }
            } else {
                let afternoonFinish = document.getElementById(`afternoon-finish-${weekDay}`).value;
                let options = document.getElementById(`afternoon-start-${weekDay}`).options;
                for(let i = afternoonHours.indexOf(afternoonFinish); i < afternoonHours.length; i++){
                    options[i].disabled = true;
                }
                for(let i = 0; i < afternoonHours.indexOf(afternoonFinish); i++){
                    options[i].disabled = false;
                }
            }
            
        }
    }

    displayWeekDays = arr => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <tr>
                    <th scope="row">{element}</th>
                    <td>
                        <select id={`morning-start-${index}`} className="setting-dropdown" onChange={() =>this.handleOnChange("morning", "start", index)}>
                            {this.displayHours(morningHours)}
                        </select>
                    </td>
                    <td>
                        <select id={`morning-finish-${index}`} className="setting-dropdown" onChange={() =>this.handleOnChange("morning", "finish", index)}>
                            {this.displayHours(morningHours)}
                        </select>
                    </td>
                    <td>
                        <select id={`afternoon-start-${index}`} className="setting-dropdown" onChange={() =>this.handleOnChange("afternoon", "start" , index)}>
                            {this.displayHours(afternoonHours)}
                        </select>
                    </td>
                    <td>
                        <select id={`afternoon-finish-${index}`} className="setting-dropdown" onChange={() =>this.handleOnChange("afternoon", "finish", index)}>
                            {this.displayHours(afternoonHours)}
                        </select>
                    </td>
                    <td>
                        <select id={`duration-${index}`} className="setting-dropdown">
                            {this.displayDurations(slotDurations)}
                        </select>
                    </td>
                    <td>
                        <select id={`capacity-${index}`} className="setting-dropdown">
                            {this.displayCapacities(slotCapacities)}
                        </select>
                    </td>
                    <td className="col-checkbox">
                        <input id={`cb-working-status-${index}`} type="checkbox" className="form-check-input"/>
                    </td>
                </tr>
            ))
        }
        return result;
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="row">
                    <SideNavBar/>
                    <div className="col-md-10 main-salon-content">
                        <div className="row">
                            <h3 className="text-center">Cài đặt ca làm việc</h3>
                        </div>
                        <form onSubmit={this.handleSaveSchedules}>
                        <table className="table table-striped table-bordered setting-table">
                            <thead className="thead-dark">
                                <tr>
                                    <th scope="col" colSpan="1"></th>
                                    <th scope="col" colSpan="2">Buổi sáng</th>
                                    <th scope="col" colSpan="2">Buổi chiều</th>
                                    <th scope="col" colSpan="3"></th>
                                </tr>
                                <tr>
                                    <th scope="col">Ngày</th>
                                    <th scope="col">Bắt đầu</th>
                                    <th scope="col">Kết thúc</th>
                                    <th scope="col">Bắt đầu</th>
                                    <th scope="col">Kết thúc</th>
                                    <th scope="col">Khoảng cách mỗi ca đặt</th>
                                    <th scope="col">Số khách tối đa 1 ca (Số ghế)</th>
                                    <th scope="col">Không làm việc</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.displayWeekDays(weekDays)}
                            </tbody>
                        </table>
                        <div className="alert alert-success invisible" id="alert-save-success">Lưu thành công</div>
                        <button type="submit" className="btn btn-primary">Lưu thay đổi</button>
                        </form>
                    </div>
                </div>
                <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer, scheduleReducer: state.scheduleReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleFetchSalonByPhoneNumber: async(phoneNumber) => {
            await dispatch(getSalonByPhoneNumber(phoneNumber))
        },
        handleFetchSchedulesBySalonId: async(salonId) => {
            await dispatch(getSchedulesBySalonIdAndWeekDay(salonId))
        },
        handleCreateSchedule: async(schedule) => {
            await dispatch(createSchedule(schedule))
        },
        handleUpdateSchedule: async(schedule) => {
            await dispatch(updateSchedule(schedule))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SalonSetting)