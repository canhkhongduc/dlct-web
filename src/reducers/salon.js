import {
    GET_SALONS,
    GET_SALONS_SUCCESS,
    POSTING_SALON,
    POSTING_SALON_SUCCESS,
    GET_SALON_BY_ID,
    GET_SALON_BY_ID_SUCCESS,
    SALON_AUTHENTICATE,
    SALON_AUTHENTICATE_SUCCESS,
    GET_SALON_BY_PHONE_NUMBER,
    GET_SALON_BY_PHONE_NUMBER_SUCCESS,
    GET_SALON_BY_NORMALIZED_NAME
} from '../actions/salon'

const initialState = {
    allSalons: [],
    salon: [],
    token: [],
    isFetching: false
}

export default function salonReducer(state = initialState, action) {
    switch (action.type) {
        case GET_SALONS:
            return {
                ...state,
                isFetching: false
            }
        case GET_SALONS_SUCCESS:
            return {
                ...state,
                allSalons: JSON.parse(action.data),
                isFetching: false
            }
        case POSTING_SALON:
            return {
                ...state,
                isFetching: false
            }
        case POSTING_SALON_SUCCESS:
            return {
                ...state,
                salon: JSON.parse(action.data),
                isFetching: false
            }
        case GET_SALON_BY_ID:
            return {
                ...state,
                isFetching: false
            }
        case GET_SALON_BY_ID_SUCCESS:
            return {
                ...state,
                salon: JSON.parse(action.data),
                isFetching: false
            }
        case GET_SALON_BY_PHONE_NUMBER:
            return {
                ...state,
                isFetching: false
            }
        case GET_SALON_BY_PHONE_NUMBER_SUCCESS:
            return {
                ...state,
                salon: JSON.parse(action.data),
                isFetching: false
            }
        case SALON_AUTHENTICATE:
            return {
                ...state,
                isFetching: false
            }
        case SALON_AUTHENTICATE_SUCCESS:
            return {
                ...state,
                token: JSON.parse(action.data),
                isFetching: false
            }
        case GET_SALON_BY_NORMALIZED_NAME:
            return {
                ...state,
                salon: JSON.parse(action.data)
            }
            
        default:
            return state;
    }
}
