import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const GET_DISTRICT_BY_ID = 'GET_DISTRICT_BY_ID';
export const GET_DISTRICT_BY_ID_SUCCESS = 'GET_DISTRICT_BY_ID_SUCCESS';

export const GET_DISTRICTS = 'GET_DISTRICTS';
export const GET_DISTRICTS_SUCCESS = 'GET_DISTRICTS_SUCCESS';

export const GET_DISTRICTS_HAS_SALON = 'GET_DISTRICTS_HAS_SALON';
export const GET_DISTRICTS_HAS_SALON_SUCCESS = 'GET_DISTRICTS_HAS_SALON_SUCCESS';

const getDistrictById = () => ({type: GET_DISTRICT_BY_ID});
const getDistrictByIdSuccess = (data) => ({type: GET_DISTRICT_BY_ID_SUCCESS, data});

const getDistrictsByCityId = () => ({type: GET_DISTRICTS});
const getDistrictsByCityIdSuccess = (data) => ({type: GET_DISTRICTS_SUCCESS, data});

const getDistrictsHasSalonByCityId = () => ({type: GET_DISTRICTS_HAS_SALON});
const getDistrictsHasSalonByCityIdSuccess = (data) => ({type: GET_DISTRICTS_HAS_SALON_SUCCESS, data});

export function fetchDataDistrictById(districtId) {
    return async(dispatch) => {
        dispatch(getDistrictById())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getDistrictByDistrictId(districtId), 'GET');
            dispatch(getDistrictByIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function fetchDataDistrictsByCityId(cityId) {
    return async(dispatch) => {
        dispatch(getDistrictsByCityId())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getAllDistrictsByCityId(cityId), 'GET')
            dispatch(getDistrictsByCityIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function fetchDataDistrictsHasSalonByCityId(cityId) {
    return async(dispatch) => {
        dispatch(getDistrictsHasSalonByCityId())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getDistrictsHasSalonByCityId(cityId), 'GET')
            dispatch(getDistrictsHasSalonByCityIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}