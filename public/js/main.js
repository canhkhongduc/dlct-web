
$("#morning-start a")
    .click(function (e) {
        e.preventDefault(); // cancel the link behaviour
        let selText = $(this).text();
        $("#btn-morning-start").text(selText);
    });

$("#morning-finish a").click(function (e) {
    e.preventDefault(); // cancel the link behaviour
    let selText = $(this).text();
    $("#btn-morning-finish").text(selText);
});

$("#afternoon-start a").click(function (e) {
    e.preventDefault(); // cancel the link behaviour
    let selText = $(this).text();
    $("#btn-afternoon-start").text(selText);
});

$("#afternoon-finish a").click(function (e) {
    e.preventDefault(); // cancel the link behaviour
    let selText = $(this).text();
    $("#btn-afternoon-finish").text(selText);
});

$("#dd-duration a").click(function (e) {
    e.preventDefault(); // cancel the link behaviour
    let selText = $(this).text();
    $("#btn-duration").text(selText);
});

$("#dd-capacity a").click(function (e) {
    e.preventDefault(); // cancel the link behaviour
    let selText = $(this).text();
    $("#btn-capacity").text(selText);
});
