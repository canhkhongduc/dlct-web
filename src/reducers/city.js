import {
    GET_CITY_BY_ID,
    GET_CITY_BY_ID_SUCCESS,
    GET_ALL_CITIES,
    GET_ALL_CITIES_SUCCESS,
    GET_CITIES_HAS_SALON,
    GET_CITIES_HAS_SALON_SUCCESS
} from '../actions/city';

const initialState = {
    allCities: [],
    allCitiesHasSalon: [],
    city: [],
    isFetching: false
};

export default function cityReducer(state = initialState, action) {
    switch (action.type) {
        case GET_CITY_BY_ID:
            return {
                ...state,
                isFetching: true
            }
        case GET_CITY_BY_ID_SUCCESS:
            return {
                ...state,
                city: JSON.parse(action.data),
                isFetching: false
            }
        case GET_ALL_CITIES:
            return {
                ...state,
                isFetching: true
            }
        case GET_ALL_CITIES_SUCCESS:
            return {
                ...state,
                allCities: JSON.parse(action.data),
                isFetching: false
            }
        case GET_CITIES_HAS_SALON:
            return {
                ...state,
                isFetching: true
            }
        case GET_CITIES_HAS_SALON_SUCCESS:
            return {
                ...state,
                allCitiesHasSalon: JSON.parse(action.data),
                isFetching: false
            }
        default:
            return state;
    }
}
