-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 19, 2019 lúc 04:42 AM
-- Phiên bản máy phục vụ: 10.1.32-MariaDB
-- Phiên bản PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `dlct_backend`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) NOT NULL,
  `booking_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `salon_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `created_by` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_by` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `booking`
--

INSERT INTO `booking` (`booking_time`, `status`, `salon_id`, `customer_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
('2019-01-12 07:29:14', 1, 3, 1, 'anonymousUser', '2019-01-12 00:29:54', 'anonymousUser', '2019-01-12 00:29:54'),
('2019-01-13 07:29:14', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:18', 'anonymousUser', '2019-01-12 00:30:18'),
('2019-01-14 07:29:14', 1, 9, 1, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24');

INSERT INTO `booking` (`booking_time`, `status`, `salon_id`, `customer_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
('2019-01-14 08:30:00', 1, 9, 1, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 09:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 09:30:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 09:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 09:30:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 10:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 11:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 13:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 15:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-01-14 16:45:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24');


INSERT INTO `booking` (`booking_time`, `status`, `salon_id`, `customer_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
('2019-02-22 08:30:00', 1, 9, 1, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 09:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 09:30:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 09:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 09:30:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 10:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 11:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 13:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 15:00:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24'),
('2019-02-22 16:45:00', 1, 9, 2, 'anonymousUser', '2019-01-12 00:30:24', 'anonymousUser', '2019-01-12 00:30:24');
-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `city`
--

CREATE TABLE `city` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `city`
--

INSERT INTO `city` (`id`, `name`) VALUES
(1, 'Hà Nội'),
(2, 'Hồ Chí Minh'),
(3, 'Đà Nẵng');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_bin NOT NULL,
  `activated` bit(1) NOT NULL,
  `created_by` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_by` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`name`, `phone_number`, `activated`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
('Cảnh', '0989284470', b'1', 'anonymousUser', '2019-01-10 09:04:53', 'anonymousUser', '2019-01-10 09:04:53');

INSERT INTO `customer` (`name`, `phone_number`, `activated`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
('Lâm', '01235468952', b'1', 'anonymousUser', '2019-01-10 09:04:53', 'anonymousUser', '2019-01-10 09:04:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `databasechangelog`
--

CREATE TABLE `databasechangelog` (
  `ID` varchar(255) COLLATE utf8_bin NOT NULL,
  `AUTHOR` varchar(255) COLLATE utf8_bin NOT NULL,
  `FILENAME` varchar(255) COLLATE utf8_bin NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) COLLATE utf8_bin NOT NULL,
  `MD5SUM` varchar(35) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LIQUIBASE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `CONTEXTS` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LABELS` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `databasechangelog`
--

INSERT INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
('00000000000001', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml', '2019-01-10 23:04:33', 1, 'EXECUTED', '7:9d88ecd533d5a3530e304f778b9dc982', 'createTable tableName=jhi_persistent_audit_event; createTable tableName=jhi_persistent_audit_evt_data; addPrimaryKey tableName=jhi_persistent_audit_evt_data; createIndex indexName=idx_persistent_audit_event, tableName=jhi_persistent_audit_event; c...', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109181912-1', 'jhipster', 'config/liquibase/changelog/20190109181912_added_entity_Salon.xml', '2019-01-10 23:04:33', 2, 'EXECUTED', '7:aa3211b225249fb049b6196c4ca3ec73', 'createTable tableName=salon', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109182020-audit-1', 'jhipster-audit-helper', 'config/liquibase/changelog/20190109181912_added_entity_Salon.xml', '2019-01-10 23:04:33', 3, 'EXECUTED', '7:42fce84eb13acf1c1ee5e8a59e64927a', 'addColumn tableName=salon', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109183058-1', 'jhipster', 'config/liquibase/changelog/20190109183058_added_entity_City.xml', '2019-01-10 23:04:33', 4, 'EXECUTED', '7:20bdbeb40684074293bf79cf7bffcd6a', 'createTable tableName=city', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109183209-1', 'jhipster', 'config/liquibase/changelog/20190109183209_added_entity_District.xml', '2019-01-10 23:04:33', 5, 'EXECUTED', '7:2bffefb0f9057c9baa97d8f212f296b6', 'createTable tableName=district', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109184757-1', 'jhipster', 'config/liquibase/changelog/20190109184757_added_entity_Customer.xml', '2019-01-10 23:04:33', 6, 'EXECUTED', '7:4af0c0ab4ce36eb89ce549d75cdb9c3d', 'createTable tableName=customer', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190225-audit-1', 'jhipster-audit-helper', 'config/liquibase/changelog/20190109184757_added_entity_Customer.xml', '2019-01-10 23:04:33', 7, 'EXECUTED', '7:7e28ed86f60be8a4b6b173e2ebbc023f', 'addColumn tableName=customer', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109185005-1', 'jhipster', 'config/liquibase/changelog/20190109185005_added_entity_Booking.xml', '2019-01-10 23:04:33', 8, 'EXECUTED', '7:166f5680978f205d2a245b3462596749', 'createTable tableName=booking; dropDefaultValue columnName=booking_time, tableName=booking', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190225-audit-1', 'jhipster-audit-helper', 'config/liquibase/changelog/20190109185005_added_entity_Booking.xml', '2019-01-10 23:04:33', 9, 'EXECUTED', '7:5a7e65f05c869b09b234abaa29aec789', 'addColumn tableName=booking', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109185209-1', 'jhipster', 'config/liquibase/changelog/20190109185209_added_entity_UnavailableSlot.xml', '2019-01-10 23:04:34', 10, 'EXECUTED', '7:f7debb598f9a01465b0eac1e2a480b53', 'createTable tableName=unavailable_slot; dropDefaultValue columnName=slot_time, tableName=unavailable_slot', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190225-audit-1', 'jhipster-audit-helper', 'config/liquibase/changelog/20190109185209_added_entity_UnavailableSlot.xml', '2019-01-10 23:04:34', 11, 'EXECUTED', '7:322ed30b323685b4fae810feab8a2acb', 'addColumn tableName=unavailable_slot', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190210-1', 'jhipster', 'config/liquibase/changelog/20190109190210_added_entity_Schedule.xml', '2019-01-10 23:04:34', 12, 'EXECUTED', '7:94eb7c6f7e6c504d5bb00781bb3afbf0', 'createTable tableName=schedule', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190225-audit-1', 'jhipster-audit-helper', 'config/liquibase/changelog/20190109190210_added_entity_Schedule.xml', '2019-01-10 23:04:34', 13, 'EXECUTED', '7:1d3e6e55e9f9b80e2df07fae7e3cf60d', 'addColumn tableName=schedule', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190225', 'jhipster', 'config/liquibase/changelog/20190109190225_added_entity_EntityAuditEvent.xml', '2019-01-10 23:04:34', 14, 'EXECUTED', '7:5a696671965a1033d4021cc98168b228', 'createTable tableName=jhi_entity_audit_event; createIndex indexName=idx_entity_audit_event_entity_id, tableName=jhi_entity_audit_event; createIndex indexName=idx_entity_audit_event_entity_type, tableName=jhi_entity_audit_event; dropDefaultValue co...', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109181912-2', 'jhipster', 'config/liquibase/changelog/20190109181912_added_entity_constraints_Salon.xml', '2019-01-10 23:04:34', 15, 'EXECUTED', '7:8b02d16c5004aec0a91383997367de2c', 'addForeignKeyConstraint baseTableName=salon, constraintName=fk_salon_city_id, referencedTableName=city; addForeignKeyConstraint baseTableName=salon, constraintName=fk_salon_district_id, referencedTableName=district', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109183209-2', 'jhipster', 'config/liquibase/changelog/20190109183209_added_entity_constraints_District.xml', '2019-01-10 23:04:34', 16, 'EXECUTED', '7:80aeee1d1be9f2d58dd0195bf70b207a', 'addForeignKeyConstraint baseTableName=district, constraintName=fk_district_city_id, referencedTableName=city', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109185005-2', 'jhipster', 'config/liquibase/changelog/20190109185005_added_entity_constraints_Booking.xml', '2019-01-10 23:04:34', 17, 'EXECUTED', '7:f44af07eeb681164c30755e392bbfe5f', 'addForeignKeyConstraint baseTableName=booking, constraintName=fk_booking_salon_id, referencedTableName=salon; addForeignKeyConstraint baseTableName=booking, constraintName=fk_booking_customer_id, referencedTableName=customer', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109185209-2', 'jhipster', 'config/liquibase/changelog/20190109185209_added_entity_constraints_UnavailableSlot.xml', '2019-01-10 23:04:34', 18, 'EXECUTED', '7:1af78762f7c83282f1c01885ed840940', 'addForeignKeyConstraint baseTableName=unavailable_slot, constraintName=fk_unavailable_slot_salon_id, referencedTableName=salon', '', NULL, '3.5.4', NULL, NULL, '7136273157'),
('20190109190210-2', 'jhipster', 'config/liquibase/changelog/20190109190210_added_entity_constraints_Schedule.xml', '2019-01-10 23:04:35', 19, 'EXECUTED', '7:6c528a80f2f58cd854bf57649883ead4', 'addForeignKeyConstraint baseTableName=schedule, constraintName=fk_schedule_salon_id, referencedTableName=salon', '', NULL, '3.5.4', NULL, NULL, '7136273157');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `databasechangeloglock`
--

CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `databasechangeloglock`
--

INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `district`
--

CREATE TABLE `district` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `city_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `district`
--

INSERT INTO `district` (`id`, `name`, `city_id`) VALUES
(1, 'Cầu Giấy', 1),
(2, 'Hoàn Kiếm', 1),
(3, 'Hà Đông', 1),
(4, 'Hai Bà Trưng', 1),
(5, 'Ba Đình', 1),
(6, 'Bắc Từ Liêm', 1),
(7, 'Đống Đa', 1),
(8, 'Hai Bà Trưng', 1),
(9, 'Hoàng Mai', 1),
(10, 'Long Biên', 1),
(11, 'Nam Từ Liêm', 1),
(12, 'Tây Hồ', 1),
(13, 'Thanh Xuân', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `jhi_entity_audit_event`
--

CREATE TABLE `jhi_entity_audit_event` (
  `id` bigint(20) NOT NULL,
  `entity_id` bigint(20) NOT NULL,
  `entity_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `action` varchar(20) COLLATE utf8_bin NOT NULL,
  `entity_value` longtext COLLATE utf8_bin,
  `commit_version` int(11) DEFAULT NULL,
  `modified_by` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `modified_date` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `jhi_entity_audit_event`
--

INSERT INTO `jhi_entity_audit_event` (`id`, `entity_id`, `entity_type`, `action`, `entity_value`, `commit_version`, `modified_by`, `modified_date`) VALUES
(1, 1, 'com.dlct.domain.Customer', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-10T16:04:53.743Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-10T16:04:53.743Z\",\r\n  \"id\" : 1,\r\n  \"name\" : \"Cảnh\",\r\n  \"phoneNumber\" : \"0989284470\",\r\n  \"activated\" : true\r\n}', 1, 'anonymousUser', '2019-01-10 09:04:53'),
(2, 2, 'com.dlct.domain.Salon', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-12T07:28:33.708Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-12T07:28:33.708Z\",\r\n  \"id\" : 2,\r\n  \"name\" : \"30Shine\",\r\n  \"email\" : \"canhkhong@gmail.com\",\r\n  \"phoneNumber\" : \"0989284470\",\r\n  \"passwordHash\" : \"123zzdhkjf\",\r\n  \"latitude\" : 105.0,\r\n  \"longitude\" : 7.0,\r\n  \"activated\" : true,\r\n  \"address\" : \"18 Xuân Thủy\",\r\n  \"city\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null\r\n  },\r\n  \"district\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"city\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-12 00:28:33'),
(3, 1, 'com.dlct.domain.Booking', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-12T07:29:54.112Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-12T07:29:54.112Z\",\r\n  \"id\" : 1,\r\n  \"time\" : \"2019-01-12T07:29:14.351Z\",\r\n  \"status\" : 1,\r\n  \"salon\" : {\r\n    \"createdBy\" : null,\r\n    \"createdDate\" : \"2019-01-12T07:29:54.105Z\",\r\n    \"lastModifiedBy\" : null,\r\n    \"lastModifiedDate\" : \"2019-01-12T07:29:54.105Z\",\r\n    \"id\" : 2,\r\n    \"name\" : null,\r\n    \"email\" : null,\r\n    \"phoneNumber\" : null,\r\n    \"passwordHash\" : null,\r\n    \"latitude\" : null,\r\n    \"longitude\" : null,\r\n    \"activated\" : null,\r\n    \"address\" : null,\r\n    \"city\" : null,\r\n    \"district\" : null\r\n  },\r\n  \"customer\" : {\r\n    \"createdBy\" : null,\r\n    \"createdDate\" : \"2019-01-12T07:29:54.105Z\",\r\n    \"lastModifiedBy\" : null,\r\n    \"lastModifiedDate\" : \"2019-01-12T07:29:54.105Z\",\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"phoneNumber\" : null,\r\n    \"activated\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-12 00:29:54'),
(4, 2, 'com.dlct.domain.Booking', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-12T07:30:18.192Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-12T07:30:18.192Z\",\r\n  \"id\" : 2,\r\n  \"time\" : \"2019-01-13T07:29:14.351Z\",\r\n  \"status\" : 1,\r\n  \"salon\" : {\r\n    \"createdBy\" : null,\r\n    \"createdDate\" : \"2019-01-12T07:30:18.190Z\",\r\n    \"lastModifiedBy\" : null,\r\n    \"lastModifiedDate\" : \"2019-01-12T07:30:18.190Z\",\r\n    \"id\" : 2,\r\n    \"name\" : null,\r\n    \"email\" : null,\r\n    \"phoneNumber\" : null,\r\n    \"passwordHash\" : null,\r\n    \"latitude\" : null,\r\n    \"longitude\" : null,\r\n    \"activated\" : null,\r\n    \"address\" : null,\r\n    \"city\" : null,\r\n    \"district\" : null\r\n  },\r\n  \"customer\" : {\r\n    \"createdBy\" : null,\r\n    \"createdDate\" : \"2019-01-12T07:30:18.190Z\",\r\n    \"lastModifiedBy\" : null,\r\n    \"lastModifiedDate\" : \"2019-01-12T07:30:18.190Z\",\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"phoneNumber\" : null,\r\n    \"activated\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-12 00:30:18'),
(5, 3, 'com.dlct.domain.Booking', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-12T07:30:24.027Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-12T07:30:24.027Z\",\r\n  \"id\" : 3,\r\n  \"time\" : \"2019-01-14T07:29:14.351Z\",\r\n  \"status\" : 1,\r\n  \"salon\" : {\r\n    \"createdBy\" : null,\r\n    \"createdDate\" : \"2019-01-12T07:30:24.026Z\",\r\n    \"lastModifiedBy\" : null,\r\n    \"lastModifiedDate\" : \"2019-01-12T07:30:24.026Z\",\r\n    \"id\" : 2,\r\n    \"name\" : null,\r\n    \"email\" : null,\r\n    \"phoneNumber\" : null,\r\n    \"passwordHash\" : null,\r\n    \"latitude\" : null,\r\n    \"longitude\" : null,\r\n    \"activated\" : null,\r\n    \"address\" : null,\r\n    \"city\" : null,\r\n    \"district\" : null\r\n  },\r\n  \"customer\" : {\r\n    \"createdBy\" : null,\r\n    \"createdDate\" : \"2019-01-12T07:30:24.026Z\",\r\n    \"lastModifiedBy\" : null,\r\n    \"lastModifiedDate\" : \"2019-01-12T07:30:24.026Z\",\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"phoneNumber\" : null,\r\n    \"activated\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-12 00:30:24'),
(6, 3, 'com.dlct.domain.Salon', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-14T16:59:26.659Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-14T16:59:26.659Z\",\r\n  \"id\" : 3,\r\n  \"name\" : \"Liêm Barber\",\r\n  \"email\" : \"abc@gmail.com\",\r\n  \"phoneNumber\" : \"0958632542\",\r\n  \"passwordHash\" : \"123zzdhkjf\",\r\n  \"latitude\" : 105.0,\r\n  \"longitude\" : 7.0,\r\n  \"activated\" : true,\r\n  \"address\" : \"114 Lạc Long Quân\",\r\n  \"city\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null\r\n  },\r\n  \"district\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"city\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-14 09:59:26'),
(7, 4, 'com.dlct.domain.Salon', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-14T17:01:06.528Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-14T17:01:06.528Z\",\r\n  \"id\" : 4,\r\n  \"name\" : \"192 Hair Salon\",\r\n  \"email\" : \"abc@gmail.com\",\r\n  \"phoneNumber\" : \"0958632542\",\r\n  \"passwordHash\" : \"123zzdhkjf\",\r\n  \"latitude\" : 105.0,\r\n  \"longitude\" : 7.0,\r\n  \"activated\" : true,\r\n  \"address\" : \"114 Trần Đại Nghĩa\",\r\n  \"city\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null\r\n  },\r\n  \"district\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"city\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-14 10:01:06'),
(8, 5, 'com.dlct.domain.Salon', 'CREATE', '{\r\n  \"createdBy\" : \"anonymousUser\",\r\n  \"createdDate\" : \"2019-01-19T02:43:52.884Z\",\r\n  \"lastModifiedBy\" : \"anonymousUser\",\r\n  \"lastModifiedDate\" : \"2019-01-19T02:43:52.884Z\",\r\n  \"id\" : 5,\r\n  \"name\" : \"Anh Minh Salon\",\r\n  \"email\" : \"abc@gmail.com\",\r\n  \"phoneNumber\" : \"0958632542\",\r\n  \"passwordHash\" : \"123zzdhkjf\",\r\n  \"latitude\" : 105.0,\r\n  \"longitude\" : 7.0,\r\n  \"activated\" : true,\r\n  \"address\" : \"114 Cầu Giấy\",\r\n  \"city\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null\r\n  },\r\n  \"district\" : {\r\n    \"id\" : 1,\r\n    \"name\" : null,\r\n    \"city\" : null\r\n  }\r\n}', 1, 'anonymousUser', '2019-01-18 19:43:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `jhi_persistent_audit_event`
--

CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL,
  `principal` varchar(50) COLLATE utf8_bin NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `jhi_persistent_audit_evt_data`
--

CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) COLLATE utf8_bin NOT NULL,
  `value` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `salon`
--

CREATE TABLE `salon` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phone_number` varchar(15) COLLATE utf8_bin NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_bin NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `address` varchar(200) COLLATE utf8_bin NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `district_id` bigint(20) NOT NULL,
  `created_by` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_by` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Đang đổ dữ liệu cho bảng `salon`
--

INSERT INTO `salon` (`id`, `name`, `email`, `phone_number`, `password_hash`, `latitude`, `longitude`, `activated`, `address`, `district_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
(2, '30Shine', 'canhkhong@gmail.com', '0989284470', '123zzdhkjf', 105, 7, b'1', '18 Xuân Thủy', 6, 'anonymousUser', '2019-01-12 00:28:33', 'anonymousUser', '2019-01-12 00:28:33'),
(3, 'Liêm Barber', 'abc@gmail.com', '0958632542', '123zzdhkjf', 105, 7, b'1', '114 Lạc Long Quân', 6, 'anonymousUser', '2019-01-14 09:59:26', 'anonymousUser', '2019-01-14 09:59:26'),
(4, '192 Hair Salon', 'abc@gmail.com', '0958632542', '123zzdhkjf', 105, 7, b'1', '114 Trần Đại Nghĩa', 6, 'anonymousUser', '2019-01-14 10:01:06', 'anonymousUser', '2019-01-14 10:01:06'),
(5, 'Anh Minh Salon', 'abc@gmail.com', '0958632542', '123zzdhkjf', 105, 7, b'1', '114 Cầu Giấy', 6, 'anonymousUser', '2019-01-18 19:43:52', 'anonymousUser', '2019-01-18 19:43:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `schedule`
--

CREATE TABLE `schedule` (
  `id` bigint(20) NOT NULL,
  `week_day` int(11) NOT NULL,
  `start_am` varchar(10) COLLATE utf8_bin NOT NULL,
  `finish_am` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `start_pm` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `finish_pm` varchar(10) COLLATE utf8_bin NOT NULL,
  `slot_duration` int(11) NOT NULL,
  `slot_capacity` int(11) NOT NULL,
  `working_status` bit(1) NOT NULL,
  `salon_id` bigint(20) NOT NULL,
  `created_by` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_by` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `unavailable_slot`
--

CREATE TABLE `unavailable_slot` (
  `id` bigint(20) NOT NULL,
  `slot_time` datetime NOT NULL,
  `salon_id` bigint(20) NOT NULL,
  `created_by` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_by` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_booking_salon_id` (`salon_id`),
  ADD KEY `fk_booking_customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_customer_phone_number` (`phone_number`);

--
-- Chỉ mục cho bảng `databasechangeloglock`
--
ALTER TABLE `databasechangeloglock`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_district_city_id` (`city_id`);

--
-- Chỉ mục cho bảng `jhi_entity_audit_event`
--
ALTER TABLE `jhi_entity_audit_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_entity_audit_event_entity_id` (`entity_id`),
  ADD KEY `idx_entity_audit_event_entity_type` (`entity_type`);

--
-- Chỉ mục cho bảng `jhi_persistent_audit_event`
--
ALTER TABLE `jhi_persistent_audit_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `idx_persistent_audit_event` (`principal`,`event_date`);

--
-- Chỉ mục cho bảng `jhi_persistent_audit_evt_data`
--
ALTER TABLE `jhi_persistent_audit_evt_data`
  ADD PRIMARY KEY (`event_id`,`name`),
  ADD KEY `idx_persistent_audit_evt_data` (`event_id`);

--
-- Chỉ mục cho bảng `salon`
--
ALTER TABLE `salon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_salon_city_id` (`city_id`),
  ADD KEY `fk_salon_district_id` (`district_id`);

--
-- Chỉ mục cho bảng `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_schedule_salon_id` (`salon_id`);

--
-- Chỉ mục cho bảng `unavailable_slot`
--
ALTER TABLE `unavailable_slot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_unavailable_slot_salon_id` (`salon_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `district`
--
ALTER TABLE `district`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `jhi_entity_audit_event`
--
ALTER TABLE `jhi_entity_audit_event`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `jhi_persistent_audit_event`
--
ALTER TABLE `jhi_persistent_audit_event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `salon`
--
ALTER TABLE `salon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `unavailable_slot`
--
ALTER TABLE `unavailable_slot`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `fk_booking_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `fk_booking_salon_id` FOREIGN KEY (`salon_id`) REFERENCES `salon` (`id`);

--
-- Các ràng buộc cho bảng `district`
--
ALTER TABLE `district`
  ADD CONSTRAINT `fk_district_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`);

--
-- Các ràng buộc cho bảng `jhi_persistent_audit_evt_data`
--
ALTER TABLE `jhi_persistent_audit_evt_data`
  ADD CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`);

--
-- Các ràng buộc cho bảng `salon`
--
ALTER TABLE `salon`
  ADD CONSTRAINT `fk_salon_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `fk_salon_district_id` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`);

--
-- Các ràng buộc cho bảng `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `fk_schedule_salon_id` FOREIGN KEY (`salon_id`) REFERENCES `salon` (`id`);

--
-- Các ràng buộc cho bảng `unavailable_slot`
--
ALTER TABLE `unavailable_slot`
  ADD CONSTRAINT `fk_unavailable_slot_salon_id` FOREIGN KEY (`salon_id`) REFERENCES `salon` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
