import ReactDOM from 'react-dom'
import React from 'react'
import { connect } from 'react-redux'
import  BookingsItem  from '../components/BookingsItem';

let Bookings = ({ data, loading }) => {
    let bookings = "";
    if (data) {
        bookings = data.map((booking, index) =>
            (
                <div key={`${index}`} className="row">
                    <BookingsItem booking={data[index]} />
                </div>
            )
        )
    }
    if (loading) {
        bookings = <h3 className="loading-indicator">Loading ...</h3>
    }

    return (
        <div>
            {bookings}
        </div>
    )
}

const mapStateToProps = (state) => ({
    data: state.json,
    loading: state.loading
})

Bookings = connect(
    mapStateToProps,
    null
)(Bookings)

export default Bookings;

