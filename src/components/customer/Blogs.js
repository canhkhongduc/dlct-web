import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import dateFormat from 'dateformat';
import moment from 'moment';
import Dialog from 'react-bootstrap-dialog'
import Carousel from 'react-bootstrap/Carousel';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import { NavLink } from 'react-router-dom';
import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,
} from "react-simple-maps"
import { getSalonByNormalizedName } from '../../actions/salon';

class Blogs extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
           
    }

    componentWillMount = async() => {
        
    }
    
    render() {
        return (
            <div>
                <Header />
                <section id="blog-zone">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="row blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                        <div className="row blog-description">
                                            Bạn có cảm thấy mệt mỏi khi quản lý cửa hàng qua excel, sổ sách? Bạn đang cần tìm giải pháp hiệu quả hơn? Giữa chuyên nghiệp và nghiệp dư, bạn muốn chọn phương án nào?
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6"></div>
                                            <div className="col-md-6">
                                            <NavLink className="pull-right btn-read" data-target="#link1" to={{pathname: '/blogs/cac-phuong-phap-quan-ly-cua-hang-ma-ban-can-biet'}}>Đọc tiếp</NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                        <div className="row blog-description">
                                            Bạn có cảm thấy mệt mỏi khi quản lý cửa hàng qua excel, sổ sách? Bạn đang cần tìm giải pháp hiệu quả hơn? Giữa chuyên nghiệp và nghiệp dư, bạn muốn chọn phương án nào?
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6"></div>
                                            <div className="col-md-6">
                                            <a class="pull-right btn-read" href="/blog/kinh-nghiem-kinh-doanh/cac-phuong-phap-quan-ly-cua-hang-ma-ban-can-biet">Đọc tiếp</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                        <div className="row blog-description">
                                            Bạn có cảm thấy mệt mỏi khi quản lý cửa hàng qua excel, sổ sách? Bạn đang cần tìm giải pháp hiệu quả hơn? Giữa chuyên nghiệp và nghiệp dư, bạn muốn chọn phương án nào?
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6"></div>
                                            <div className="col-md-6">
                                            <a class="pull-right btn-read" href="/blog/kinh-nghiem-kinh-doanh/cac-phuong-phap-quan-ly-cua-hang-ma-ban-can-biet">Đọc tiếp</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                        <div className="row blog-description">
                                            Bạn có cảm thấy mệt mỏi khi quản lý cửa hàng qua excel, sổ sách? Bạn đang cần tìm giải pháp hiệu quả hơn? Giữa chuyên nghiệp và nghiệp dư, bạn muốn chọn phương án nào?
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6"></div>
                                            <div className="col-md-6">
                                            <a class="pull-right btn-read" href="/blog/kinh-nghiem-kinh-doanh/cac-phuong-phap-quan-ly-cua-hang-ma-ban-can-biet">Đọc tiếp</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 popular-blogs">
                                <div className="popular-blog-label">
                                    Bài viết tiêu biểu
                                </div>
                                <div className="row popular-blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                    </div>
                                </div>
                                <div className="row popular-blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                    </div>
                                </div>
                                <div className="row popular-blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <a class="btn-load-more btn btn-primary"> Tiếp theo <i class="fas fa-chevron-right"></i> </a>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetSalonByNormalizedName: async(salonNormalizedName) => {
            await dispatch(getSalonByNormalizedName(salonNormalizedName));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Blogs)