import * as apiRequest from '../util/apiRequest'
import * as endpoints from '../constants/endpoints'
export const CREATING_PIN_CODE = 'CREATING_PIN_CODE'
export const CREATING_PIN_CODE_SUCCESS = 'CREATING_PIN_CODE_SUCCESS'
export const VERIFYING_PIN_CODE_SUCCESS = 'VERIFYING_PIN_CODE_SUCCESS'
export const CREATING_CUSTOMER_SUCCESS = 'CREATING_CUSTOMER_SUCCESS'
export const GETTING_CUSTOMER_BY_PHONE_SUCCESS = 'GETTING_CUSTOMER_BY_PHONE_SUCCESS'

export function createPinCode() {
    return {type: CREATING_PIN_CODE}
}
export function createPinCodeSuccess(data) {
    return {type: CREATING_PIN_CODE_SUCCESS, data}
}

export function verifyPinCodeSuccess(data) {
    return {type: VERIFYING_PIN_CODE_SUCCESS, data}
}

export function createCustomerSuccess(data) {
    return {type: CREATING_CUSTOMER_SUCCESS, data}
}


export function getCustomerByPhoneSuccess(data) {
    return {type: GETTING_CUSTOMER_BY_PHONE_SUCCESS, data}
}

export function createAndSendPinCode(phones) {
    return async(dispatch) => {
        dispatch(createPinCode())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createAndSendPinCode, "POST", phones);
            dispatch(createPinCodeSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}
export function verifyPinCode(phone, pin_code) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.verifyPinCode(phone, pin_code), "GET");
            dispatch(verifyPinCodeSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function createCustomer(customer) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createCustomer, "POST", customer);
            dispatch(createCustomerSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getCustomerByPhoneNumber(phoneNumber) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getCustomerByPhoneNumber(phoneNumber), "GET");
            dispatch(getCustomerByPhoneSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}
