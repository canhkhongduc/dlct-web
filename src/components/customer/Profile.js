import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import dateFormat from 'dateformat';
import moment from 'moment';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import { ToastContainer, toast } from 'mdbreact';
import Dialog from 'react-bootstrap-dialog'
import DayPicker from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/vi';
import 'react-day-picker/lib/style.css';
import ProfileSideNav from '../layout/ProfileSideNav';
import { getUserById, updateUserInformation } from '../../actions/user';
const cookies = new Cookies();

class Profile extends Component {
    constructor(props) {
        super(props);
        this.input = React.createRef();
        this.state = {
            user: [],
            updateUser: [],

        };
    }

    componentWillMount = async() => {
        let userId = cookies.get("userId");
        let token = cookies.get("token");
        await this.props.handleGetUserById(userId, token);
        if(this.props.authenticationReducer.user.length !== 0) {
            this.setState({
                user: this.props.authenticationReducer.user
            })
        }
    }

    handleUpdateUserInformation = async(event) => {
        event.preventDefault();
        let phoneNumber = event.target.phoneNumber.value;
        let user = this.state.user;
        user.username = event.target.username.value;
        user.email = event.target.email.value;
        user.name = event.target.fullName.value;
        user.phoneNumber = event.target.phoneNumber.value;
        await this.props.handleUpdateUserInformation(user, cookies.get("token"));
        
        if(this.props.authenticationReducer.updateUser.length !== 0) {
            console.log(this.props.authenticationReducer.updateUser);
            toast.success("Cập nhật thành công");
            this.setState({
                user: user
            })
        }
    }

    render() {
        const {user} = this.state;
        const currentNav = "profile-nav";
        return (
            <div>
                <Header/>
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <ProfileSideNav currentNav={currentNav}/>
                            <div className="col-md-8">
                                <div className="row margin-bottom">
                                    Thông tin cá nhân
                                </div>
                                <form onSubmit={this.handleUpdateUserInformation}>
                                    <div className="form-group">
                                        <label htmlFor="username">Tài khoản *</label>
                                        <input
                                            ref={this.input}
                                            type="text"
                                            className="form-control"
                                            id="username"
                                            name="username"
                                            defaultValue={user.username}
                                            required/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Email liên hệ *</label>
                                        <input
                                            ref={this.input}
                                            type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            defaultValue={user.email}
                                            required/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="fullName">Họ và tên</label>
                                        <input
                                            ref={this.input}
                                            type="text"
                                            className="form-control"
                                            id="fullName"
                                            name="fullName"
                                            defaultValue={user.name}
                                            required/>
                                    </div>
                                    <div className="form-group customer-register">
                                        <label htmlFor="phoneNumber">Số điện thoại *</label>
                                        <input
                                            ref={this.input}
                                            type="text"
                                            className="form-control"
                                            id="phoneNumber"
                                            name="phoneNumber"
                                            defaultValue={user.phoneNumber}
                                            required/>
                                        <div className="invalid-feedback">
                                        Vui lòng điền số điện thoại
                                        </div>
                                    </div>
                                    <div className="row action-button">
                                        <button
                                            type="submit"
                                            className="btn btn-primary btn-salon-continue"
                                            >Cập nhật</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />  
                <Dialog ref={(component) => { this.dialog = component }} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        authenticationReducer: state.authenticationReducer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetUserById: async(id, token) => {
            await dispatch(getUserById(id, token));
        },
        handleUpdateUserInformation: async(user, token) => {
            await dispatch(updateUserInformation(user, token));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)