import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import {getSalonByNormalizedName} from '../../actions/salon';
import {fetchDataCityById} from '../../actions/city';
import {fetchDataDistrictById} from '../../actions/district';
import dateFormat from 'dateformat';
import moment from 'moment';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import { ToastContainer, toast } from 'mdbreact';
import { getCustomerByPhoneNumber } from '../../actions/customer';
import { createBooking, getBookingsByCustomerId, updateBooking, getOpenBookingsBySalonIdAndDate, createBookingService } from '../../actions/booking';
import Dialog from 'react-bootstrap-dialog'
import { getUnavailableSlotsBySalonId } from '../../actions/unavailableSlot';
import DayPicker from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/vi';
import 'react-day-picker/lib/style.css';
import { getSchedulesBySalonIdAndWeekDay } from '../../actions/schedule';
import ProfileSideNav from '../layout/ProfileSideNav';
import { getUserById, updateUserInformation } from '../../actions/user';
const cookies = new Cookies();

class Notification extends Component {
    constructor(props) {
        super(props);
        this.input = React.createRef();
        this.state = {
            user: [],
            updateUser: [],

        };
    }


    
    componentWillMount = async() => {
        let userId = cookies.get("userId");
        let token = cookies.get("token");
        await this.props.handleGetUserById(userId, token);
        if(this.props.authenticationReducer.user.length !== 0) {
            this.setState({
                user: this.props.authenticationReducer.user
            })
        }
    }

    handleUpdateUserInformation = async(event) => {
        event.preventDefault();
        let phoneNumber = event.target.phoneNumber.value;
        let user = this.state.user;
        user.username = event.target.username.value;
        user.email = event.target.email.value;
        user.name = event.target.name.value;
        user.phoneNumber = event.target.phoneNumber.value;
        await this.props.handleUpdateUserInformation(user, cookies.get("token"));
        if(this.props.authenticationReducer.updateUser.length !== 0) {
            toast.success("Cập nhật thành công");
            this.setState({
                user: user
            })
        }
    }
    render() {
        const {user} = this.state;
        const currentNav = "notification-nav";
        return (
            <div>
                <Header/>
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <ProfileSideNav currentNav={currentNav}/>
                            <div className="col-md-8">
                                <div className="row margin-bottom">
                                    Thông báo của tôi (3)
                                </div>
                                <div className="row notification-item">
                                    <div className="col-md-2 notification-date">04/05/2018</div>
                                    <div className="col-md-7 notification-message">Bạn đã huỷ đơn đặt chỗ mã số #802 tại Most Hair thành công! <span className="notification-detail">Chi tiết</span></div>
                                    <div className="col-md-2 notification-read">Đã đọc</div>
                                    <div className="col-md-1 notification-delete">Xóa</div>
                                </div>
                                <div className="row notification-item">
                                    <div className="col-md-2 notification-date">04/05/2018</div>
                                    <div className="col-md-7 notification-message">Bạn đã huỷ đơn đặt chỗ mã số #802 tại Most Hair thành công! <span className="notification-detail">Chi tiết</span></div>
                                    <div className="col-md-2 notification-read">Đã đọc</div>
                                    <div className="col-md-1 notification-delete">Xóa</div>
                                </div>
                                <div className="row notification-item unread">
                                    <div className="col-md-2 notification-date">04/05/2018</div>
                                    <div className="col-md-7 notification-message">Bạn đã huỷ đơn đặt chỗ mã số #802 tại Most Hair thành công! <span className="notification-detail">Chi tiết</span></div>
                                    <div className="col-md-2 notification-read">Đã đọc</div>
                                    <div className="col-md-1 notification-delete">Xóa</div>
                                </div>
                                <div className="row notification-item unread">
                                    <div className="col-md-2 notification-date">04/05/2018</div>
                                    <div className="col-md-7 notification-message">Bạn đã huỷ đơn đặt chỗ mã số #802 tại Most Hair thành công! <span className="notification-detail">Chi tiết</span></div>
                                    <div className="col-md-2 notification-read">Đã đọc</div>
                                    <div className="col-md-1 notification-delete">Xóa</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />  
                <Dialog ref={(component) => { this.dialog = component }} />
            </div>
        );
    }

}


function mapStateToProps(state) {
    return {
        authenticationReducer: state.authenticationReducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetUserById: async(id, token) => {
            await dispatch(getUserById(id, token));
        },
        handleUpdateUserInformation: async(user, token) => {
            await dispatch(updateUserInformation(user, token));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notification)