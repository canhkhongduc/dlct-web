import {
    GET_SERVICES_BY_SALONID,
    GET_SERVICES_BY_SALONID_SUCCESS
} from '../actions/service'

const initialState = {
    services: [],
    isFetching: false
}

export default function serviceReducer(state = initialState, action) {
    switch (action.type) {
        case GET_SERVICES_BY_SALONID:
            return {
                ...state,
                isFetching: true
            }
        case GET_SERVICES_BY_SALONID_SUCCESS:
            return {
                ...state,
                services: JSON.parse(action.data),
                isFetching: false
            }
        default:
            return state;
    }
}
