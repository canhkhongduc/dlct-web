import {
    GET_REVIEWS_BY_SALONID,
    GET_REVIEWS_BY_SALONID_SUCCESS,
    POSTING_REVIEW_SUCCESS
} from '../actions/review'

const initialState = {
    reviews: [],
    review: [],
    isFetching: false
}

export default function reviewReducer(state = initialState, action) {
    switch (action.type) {
        case GET_REVIEWS_BY_SALONID:
            return {
                ...state,
                isFetching: true
            }
        case GET_REVIEWS_BY_SALONID_SUCCESS:
            return {
                ...state,
                reviews: JSON.parse(action.data),
                isFetching: false
            }
        case POSTING_REVIEW_SUCCESS:
            return {
                ...state,
                review: JSON.parse(action.data),
                isFetching: false
            }
        default:
            return state;
    }
}
