import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const POSTING_IMAGE_SUCCESS = 'POSTING_IMAGE_SUCCESS';

export function postImageSuccess(data) {
    return { type: POSTING_IMAGE_SUCCESS, data };
}

export function uploadImage(image, token) {
    return async (dispatch) => {
        try {
            console.log(endpoints.uploadImage);
            const res = await apiRequest.uploadFile(token, endpoints.uploadImage, image);
            dispatch(postImageSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}