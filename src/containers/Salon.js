import React from 'react'
import { connect } from 'react-redux'
import { getSalon, activateChannel } from '../actions'

let Salon = ({ salonName, salonId, onClick, active }) => (
    <div  className=" col-lg-2 col-md-4 col-sm-6 ">
        <div className="channel-button" onClick={onClick}
            style={{ backgroundColor: active === salonId ? 'orange' : '' }}>
            <p>{salonName}</p>
        </div>
    </div>
)


const mapStateToProps = (state) => ({
    active: state.salon
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onClick: () => {
        dispatch(getSalon(ownProps.salonId));
    }
})

Salon = connect(
    mapStateToProps,
    mapDispatchToProps
)(Salon)

export default Salon;
