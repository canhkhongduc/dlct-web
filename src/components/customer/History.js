import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import dateFormat from 'dateformat';
import moment from 'moment';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import { ToastContainer, toast } from 'mdbreact';
import Dialog from 'react-bootstrap-dialog'
import DayPicker from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/vi';
import 'react-day-picker/lib/style.css';
import ProfileSideNav from '../layout/ProfileSideNav';
import { getUserById, updateUserInformation } from '../../actions/user';
import DatePicker from "react-datepicker";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
 
import "react-datepicker/dist/react-datepicker.css";
import { getAllBookingsByCustomerId } from '../../actions/booking';
const cookies = new Cookies();

class History extends Component {
    constructor(props) {
        super(props);
        this.input = React.createRef();
        this.state = {
            user: [],
            updateUser: [],
            bookings: []

        };
    }


    
    componentWillMount = async() => {
        let userId = cookies.get("userId");
        let token = cookies.get("token");
        if(token) {
            await this.props.handleGetUserById(userId, token);
            if(this.props.authenticationReducer.user.length !== 0) {
                this.setState({
                    user: this.props.authenticationReducer.user
                })
            }
            await this.props.handleGetBookingsByUserId(userId, token);
            if(this.props.bookingReducer.bookings.length !== 0) {
                this.setState({
                    
                    bookings: this.props.bookingReducer.bookings
                });
                console.log(this.props.bookingReducer.bookings)
            }
        }
        
        

    }

    handleUpdateUserInformation = async(event) => {
        event.preventDefault();
        let phoneNumber = event.target.phoneNumber.value;
        let user = this.state.user;
        user.username = event.target.username.value;
        user.email = event.target.email.value;
        user.name = event.target.name.value;
        user.phoneNumber = event.target.phoneNumber.value;
        await this.props.handleUpdateUserInformation(user, cookies.get("token"));
        if(this.props.authenticationReducer.updateUser.length !== 0) {
            toast.success("Cập nhật thành công");
            this.setState({
                user: user
            })
        }
    }
    render() {
        const {user, bookings} = this.state;
        for(let i = 0; i < bookings.length; i++) {
            bookings[i].salonName = bookings[i].salon.name; 
        }
        const currentNav = "history-nav";
        return (
            <div>
                <Header/>
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <ProfileSideNav currentNav={currentNav}/>
                            <div className="col-md-8">
                                <div className="row margin-bottom">
                                    Lịch sử đặt lịch   
                                </div>
                                <form className="row filter-date-form">
                                    <div className="col-md-3">
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleChange}
                                            placeholderText="Từ ngày"
                                            className="filter-input-date"
                                        />
                                    </div>
                                    <div className="col-md-3">
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleChange}
                                            placeholderText="Đến ngày"
                                            className="filter-input-date"
                                        />
                                    </div>
                                    <div className="col-md-3">
                                        <select className="filter-booking-status">
                                            <option value="0">Tất cả</option>
                                            <option value="1">Hủy bởi salon</option>
                                            <option value="2">Hủy bởi khách</option>
                                            <option value="3">Chờ xử lý</option>
                                        </select>
                                    </div>
                                    <div className="col-md-3">
                                        <button type="button" class="btn btn-primary">Tìm kiếm</button>
                                    </div>
                                </form>
                                <div className="row">
                                    <BootstrapTable
                                        data={bookings}
                                        striped
                                        hover
                                        pagination>
                                        <TableHeaderColumn
                                            width={'32%'}
                                            dataField='id'
                                            editable={false}
                                            isKey
                                            >Mã</TableHeaderColumn>
                                        <TableHeaderColumn
                                            width={'28%'}
                                            dataField='time'
                                            editable={false}
                                           >Ngày đặt</TableHeaderColumn>
                                        <TableHeaderColumn 
                                        width={'20%'}
                                        dataField='salonName' 
                                        dataSort={true}
                                        >
                                           Phục vụ bởi
                                        </TableHeaderColumn>
                                        <TableHeaderColumn
                                            width={'22%'}
                                            dataField='status'
                                            >
                                            Trạng thái
                                        </TableHeaderColumn>
                                    </BootstrapTable>
                                </div>
                            </div>
                            
                        </div>
                        
                        
                    </div>
                </section>
                <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />  
            </div>
        );
    }

}


function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        cityReducer: state.cityReducer, 
        districtReducer: state.districtReducer, 
        scheduleReducer: state.scheduleReducer,
        authenticationReducer: state.authenticationReducer,
        bookingReducer: state.bookingReducer,
        unavailableSlotReducer: state.unavailableSlotReducer,
        bookingReducer: state.bookingReducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetUserById: async(id, token) => {
            await dispatch(getUserById(id, token));
        },
        handleUpdateUserInformation: async(user, token) => {
            await dispatch(updateUserInformation(user, token));
        },
        handleGetBookingsByUserId: async(id, token) => {
            await dispatch(getAllBookingsByCustomerId(id, token));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(History)