import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../layout/Header';
import Cookies from 'universal-cookie';
import dateFormat from 'dateformat';
import { getBookingsByCustomerId, updateBooking } from '../../actions/booking';
import { getCustomerByPhoneNumber } from '../../actions/customer';
import { fetchDataSalonById } from '../../actions/salon';
import Dialog from 'react-bootstrap-dialog'
import { fetchDataCityById } from '../../actions/city';
import { fetchDataDistrictById } from '../../actions/district';
const cookies = new Cookies();



Dialog.setOptions({
    defaultOkLabel: 'Cập nhật',
    primaryClassName: 'btn-primary',
  })


class BookingSuccessPage extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            customer: [],
            salon: [],
            booking: [],
            city: [],
            district: []

        };
        
    }
    componentWillMount = async (e) => {
        let customerPhoneNumber = cookies.get('phoneNumber');
        let role = cookies.get('role');
        if(role !== "customer"){
            window.location.href = "/home";
        } else {
            await this.props.handleGetCustomerByPhoneNumber(customerPhoneNumber);
            if(this.props.authenticationReducer.getCustomer.length !== 0) {         
                await this.props.handleGetBookingsByCustomerId(this.props.authenticationReducer.getCustomer[0].id);
                if(this.props.bookingReducer.bookings) {
                    if(this.props.bookingReducer.bookings.length !== 0){
                        await this.props.handleGetSalonById(this.props.bookingReducer.bookings[this.props.bookingReducer.bookings.length - 1].salonId);
                        if(this.props.salonReducer.salon) {
                            await this.props.handleFetchDistrictById(this.props.salonReducer.salon[0].districtId);
                            if(this.props.districtReducer.district) {
                                await this.props.handleFetchCityById(this.props.districtReducer.district[0].cityId);
                                if(this.props.cityReducer.city) {
                                    this.setState({
                                        customer: this.props.authenticationReducer.getCustomer[0],
                                        salon: this.props.salonReducer.salon[0],
                                        booking: this.props.bookingReducer.bookings[this.props.bookingReducer.bookings.length - 1],
                                        city: this.props.cityReducer.city[0],
                                        district: this.props.districtReducer.district[0]
                                    });
                                }
                            }
                            
                            let timezone = -(new Date().getTimezoneOffset() / 60);
                            let bookingTime = new Date(this.state.booking.time);
                            bookingTime.setHours(bookingTime.getHours() - timezone);
                            this.setState({
                                bookingDate: dateFormat(bookingTime, "dd/mm/yyyy")
                            });
                            if(bookingTime < new Date()){
                                this.dialog.show({
                                    title: 'Quá hạn',
                                    body: 'Lịch đặt của bạn đã quá hạn. Vui lòng chọn lịch đặt mới',
                                    actions: [
                                        Dialog.OKAction(this.dialogPositive)
                                      ],
                                    onHide: (dialog) => {
                                        dialog.hide()
                                        window.location.href = `/${this.state.salon.normalizedName}`;  
                                    }
                                });
                            }
                        }
                    } else {
                        window.location.href = "/#booking";
                    }
                } 
            }
        }
    }

    dialogPositive = async(e) => {
        window.location.href = `/${this.state.salon.normalizedName}`;  
    }

    handleChangeBooking = async(e) => {
        window.location.href = `/${this.state.salon.normalizedName}`;
    }

    handleCancelBooking = async(e) => {
        let newBooking = this.state.booking;
        newBooking.status = 2;
        await this.props.handleUpdateBooking(newBooking);
        if(this.props.bookingReducer.booking) {
            window.location.href = "/#booking";
        }
    }

    render() {
        const {customer, salon, booking, city, district, bookingDate} = this.state;
        let timezone = -(new Date().getTimezoneOffset() / 60);
        let bookingTime = new Date(booking.time);
        bookingTime.setHours(bookingTime.getHours() - timezone);
        let timeDifference = (bookingTime - (new Date())) / 1000;
        let days = Math.floor(timeDifference / 86400);
        timeDifference -= days * 86400;
        let hours = Math.floor(timeDifference / 3600) % 24;
        timeDifference -= hours * 3600;
        let minutes = Math.floor(timeDifference / 60) % 60;
        timeDifference -= minutes * 60;
        
        return (   
            <div>
                <Header />
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Anh {customer.name} đã đặt lịch thành công</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <p>Tiệm {salon.name}, {salon.address}, {district.name}, {city.name}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center">
                            
                                <p>Số điện thoại: <a href={`tel:${salon.phoneNumber}`} className="phone-number"> {salon.phoneNumber} <i className="fas fa-phone"></i></a></p>
                            </div>
                        </div>
                        
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <p><b>Lịch đặt</b>: {bookingTime.getHours() < 10 ? ("0" + bookingTime.getHours()) : bookingTime.getHours()}:{bookingTime.getMinutes() < 10 ? ("0" + bookingTime.getMinutes()) : bookingTime.getMinutes()}, {getDayOfWeek(bookingTime.getDay())}, ngày {bookingDate}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <p>Còn {days} ngày {hours} giờ {minutes} phút là đến lịch cắt tóc của bạn</p>
                            </div>
                        </div>

                        <div className="modal fade" id="cancel-booking-dialog" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Xác nhận</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                <p>Bạn có muốn đổi lịch cắt tóc không?</p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#cancel-booking-dialog" onClick={this.handleChangeBooking}>Đổi lịch</button>
                                    <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#cancel-booking-dialog" onClick={this.handleCancelBooking}>Hủy lịch</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="row text-center action-button-success">
                            <button type="button" className="btn btn-primary btn-back" onClick={this.handleChangeBooking}>Đổi lịch</button>
                            <button type="button" className="btn btn-primary btn-back" data-toggle="modal" data-target="#cancel-booking-dialog">Hủy lịch</button>
                        </div>
                    </div>
                    <Dialog ref={(component) => { this.dialog = component }} />
                </section>
            </div>
        );
    }
}

function getDayOfWeek(dayOfWeek) {
    switch (dayOfWeek) {
        case 1:
            return 'Thứ hai';
        case 2:
            return 'Thứ ba';
        case 3:
            return 'Thứ tư';
        case 4:
            return 'Thứ năm';
        case 5:
            return 'Thứ sáu';
        case 6:
            return 'Thứ bảy';
        case 0:
            return 'Chủ nhật';
        default:
            return '';
    }
}

function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer,
        bookingReducer: state.bookingReducer,
        authenticationReducer: state.authenticationReducer,
        cityReducer: state.cityReducer,
        districtReducer: state.districtReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetBookingsByCustomerId: async(customerId) => {
            await dispatch(getBookingsByCustomerId(customerId))
        },
        handleGetCustomerByPhoneNumber: async(phoneNumber) => {
            await dispatch(getCustomerByPhoneNumber(phoneNumber))
        },
        handleGetSalonById: async(id) => {
            await dispatch(fetchDataSalonById(id))
        },
        handleUpdateBooking: async(booking) => {
            await dispatch(updateBooking(booking))
        },
        handleFetchCityById: async(cityId) => {
            await dispatch(fetchDataCityById(cityId));
        },
        handleFetchDistrictById: async(districtId) => {
            await dispatch(fetchDataDistrictById(districtId));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingSuccessPage)