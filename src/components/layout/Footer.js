import React, {Component} from 'react'
import {connect} from 'react-redux'
class Footer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
    }

    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <span className="copyright">Bản quyền &copy; DatLichCatToc.com 2019</span>
                        </div>
                        <div className="col-md-4">
                            <ul className="list-inline social-buttons">
                                <li className="list-inline-item">
                                    <a href="mailto:datlichcattoc@gmail.com">
                                        <i className="fab fa-google"></i>
                                    </a>
                                </li>
                                <li className="list-inline-item">
                                    <a href="https://www.facebook.com/datlichcattoc/">
                                        <i className="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li className="list-inline-item">
                                    <a href="tel:0989284470">
                                        <i className="fas fa-phone"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-4">
                            <span className="copyright">All rights reserved.</span>
                        </div>
                    </div>
                    

                </div>
            </footer>
        )
    }
}

export default connect(null, null)(Footer)
