import React, {Component} from 'react';
import {fetchDataAllCitiesHasSalon} from '../../actions/city';
import {fetchDataDistrictsHasSalonByCityId} from '../../actions/district';
import {fetchDataSalonsByCityIdAndDistrictId, fetchDataSalonById, getAllSalons} from '../../actions/salon';
import {connect} from 'react-redux';
import HeaderHome from '../layout/HeaderHome'
import { getCustomerByPhoneNumber } from '../../actions/customer';
import { getAllBookingsByCustomerId } from '../../actions/booking';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCitiesHasSalon: [],
            allDistrictsHasSalon: [],
            allSalons: [],
            bookedSalons: [],
        };

    }

    componentWillMount = async(e) => {
        
        let bookedSalons = [], bookedSalonIds = [];
        await this.props.handleGetAllSalons();
        if(this.props.salonReducer.allSalons) {
            this.setState({
                allSalons: this.props.salonReducer.allSalons
            });
            console.log(this.props.salonReducer.allSalons);
        }
        await this
            .props
            .handleFetchDataAllCities();
        let role = cookies.get("role");
        let phoneNumber = cookies.get("phoneNumber");
        console.log(cookies.get("token"));
        
        if(role === "customer") {
            await this.props.handleGetCustomerByPhoneNumber(phoneNumber);
            if(this.props.authenticationReducer.getCustomer.length !== 0) {

                await this.props.handleGetAllBookingsByCustomerId(this.props.authenticationReducer.getCustomer[0].id);
                if(this.props.bookingReducer.bookings){
                    let pastBookings = this.props.bookingReducer.bookings;
                    for(let i = 0; i < pastBookings.length; i++) {
                        bookedSalonIds[i] = pastBookings[i].salonId;
                        
                    } 
                    bookedSalonIds = bookedSalonIds.filter(function(item, pos) {
                        return bookedSalonIds.indexOf(item) === pos;
                    });
                    for(let i = 0; i < bookedSalonIds.length; i++) {
                        await this.props.handleGetSalonById(bookedSalonIds[i]);
                        if(this.props.salonReducer.salon) {
                            bookedSalons[i] = this.props.salonReducer.salon[0];
                        }
                    }
                    if(bookedSalons.length !== 0) {
                        this.setState({
                            bookedSalons: bookedSalons
                        });
                        document.getElementById("booked-salons").classList.remove("invisible");
                    }
                    
                }
            }
        } else if(role === "salon") {
            window.location.href= "/salon/home";
        }
        this.setState({allCitiesHasSalon: this.props.cityReducer.allCitiesHasSalon});

    }

    handleChooseCity = async(cityId, cityName) => {
        if (cityId !== 0) {
            document
                .getElementById('district-dropdown')
                .disabled = false;
            document
                .getElementById('city-dropdown')
                .innerHTML = cityName;
            await this
                .props
                .handleFetchDataDistrictsByCityId(cityId);
            this.setState({allDistrictsHasSalon: this.props.districtReducer.allDistrictsHasSalon, allSalons: []})
        }
    }

    handleChooseDistrict = async(districtId, cityId, districtName) => {
        let noSalonMessage = document.getElementById("alert-no-salon");
        if (cityId !== 0 && districtId !== 0) {
            document
                .getElementById('district-dropdown')
                .innerHTML = districtName;
            await this
                .props
                .handleFetchDataSalonsByCityIdAndDistrictId(districtId, cityId);
            this.setState({allSalons: this.props.salonReducer.allSalons});
            if(this.props.salonReducer.allSalons[0]){
                noSalonMessage.classList.add("invisible");
            } else {
                noSalonMessage.innerHTML = "Khu vực bạn chọn chưa có tiệm cắt tóc nào. Tính năng tìm kiếm tiệm cắt tóc xung quanh sẽ được cập nhất trong thời gian tới";
                noSalonMessage.classList.remove("invisible");
            }
            
        }
    }

    handleChooseSalon = async(normalizedName) => {
        window.location.href = `/${normalizedName}`;
    }
    searchFocus = (e) => {
        document.getElementById("search").focus();
    }

    render() {
        const {allCitiesHasSalon, allDistrictsHasSalon, allSalons, bookedSalons} = this.state

        return (

            <div>
                <HeaderHome/>
                <section id="services">
                    <div className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                        <h2 className="section-heading text-uppercase">Tính năng chính</h2>
                        </div>
                    </div>
                    <div className="row text-center">
                        <div className="col-md-4" onClick={this.searchFocus}>
                        <span className="fa-stack fa-4x">
                            <i className="fas fa-circle fa-stack-2x text-primary"></i>
                            <i className="fas fa-search fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 className="service-heading">Tìm kiếm</h4>
                        </div>
                        <div className="col-md-4">
                        <span className="fa-stack fa-4x">
                            <i className="fas fa-circle fa-stack-2x text-primary"></i>
                            <i className="fas fa-star fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 className="service-heading">Đánh giá</h4>
                        </div>
                        <div className="col-md-4">
                        <span className="fa-stack fa-4x">
                            <i className="fas fa-circle fa-stack-2x text-primary"></i>
                            <i className="fas fa-calendar-check fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 className="service-heading">Đặt lịch</h4>
                        </div>
                    </div>
                    </div>
                </section>
                <section className="bg-light" id="portfolio">
                    <div className="container">
                        
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h2 className="section-heading text-uppercase">Salon phổ biến</h2>
                            </div>
                        </div>
                        <div className="row sort-bar">
                            <span className="sort-bar-label">
                                Sắp xếp theo
                            </span>
                            <div className="btn-group">
                                <button type="button" className="btn btn-primary">Phổ biến</button>
                                <button type="button" className="btn btn-secondary">Gần đây</button>
                                <button type="button" className="btn btn-secondary">Khuyến mại</button>
                            </div>
                        </div>
                        <div className="row">
                            {this.displaySalons(allSalons)}
                        </div>
                    </div>
                </section>
                <section className="bg-light" id="portfolio">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h2 className="section-heading text-uppercase">Thành phố phổ biến</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-sm-6 portfolio-item">
                                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                                    <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <i className="fas fa-info-circle fa-3x"></i>
                                    </div>
                                    </div>
                                    <img className="img-fluid" src="img/portfolio/01-thumbnail.jpg" alt="" />
                                </a>
                                <div className="portfolio-caption">
                                    <h4>Hà Nội</h4>
                                    <p className="text-muted">15 salons</p>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6 portfolio-item">
                                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                                    <div className="portfolio-hover">
                                        <div className="portfolio-hover-content">
                                            <i className="fas fa-info-circle fa-3x"></i>
                                        </div>
                                    </div>
                                    <img className="img-fluid" src="img/portfolio/02-thumbnail.jpg" alt="" />
                                </a>
                                <div className="portfolio-caption">
                                    <h4>TP Hồ Chí Minh</h4>
                                    <p className="text-muted">10 salon</p>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6 portfolio-item">
                                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                                    <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <i className="fas fa-info-circle fa-3x"></i>
                                    </div>
                                    </div>
                                    <img className="img-fluid" src="img/portfolio/03-thumbnail.jpg" alt="" />
                                </a>
                                <div className="portfolio-caption">
                                    <h4>TP Đà Nẵng</h4>
                                    <p className="text-muted">5 salon</p>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6 portfolio-item">
                                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                                    <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <i className="fas fa-info-circle fa-3x"></i>
                                    </div>
                                    </div>
                                    <img className="img-fluid" src="img/portfolio/04-thumbnail.jpg" alt="" />
                                </a>
                                <div className="portfolio-caption">
                                    <h4>TP Hải Phòng</h4>
                                    <p className="text-muted">20 salon</p>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6 portfolio-item">
                                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                                    <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <i className="fas fa-info-circle fa-3x"></i>
                                    </div>
                                    </div>
                                    <img className="img-fluid" src="img/portfolio/05-thumbnail.jpg" alt="" />
                                </a>
                                <div className="portfolio-caption">
                                    <h4>TP Cần Thơ</h4>
                                    <p className="text-muted">20 salon</p>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6 portfolio-item">
                                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                                    <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <i className="fas fa-info-circle fa-3x"></i>
                                    </div>
                                    </div>
                                    <img className="img-fluid" src="img/portfolio/06-thumbnail.jpg" alt="" />
                                </a>
                                <div className="portfolio-caption">
                                    <h4>Window</h4>
                                    <p className="text-muted">Photography</p>
                                </div>
                            </div>
                    </div>
                    </div>
                </section>
                <section id="booked-salons" className="bg-light invisible">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Salon bạn đã đặt</h3>
                            </div>
                        </div>
                        
                        <div className="row list-salons">
                            {this.displaySalons(bookedSalons)}
                        </div>
                    </div>
                </section>
            </div>
        );
    }
    displayCities = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <li>
                    <span
                        className="dropdown-item"
                        onClick={() => this.handleChooseCity(element.id, element.name)}
                        key={index}>{element.name}</span>
                </li>
            ))
        }
        return result
    }
    displayDistricts = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <span
                    className="dropdown-item"
                    onClick={() => this.handleChooseDistrict(element.id, element.cityId, element.name)}
                    key={index}>{element.name}</span>
            ))
        }
        return result
    }
    displaySalons = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div className="col-md-4 col-sm-6 portfolio-item" onClick={() => this.handleChooseSalon(element.normalizedName)}>
                    <a className="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                        <div className="portfolio-hover">
                        <div className="portfolio-hover-content">
                            <i className="fas fa-info-circle fa-3x"></i>
                        </div>
                        </div>
                        <img className="img-fluid" src={`img/about/${element.profileUrl ? element.profileUrl : 'default-image.png'}`} alt="" />
                    </a>
                    <div className="portfolio-caption">
                        <h4>{element.name}</h4>
                        <p className="text-muted">{element.address}</p>
                    </div>
                </div>
            ))
        }
        return result
    }
}

function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        cityReducer: state.cityReducer, 
        districtReducer: state.districtReducer,
        authenticationReducer: state.authenticationReducer,
        bookingReducer: state.bookingReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleFetchDataAllCities: async() => {
            await dispatch(fetchDataAllCitiesHasSalon())
        },
        handleFetchDataDistrictsByCityId: async(cityId) => {
            await dispatch(fetchDataDistrictsHasSalonByCityId(cityId))
        },
        handleFetchDataSalonsByCityIdAndDistrictId: async(districtId, cityId) => {
            await dispatch(fetchDataSalonsByCityIdAndDistrictId(districtId, cityId))
        },
        handleGetCustomerByPhoneNumber: async(phoneNumber) => {
            await dispatch(getCustomerByPhoneNumber(phoneNumber))
        },
        handleGetAllBookingsByCustomerId: async(customerId) => {
            await dispatch(getAllBookingsByCustomerId(customerId))
        },
        handleGetSalonById: async(id) => {
            await dispatch(fetchDataSalonById(id))
        },
        handleGetAllSalons: async() => {
            await dispatch(getAllSalons());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)