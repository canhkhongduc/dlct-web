import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';


export const ADMIN_AUTHENTICATE_SUCCESS = 'ADMIN_AUTHENTICATE_SUCCESS';

export function authenticateSuccess(data) {
    return {type: ADMIN_AUTHENTICATE_SUCCESS, data};
}

export function adminAuthenticate(credentials) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.adminAuthenticate, 'POST', credentials)
            dispatch(authenticateSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

