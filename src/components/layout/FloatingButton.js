import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Container, Button, Link } from 'react-floating-action-button'
class FloatingButton extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
    }
    showNotifications = (e) => {
        document.getElementById("noti-popup").style.display = "block";
    }
    hideNotifications = (e) => {
        document.getElementById("noti-popup").style.display = "none";
    }

    render() {
        return (
            <div className="notification" onMouseOut={this.hideNotifications}>
            <div onMouseOver={this.showNotifications}>
            <Container className="bubble">
                <Button
                    className="bell-float-button"
                    icon="fas fa-bell"
                     />
            </Container>
            </div>
            <div className="popup" id="noti-popup" onMouseOver={this.showNotifications}>
                <div className="popup-header">
                Thông báo
                </div>
                <div className="err-container"></div>
                <div className="popup-container">
                    <div className="item feedback">
                        <i className="fas fa-tag"></i>
                        <div className="text"><a>Salon Thùy Dung giảm giá 30% cho dịch vụ cắt tóc nam từ ngày 27/4 - 08/5</a>
                        </div>
                    </div>
                    <div className="item feedback">
                        <i className="fas fa-tag"></i>
                        <div className="text"><a>Salon Thùy Dung giảm giá 30% cho dịch vụ cắt tóc nam từ ngày 27/4 - 08/5</a>
                        </div>
                    </div>
                    <div className="item feedback">
                        <i className="fas fa-tag"></i>
                        <div className="text"><a>Salon Thùy Dung giảm giá 30% cho dịch vụ cắt tóc nam từ ngày 27/4 - 08/5</a>
                        </div>
                    </div>
            </div>
            <div className="popup-tail-shadow"></div>
            <div className="popup-tail-glow"></div>
            <div className="popup-tail"></div></div>

            </div>
            
        )
    }
}

export default connect(null, null)(FloatingButton)
