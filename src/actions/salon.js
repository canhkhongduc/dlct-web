import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const GET_SALONS = 'GET_SALONS';
export const GET_SALONS_SUCCESS = 'GET_SALONS_SUCCESS';

export const POSTING_SALON = 'POSTING_SALON';
export const POSTING_SALON_SUCCESS = 'POSTING_SALON_SUCCESS';

export const GET_SALON_BY_ID = 'GET_SALON_BY_ID';
export const GET_SALON_BY_ID_SUCCESS = 'GET_SALON_BY_ID_SUCCESS';

export const SALON_AUTHENTICATE = 'SALON_AUTHENTICATE';
export const SALON_AUTHENTICATE_SUCCESS = 'SALON_AUTHENTICATE_SUCCESS';

export const GET_SALON_BY_PHONE_NUMBER = 'GET_SALON_BY_PHONE_NUMBER';
export const GET_SALON_BY_PHONE_NUMBER_SUCCESS = 'GET_SALON_BY_PHONE_NUMBER_SUCCESS';

export const GET_SALON_BY_NORMALIZED_NAME = 'GET_SALON_BY_NORMALIZED_NAME';

export function fetchingSalonsByCityIdAndDistrictId() {
    return {type: GET_SALONS};
}
export function fetchingSalonsByCityIdAndDistrictIdSuccess(data) {
    return {type: GET_SALONS_SUCCESS, data};
}

export function postingSalon() {
    return {type: POSTING_SALON};
}
export function postingSalonSuccess(data) {
    return {type: POSTING_SALON_SUCCESS, data};
}

export function fetchingSalonById() {
    return {type: GET_SALON_BY_ID};
}

export function fetchingSalonByIdSuccess(data) {
    return {type: GET_SALON_BY_ID_SUCCESS, data};
}

export function fetchingSalonByPhoneNumber() {
    return {type: GET_SALON_BY_PHONE_NUMBER};
}
export function fetchingSalonByPhoneNumberSuccess(data) {
    return {type: GET_SALON_BY_PHONE_NUMBER_SUCCESS, data};
}

export function authenticate() {
    return {type: SALON_AUTHENTICATE};
}
export function authenticateSuccess(data) {
    return {type: SALON_AUTHENTICATE_SUCCESS, data};
}

export function getSalon(type, data) {
    return {type: type, data};
}

export function salonAuthenticate(credentials) {
    return async(dispatch) => {
        dispatch(authenticate());
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.authenticate, 'POST', credentials)
            dispatch(authenticateSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function fetchDataSalonsByCityIdAndDistrictId(districtId, cityId) {
    return async(dispatch) => {
        dispatch(fetchingSalonsByCityIdAndDistrictId())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getSalonsByCityIdAndDistrictId(districtId, cityId), 'GET')
            dispatch(fetchingSalonsByCityIdAndDistrictIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getAllSalons() {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getAllSalons, 'GET')
            dispatch(fetchingSalonsByCityIdAndDistrictIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function fetchDataSalonById(salonId) {
    return async(dispatch) => {
        dispatch(fetchingSalonById());
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getSalonById(salonId), 'GET');
            dispatch(fetchingSalonByIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err);
        }
    }
}

export function getSalonByNormalizedName(normalizedName) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getSalonByNormalizedName(normalizedName), 'GET');
            dispatch(getSalon(GET_SALON_BY_NORMALIZED_NAME, JSON.stringify(res)));
        } catch (err) {
            // console.log(err);
        }
    }
}

export function postDataSalon(data) {
    return async(dispatch) => {
        dispatch(postingSalon());
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.requestToBeSalon, 'POST', data)
            dispatch(postingSalonSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function updateSalon(salon) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.requestToBeSalon, 'PUT', salon)
            dispatch(postingSalonSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getSalonByPhoneNumber(phoneNumber) {
    return async(dispatch) => {
        dispatch(fetchingSalonByPhoneNumber())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getSalonByPhoneNumber(phoneNumber), 'GET')
            dispatch(fetchingSalonByPhoneNumberSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}
