import React, {Component} from 'react'
import {connect} from 'react-redux'
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
const cookies = new Cookies();
class ProfileSideNav extends Component {

    constructor(props) {
        super(props);
 
        this.state = {
          isVisible: false,
          currentNav: this.props.currentNav
        };
    }

    componentWillMount = async(e) => {
        
        
    }
    componentDidMount = async(e) => {
        let path = window.location.href.replace("http://localhost:3001/account/", "");

        document.querySelectorAll('.profile-nav-item').forEach((nav) => {
            nav.classList.remove("current-profile-nav");
        });
        console.log(this.state.currentNav);
        document.getElementById(`${this.state.currentNav}`).classList.add("current-profile-nav");

    }
    
    clickProfile = (e) => {
        document.querySelectorAll('.profile-nav-item').forEach((nav) => {
            nav.classList.remove("current-profile-nav");
        });
        
    }

    
    clickLogout = (e) => {
        
    }
    render() {
        return (
            <div className="col-md-3">
                <NavLink className="row profile-nav-item current-profile-nav" id="profile-nav" data-target="#link1" to={{pathname: '/account/profile'}}>
                    <div className="col-md-1">
                        <i class="fas fa-user-circle"></i>
                    </div>
                    <div className="col-md-10">
                        Trang cá nhân
                    </div>
                </NavLink>
                <NavLink className="row profile-nav-item" data-target="#link1" id="notification-nav" to={{pathname: '/account/notification'}}>
                    <div className="col-md-1">
                    <i class="fas fa-bell"></i>
                    </div>
                    <div className="col-md-10">
                        Thông báo của tôi
                    </div>
                </NavLink>
                <NavLink className="row profile-nav-item" id="history-nav" data-target="#link1" to={{pathname: '/account/history'}}>
                    <div className="col-md-1">
                        <i class="fas fa-calendar-alt"></i>
                    </div>
                    <div className="col-md-10">
                        Lịch sử đặt lịch
                    </div>
                </NavLink>
                <NavLink className="row profile-nav-item" id="favorite-nav" data-target="#link1" to={{pathname: '/account/favorite'}}>
                    <div className="col-md-1">
                        <i class="fas fa-heart"></i>
                    </div>
                    <div className="col-md-10">
                        Yêu thích
                    </div>
                </NavLink>
                <NavLink className="row profile-nav-item" id="password-nav" data-target="#link1" to={{pathname: '/account/change-password'}}>
                    <div className="col-md-1">
                        <i class="fas fa-cog"></i>
                    </div>
                    <div className="col-md-10">
                        Đổi mật khẩu
                    </div>
                </NavLink>
                <div className="row profile-nav-item" onClick={this.clickLogout}>
                    <div className="col-md-1">
                        <i class="fas fa-calendar-alt"></i>
                    </div>
                    <div className="col-md-10">
                        Đăng xuất
                    </div>
                </div>
                
            </div>
        )
    }
}

export default connect(null, null)(ProfileSideNav)
