import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import dateFormat from 'dateformat';
import moment from 'moment';
import Dialog from 'react-bootstrap-dialog'
import Carousel from 'react-bootstrap/Carousel';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,
} from "react-simple-maps"
import { getSalonByNormalizedName } from '../../actions/salon';

class BlogDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
           
    }

    componentWillMount = async() => {
        
    }
    
    render() {
        return (
            <div>
                <Header />
                <section id="blog-zone">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8">
                                <h1>aaaaaaaaaaaaaaaaaaaaa</h1>
                            </div>
                            <div className="col-md-4 popular-blogs">
                                <div className="popular-blog-label">
                                    Bài viết tiêu biểu
                                </div>
                                <div className="row popular-blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                    </div>
                                </div>
                                <div className="row popular-blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                    </div>
                                </div>
                                <div className="row popular-blog-item">
                                    <div className="col-md-4">
                                        <img src="img/about/1.jpg" className="popular-blog-image"/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <a className="blog-title">QUẢN LÝ CỬA HÀNG : BẠN CHỌN CHUYÊN NGHIỆP HAY NGHIỆP DƯ?</a>
                                        </div>
                                        <div className="row blog-date">
                                            28-02-2019 15:31
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetSalonByNormalizedName: async(salonNormalizedName) => {
            await dispatch(getSalonByNormalizedName(salonNormalizedName));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BlogDetail)