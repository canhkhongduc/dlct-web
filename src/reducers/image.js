import {
    POSTING_IMAGE_SUCCESS
} from '../actions/image'

const initialState = {
    image: [],
    isFetching: false
}

export default function imageReducer(state = initialState, action) {
    switch (action.type) {
        case POSTING_IMAGE_SUCCESS:
            return {
                ...state,
                image: JSON.parse(action.data),
                isFetching: false
            }
        default:
            return state;
    }
}
