import {combineReducers} from 'redux';
import salonReducer from './salon';
import cityReducer from './city';
import districtReducer from './district'
import authenticationReducer from './authentication';
import scheduleReducer from './schedule';
import bookingReducer from './booking';
import unavailableSlotReducer from './unavailableSlot';
import adminReducer from './admin';
import search from './search';
import reviewReducer from './review';
import serviceReducer from './service';
import imageReducer from './image';


const rootReducer = combineReducers({
    salonReducer, 
    authenticationReducer, 
    cityReducer, 
    districtReducer, 
    scheduleReducer, 
    bookingReducer,
    unavailableSlotReducer,
    adminReducer,
    search,
    reviewReducer,
    serviceReducer,
    imageReducer
});

export default rootReducer;
