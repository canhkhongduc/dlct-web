import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import Cookies from 'universal-cookie';
import { adminAuthenticate } from '../../actions/admin';
const cookies = new Cookies();

class AdminLoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: [],
            loginSuccess: true,
        };

    }
    componentWillMount = async(e) => {
    }


    handleLogin = async(event) => {
        event.preventDefault();
        let username = event.target.username.value;
        let password = event.target.password.value;
        let credentials = {
            username: username,
            password: password
        };
        await this.props.handleAdminAuthenticate(credentials);
        if(this.props.adminReducer.token.length !== 0){
            cookies.set('token', this.props.adminReducer.token, {
                maxAge: 24 * 3600,
                path: '/'
            });
            cookies.set('role', "admin", {
                maxAge: 24 * 3600,
                path: '/'
            });
            cookies.set('username', username, {
                maxAge: 24 * 3600,
                path: '/'
            });
            window.location.href=`/admin/salon`;
        } else {
            this.setState({
                loginSuccess: false
            });
        }
    }
    render() {
        const {loginSuccess} = this.state
        return (
            <div>
                <Header/>
                <section id="login">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Admin login</h3>
                            </div>
                        </div>
                        <form className="salon-login-form" onSubmit={this.handleLogin}>
                            <div className="form-group customer-register">
                                <label htmlFor="username">Username</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="username"
                                    name="username"
                                    required/>
                                <div className="invalid-feedback">
                                    Vui lòng điền tài khoản
                                </div>
                            </div>
                           
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền mật khẩu
                                </div>
                            </div>
                            
                            
                            <div className={loginSuccess? "alert alert-danger invisible":"alert alert-danger"}>
                                Số điện thoại hoặc mật khẩu của bạn không đúng.
                            </div>
                            
                            
                            <div className="row action-button">
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-salon-continue"
                                    >Tiếp tục</button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {adminReducer: state.adminReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleAdminAuthenticate: async(credentials) => {
            await dispatch(adminAuthenticate(credentials))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminLoginPage)