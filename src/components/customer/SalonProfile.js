import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import dateFormat from 'dateformat';
import moment from 'moment';
import Dialog from 'react-bootstrap-dialog'
import Carousel from 'react-bootstrap/Carousel';
import Lightbox from 'react-image-lightbox';

import 'react-image-lightbox/style.css';

import { NavLink } from 'react-router-dom';
import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,
} from "react-simple-maps"
import { getSalonByNormalizedName } from '../../actions/salon';
import { getReviewsBySalonId, postNewReview } from '../../actions/review';
import { fetchDataDistrictById } from '../../actions/district';
import { fetchDataCityById } from '../../actions/city';
import StarRatings from 'react-star-ratings';
import { getServicesBySalonId } from '../../actions/service';
import Cookies from 'universal-cookie';
import { ToastContainer, toast } from 'mdbreact';
import { uploadImage } from '../../actions/image';
const cookies = new Cookies();

const slideImages = [
    'img/about/1.jpg',
    'img/about/2.jpg',
    'img/about/3.jpg'
  ];
const properties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: false,
    indicators: true,
    arrows: true,
    scale: 0.5
  }

class SalonProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photoIndex: 0,
            isOpen: false,
            salon: [],
            district: [],
            city: [],
            allReviews: [],
            services: [],
            displayingReviews: [],
            chosenServices: [],
            rating: 1,
            avgRating: 0,
            numberOfReviews: 0,
            fivePropotion: 0,
            fourPropotion: 0,
            threePropotion: 0,
            twoPropotion: 0, 
            onePropotion: 0,
            currentRating: 0,
            uploadImages: [],
            image: [],
            listImages: []
        };
    }

    componentWillMount = async() => {
        console.log(cookies.get('token'));
        const {salonNormalizedName} = this.props.match.params;
        await this.props.handleGetSalonByNormalizedName(salonNormalizedName);
        let salon = this.props.salonReducer.salon[0];
       
        await this.props.handleGetCityById(salon.district.cityId);
        await this.props.handleGetReviewsBySalonId(salon.id);
        
        let allReviews = this.props.reviewReducer.reviews;
        await this.props.handleGetServicesBySalonId(salon.id);
        let sum = 0;
        let fiveStars = 0, fourStars = 0, threeStars = 0, twoStars = 0, oneStar = 0;
        for(let i = 0; i < allReviews.length; i++) {
            if(allReviews[i]['rating'] == 5) {
                fiveStars++;
            } else if(allReviews[i]['rating'] == 4) {
                fourStars++;
            } else if(allReviews[i]['rating'] == 3) {
                threeStars++;
            } else if(allReviews[i]['rating'] == 2) {
                twoStars++;
            } else {
                oneStar++;
            }
            sum += allReviews[i]['rating'];
        }

        this.setState({
            allReviews: allReviews,
            displayingReviews: allReviews,
            avgRating: sum / allReviews.length,
            numberOfReviews: allReviews.length,
            fivePropotion: fiveStars / allReviews.length * 100,
            fourPropotion: fourStars / allReviews.length * 100,
            threePropotion: threeStars / allReviews.length * 100,
            twoPropotion: twoStars / allReviews.length * 100,
            onePropotion: oneStar / allReviews.length * 100,
            district: salon.district,
            city: this.props.cityReducer.city[0],
            salon: salon,
            normalizedName: salonNormalizedName,
            services: this.props.serviceReducer.services,
        });
        
        if(cookies.get("chosenServices")) {
            this.setState({
                chosenServices: cookies.get("chosenServices")
            });
            if(cookies.get("chosenServices").length > 0) {
                document.getElementById("btnFloating").style.display = "block";
            }
        }
        
        
        
        
    }
    
    handleChooseService = async(service) => {
        if(cookies.get("signedIn")) {
            let chosenServices = this.state.chosenServices;
            chosenServices.push(service);
            this.setState({
                chosenServices: chosenServices
            });
            document.getElementById(`btn-choose-${service.serviceId}`).style.display = "none";
            let unchooseBtn = document.getElementById(`btn-unchoose-${service.serviceId}`);
            unchooseBtn.classList.remove("invisible");
            
            if(this.state.chosenServices.length !== 0) {
                document.getElementById("btnFloating").style.display = "block";
            } else {
                document.getElementById("btnFloating").style.display = "none";
            }
            cookies.set('chosenServices', this.state.chosenServices, {
                maxAge: 24 * 3600,
                path: '/'
            });
        } else {
            cookies.set('previousPath', `/${this.state.normalizedName}`, {
                maxAge: 24 * 3600,
                path: '/'
            });
            this.props.history.push("/login");
            // window.location.href = ;
        }
        
    }
    handleUnChooseService = async(service) => {
        let chosenServices = this.state.chosenServices;
        for(let i = 0; i < chosenServices.length; i++) {
            if(chosenServices[i].serviceId == service.serviceId) {
                chosenServices.splice(i, 1);
            }
        }
        this.setState({
            chosenServices: chosenServices
        });
        if(this.state.chosenServices.length !== 0) {
            document.getElementById("btnFloating").style.display = "block";
        } else {
            document.getElementById("btnFloating").style.display = "none";
        }
        document.getElementById(`btn-choose-${service.serviceId}`).style.display = "block";
        let unchooseBtn = document.getElementById(`btn-unchoose-${service.serviceId}`);
        unchooseBtn.classList.add("invisible");
        cookies.set('chosenServices', this.state.chosenServices, {
            maxAge: 24 * 3600,
            path: '/'
        });
    }
    handleOpenReview = (e) => {
        if(!cookies.get("signedIn")) {
            cookies.set('previousPath', `/${this.state.normalizedName}`, {
                maxAge: 24 * 3600,
                path: '/'
            });
            this.props.history.push("/login");
        }
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({rating: nextValue});
    }
    handleChangeImage = async(e) => {
        let num = 4;
        let files = e.target.files;
        console.log(files[0]);
        this.setState({
            uploadImages: files
        })
        let token = cookies.get('token');
        let images = this.state.listImages;
        for(let i = 0; i < files.length; i++) {
            await this.props.handleUploadReviewImage(files[i], token);
            if(this.props.imageReducer.image.length !== 0) {
                console.log(this.props.imageReducer.image);
                images.push(this.props.imageReducer.image);
            }
        }
        let previewZone = document.getElementById("preview-images-zone");
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            
            if (!file.type.match('image')) continue;
            
            var picReader = new FileReader();
            
            picReader.addEventListener('load', function (event) {
                var picFile = event.target;
                let container = document.createElement('div');
                container.classList.add("preview-images-zone");
                var html =  `<div class="preview-image preview-show-` + num + `">` +
                            `<div class="image-cancel" data-no="` + num + `">x</div>` +
                            `<div class="image-zone"><img id="pro-img-` + num + `" src="` + picFile.result + `"></div>` +
                            `</div>`;
                container.innerHTML = html;
                previewZone.appendChild(container);
                num = num + 1;
            });

            picReader.readAsDataURL(file);
        }
    }

    filterReviewStar = (star) => {
        let displayReviews = [];
        if(star == 0) {
            this.setState({
                displayingReviews: this.state.allReviews
            })
        } else {
            for (let i = 0; i < this.state.allReviews.length; i++) {
                if(this.state.allReviews[i].rating == star) {
                    displayReviews.push(this.state.allReviews[i]);
                }
            }
            this.setState({ displayingReviews: displayReviews});
        }
    }

    handleCreateNewReview = async(event) => {
        event.preventDefault();
        let title = event.target.title.value;
        let comment = event.target.comment.value; 
        let rating = this.state.currentRating;
        let images = this.state.uploadImages;

        if(rating == 0) {
            toast.error("Vui lòng chọn số sao");
        } else {
            
            // let review = {
            //     title: title,
            //     comment: comment,
            //     rating: rating,
            //     status: 0,
            //     dlctUserId: cookies.get("userId"),
            //     salonId: this.state.salon.id
            // }
            // await this.props.handlePostNewReview(review);
            // if(this.props.reviewReducer.review.length !== 0) {
            //     toast.success("Bạn đã gửi đánh giá thành công");
            //     setTimeout(function() {
                    
            //     }, 1000);
            // }
            // window.location.href = `/${this.state.normalizedName}`;
        }
    }
    changeRating = (newRating, name) => {
        this.setState({
          currentRating: newRating
        });
    }

    
    
    render() {
        const { reviews, displayingReviews, photoIndex, isOpen, salon, avgRating, 
            numberOfReviews, fivePropotion, fourPropotion, threePropotion, 
            twoPropotion, onePropotion, city, district, services, chosenServices, currentRating } = this.state;
        const fiveStyle = {
            width: `${fivePropotion}%`
        };
        const fourStyle = {
            width: `${fourPropotion}%`
        }
        const threeStyle = {
            width: `${threePropotion}%`
        }
        const twoStyle = {
            width: `${twoPropotion}%`
        }
        const oneStyle = {
            width: `${onePropotion}%`
        }
        console.log(chosenServices);
        let totalPrice = 0;

        if(chosenServices) {
            for(let i = 0; i < chosenServices.length; i++) {
                totalPrice += chosenServices[i].price;
            }
        }
        return (
            <div>
                <Header/>
                <section id="salon-profile">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 salon-info">
                                <div className="row">
                                    <div className="salon-name">{salon.name}</div>
                                    <div className="salon-review-star">
                                        <StarRatings
                                        className="average-rating"
                                        starSpacing="0px"
                                        starRatedColor="#fed136"
                                        starWidthAndHeight="25px"
                                        rating={avgRating}
                                        numberOfStars={5}
                                        starDimension="20px"
                                        name='rating'
                                        />
                                        
                                    </div>
                                    <a href="#review" className="salon-review-point">
                                        {avgRating} ({numberOfReviews} đánh giá)
                                    </a>
                                </div>
                                <div className="row">
                                    <div className="salon-address">{salon.address}, {district.name}, {city.name} (1.2km)</div>
                                </div>
                                <div className="row">
                                    <div className="salon-phone-number">
                                        <p>Số điện thoại: 
                                            <a href={`tel:0989284470`} className="phone-number"> {salon.phoneNumber}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <button type="button" className="btn btn-primary btn-own-salon">Bạn là chủ salon này?</button>
                            </div>
                        </div>
                        <div className="row">
                        <div className="col-md-8 salon-images">
                            <Carousel>
                                <Carousel.Item>
                                    <img
                                    className="d-block w-100"
                                    src="img/about/1.jpg"
                                    alt="First slide"
                                    />
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img
                                    className="d-block w-100"
                                    src="img/about/1.jpg"
                                    alt="Third slide"
                                    />
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img
                                    className="d-block w-100"
                                    src="img/about/1.jpg"
                                    alt="Third slide"
                                    />
                                </Carousel.Item>
                                </Carousel>
                                <div className="salon-description">
                                    <div className="salon-section-title">Mô tả</div>
                                    <div className="salon-description-detail">{salon.description}</div>
                                </div>
                            </div>
                            <div className="col-md-4 services">
                            <div className="service-service-text">Chọn dịch vụ</div>
                                <div className="salon-service-list">
                                    {this.displayService(services, salon.allowBooking)}
                                    <button type="button" className={`btn btn-primary ${salon.allowBooking ? "invisible" : ""}`}>Yêu cầu tính năng đặt lịch</button>
                                </div>
                            </div>
                        </div>
                        <div className="customer-reviews" id="review">
                            <div className="row salon-section-title">Khách hàng nhận xét</div>
                            <div className="row">
                                <div className="col-md-3 average-review">
                                    <div className="row average-review-label">
                                        Đánh giá trung bình
                                    </div>
                                    <div className="row average-review-point">
                                        {avgRating}
                                    </div>
                                    <div className="row average-review-star">
                                        <StarRatings
                                            className="average-rating"
                                            starSpacing="0px"
                                            starRatedColor="#fed136"
                                            starWidthAndHeight="25px"
                                            rating={avgRating}
                                            numberOfStars={5}
                                            starDimension="40px"
                                            name='small-rating'
                                        />
                                    </div>
                                    <div className="row average-review-number">
                                        ({numberOfReviews} nhận xét)
                                    </div>
                                </div>
                                <div className="col-md-6 average-review">
                                    <div className="row star-rate">
                                        <div className="rate-label">5<span className="fa fa-star checked"></span></div>
                                        <div className="progress review-progress">
                                            <div className="progress-bar" role="progressbar" aria-valuenow="51" aria-valuemin="0" aria-valuemax="100" style={fiveStyle}>{fivePropotion}%</div>
                                        </div>
                                    </div>
                                    <div className="row star-rate">
                                        <div className="rate-label">
                                            4<span className="fa fa-star checked"></span>
                                        </div>
                                        <div className="progress review-progress">
                                            <div className="progress-bar" role="progressbar" aria-valuenow="51" aria-valuemin="0" aria-valuemax="100" style={fourStyle}>{fourPropotion}%</div>
                                        </div>
                                    </div>
                                    <div className="row star-rate">
                                        <div className="rate-label">3<span className="fa fa-star checked"></span></div>
                                        <div className="progress review-progress">
                                            <div className="progress-bar" role="progressbar" aria-valuenow="51" aria-valuemin="0" aria-valuemax="100" style={threeStyle}>{threePropotion}%</div>
                                        </div>
                                    </div>
                                    <div className="row star-rate">
                                        <div className="rate-label">2<span className="fa fa-star checked"></span></div>
                                        <div className="progress review-progress">
                                            <div className="progress-bar" role="progressbar" aria-valuenow="51" aria-valuemin="0" aria-valuemax="100" style={twoStyle}>{twoPropotion}%</div>
                                        </div>
                                    </div>
                                    <div className="row star-rate">
                                        <div className="rate-label">1<span className="fa fa-star checked"></span></div>
                                        <div className="progress review-progress">
                                            <div className="progress-bar" role="progressbar" aria-valuenow="51" aria-valuemin="0" aria-valuemax="100" style={oneStyle}>{onePropotion}%</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 average-review">
                                    <button type="button" className="btn btn-primary review-button" data-toggle="modal" data-target="#addReviewModal" onClick={this.handleOpenReview}>Viết nhận xét của bạn</button>
                                </div>
                            </div>
                        </div>
                        <div className="row review-filter">
                            <div className="col-md-3 review-filter-title">
                                Chọn xem nhận xét
                            </div>
                            <div className="col-md-3 review-filter-dropdown">
                                <div className="dropdown">
                                    <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Tất cả khách hàng
                                    </button>
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a className="dropdown-item" href="#">Tất cả khách hàng</a>
                                        <a className="dropdown-item" href="#">Khách đã đặt lịch online</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 review-filter-dropdown">
                                <div className="dropdown">
                                    <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Tất cả sao
                                    </button>
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a className="dropdown-item" onClick={() => this.filterReviewStar(0)}>Tất cả sao</a>
                                        <a className="dropdown-item" onClick={() => this.filterReviewStar(5)}>5 sao</a>
                                        <a className="dropdown-item" onClick={() => this.filterReviewStar(4)}>4 sao</a>
                                        <a className="dropdown-item" onClick={() => this.filterReviewStar(3)}>3 sao</a>
                                        <a className="dropdown-item" onClick={() => this.filterReviewStar(2)}>2 sao</a>
                                        <a className="dropdown-item" onClick={() => this.filterReviewStar(1)}>1 sao</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.displayReviews(displayingReviews)}
                    </div>
                </section>
                <div className="floating-button" id="btnFloating">
                    <div className="wrapper">
                        <div className="container">
                            <div className="row float-booking">
                                <div className="col-4">
                                    <div className="float-booking-services">
                                        {chosenServices? chosenServices.length : 0} dịch vụ
                                    </div>
                                </div>
                                <div className="col-2">
                                    <div className="float-booking-price">
                                        {totalPrice}K
                                    </div>
                                </div>
                                <div className="col-6">
                                    <NavLink className="btn btn-light" data-target="#link1" to={{pathname: `/${this.state.normalizedName}/date-time`}} activeClassName="active">Chọn thời gian</NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    className="modal fade"
                    id="addReviewModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content form-elegant">
                            <div className="text-center">
                                <h3
                                    className="modal-title w-100 dark-grey-text font-weight-bold my-3"
                                    id="myModalLabel">
                                    <strong>Viết nhận xét</strong>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </h3>
                            </div>
                           
                            <div className="modal-body mx-4">
                                <form onSubmit={this.handleCreateNewReview} id="insert-customer-form">
                                    <StarRatings
                                            rating={currentRating}
                                            changeRating={this.changeRating}
                                            className="average-rating"
                                            starSpacing="0px"
                                            starRatedColor="#fed136"
                                            starWidthAndHeight="25px"
                                            numberOfStars={5}
                                            starDimension="40px"
                                            name='small-rating'
                                            isSelectable={true}
                                            
                                        />
                                    <div className="form-group">
                                        <label htmlFor="review-title">Tiêu đề</label>
                                        <input type="text" className="form-control" name="title" id="review-title" required/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="comment">Nhận xét</label>
                                        <textarea className="form-control" name="comment" id="exampleFormControlTextarea1" rows="3" required></textarea>
                                    </div>
                                    
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlFile1">Thêm hình ảnh (tối đa 5 hình)
                                        <span className="btn-choose-image">Chọn hình</span>
                                        </label>
                                        <input type="file" className="form-control-file upload-file" id="exampleFormControlFile1" multiple onChange={this.handleChangeImage}/>
                                    </div>
                                    <div className="row" id="preview-images-zone">
                                    
                                    </div>
                                    <div className="text-center mb-3 margin-top">
                                        <button
                                            type="submit"
                                            className="btn btn-primary btn-block btn-rounded z-depth-1a">Lưu</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />  
                {isOpen && (
                <Lightbox
                    mainSrc={slideImages[photoIndex]}
                    nextSrc={slideImages[(photoIndex + 1) % slideImages.length]}
                    prevSrc={slideImages[(photoIndex + slideImages.length - 1) % slideImages.length]}
                    onCloseRequest={() => this.setState({ isOpen: false })}
                    onMovePrevRequest={() =>
                    this.setState({
                        photoIndex: (photoIndex + slideImages.length - 1) % slideImages.length,
                    })
                    }
                    onMoveNextRequest={() =>
                    this.setState({
                        photoIndex: (photoIndex + 1) % slideImages.length,
                    })
                    }
                />
                )}
            </div>
            
        );
    }
    displayReviews = (arr) => {
        let result = null;
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div className="row review-item">
                    <div className="col-md-3">
                        <div className="row">
                            <img src="img/about/1.jpg" alt="review" className="reviewer-image" />
                        </div>
                        <div className="row reviewer-name">
                            {element.dlctUser.name}
                        </div>
                        <div className="row review-time">
                            4 tháng trước
                        </div>
                    </div>
                    <div className="col-md-9">
                        <div className="row">
                            <div className="review-item-star">
                                <StarRatings
                                    starSpacing="0px"
                                    starRatedColor="#fed136"
                                    rating={element.rating}
                                    numberOfStars={5}
                                    starDimension="25px"
                                    name='rating'
                                />
                            </div>
                            {element.title}
                        </div>
                        <div className="row review-item-status">
                            {element.status == 2 ? "Đã đặt lịch qua NolaSalon" : ""}
                        </div>
                        <div className="row review-item-description">
                            {element.comment}
                        </div>
                        <div className="row review-item-description">
                            <a onClick={() => this.setState({ isOpen: true })}><img src="img/about/2.jpg" alt="review" className="review-image" /></a>
                            <img src="img/about/2.jpg" alt="review" className="review-image" />
                            <img src="img/about/2.jpg" alt="review" className="review-image" />
                        </div>
                    </div>
                </div>
            ))
        }
        return result;
    }

    displayService = (arr, allowBooking) => {
        let result = null;
        let chosenServices = cookies.get("chosenServices");
        if(!chosenServices) chosenServices = [];
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div className="service">
                    <div className="service-name">{element.service.name}</div>
                    <div className="service-detail">
                        <div className="row">
                            <div className="col-4">
                                <p className="service-price">{element.price}</p>
                            </div>
                            <div className="col-3">
                                <p className="service-duration">{element.duration} phút</p>
                            </div>
                            <div className={`col-5 ${allowBooking ? "" : "invisible"}`}>
                                <button type="button" className={`btn btn-primary ${this.includeChosenservice(chosenServices, element) ? "invisible" : ""}`} id={`btn-choose-${element.serviceId}`} onClick={() => this.handleChooseService(element)}>Chọn</button>
                                <button type="button" className={`btn btn-primary ${this.includeChosenservice(chosenServices, element) ? "" : "invisible"}`} id={`btn-unchoose-${element.serviceId}`} onClick={() => this.handleUnChooseService(element)}>Bỏ Chọn</button>
                            </div>
                        </div>
                    </div>
                </div>
            ))
        }
        return result;
    }

    includeChosenservice = (chosenServices, service) => {
        for(let i = 0; i < chosenServices.length; i++) {
            if(service.id === chosenServices[i].id) {
                return true;
            }
        }
        return false;
    }
}




function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        cityReducer: state.cityReducer, 
        districtReducer: state.districtReducer, 
        scheduleReducer: state.scheduleReducer,
        authenticationReducer: state.authenticationReducer,
        bookingReducer: state.bookingReducer,
        unavailableSlotReducer: state.unavailableSlotReducer,
        reviewReducer: state.reviewReducer,
        serviceReducer: state.serviceReducer,
        imageReducer: state.imageReducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetSalonByNormalizedName: async(salonNormalizedName) => {
            await dispatch(getSalonByNormalizedName(salonNormalizedName));
        },
        handleGetReviewsBySalonId: async(salonId) => {
            await dispatch(getReviewsBySalonId(salonId));
        },
        handleGetServicesBySalonId: async(salonId) => {
            await dispatch(getServicesBySalonId(salonId));
        },
        handleGetDistrictById: async(id) => {
            await dispatch(fetchDataDistrictById(id));
        },
        handleGetCityById: async(id) => {
            await dispatch(fetchDataCityById(id));
        },
        handlePostNewReview: async(review) => {
            await dispatch(postNewReview(review));
        },
        handleUploadReviewImage: async(image, token) => {
            await dispatch(uploadImage(image, token));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SalonProfile)