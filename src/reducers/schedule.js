import {RECEIVE_SCHEDULE, CREATE_SCHEDULE_SUCCESS} from '../actions/schedule';

const initialState = {};

export default function scheduleReducer(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_SCHEDULE:
            return {
                ...state,
                schedules: JSON.parse(action.schedules)
            }
        case CREATE_SCHEDULE_SUCCESS:
            return {
                ...state,
                schedule: JSON.parse(action.schedule)
            }
        default:
            return state;
    }
}