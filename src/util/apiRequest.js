import Cookies from 'universal-cookie'
import get from 'lodash/get'

const cookies = new Cookies()
const axios = require('axios')

export async function fetchWithToken(api, post = false, data = {}, formData = false) {
    const accessToken = get(cookies.get('token'), 'token', null)
    let option = {
        method: 'GET',
        url: api,
        headers: {
            'x-access-token': accessToken
        }
    }
    if (post) {
        if (formData) {
            option = {
                method: 'POST',
                url: api,
                headers: {
                    'x-access-token': accessToken
                },
                data
            }
        } else {
            option = {
                method: 'POST',
                url: api,
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': accessToken
                },
                data
            }
        }
    }
    const res = await axios(option)
    if (res.status === 200)
        return res.data
    const error = new Error(res.statusText)
    throw error
}

export async function requestWithoutToken(api, method, requestBody) {
    const option = {
        url: api,
        method: method
    };

    if (requestBody)
        option.data = requestBody;

    const response = await axios(option);

    if (response.status === 200 || response.status === 201)
        return response.data;

    throw new Error(response.statusText);
}

export async function requestWithToken(accessToken, api, method, requestBody, ) {
    console.log(accessToken);
    const option = {
        url: api,
        method: method,
        headers: {
            'Authorization': accessToken
        },
    };

    if (requestBody)
        option.data = requestBody;

    const response = await axios(option);

    if (response.status === 200 || response.status === 201)
        return response.data;

    throw new Error(response.statusText);
}

export async function uploadFile(accessToken, api, file) {
    const data = new FormData();
    data.append('file', file);
    const response = await axios.post(api, data, {
        headers: {
            'Authorization': accessToken
        }
    });
    return response.data;
}