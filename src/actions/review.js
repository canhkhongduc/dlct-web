import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const GET_REVIEWS_BY_SALONID = 'GET_REVIEWS_BY_SALONID';
export const GET_REVIEWS_BY_SALONID_SUCCESS = 'GET_REVIEWS_BY_SALONID_SUCCESS';

export const POSTING_REVIEW_SUCCESS = 'POSTING_REVIEW_SUCCESS';

export function fetchReviewsBySalonId() {
    return {type: GET_REVIEWS_BY_SALONID};
}
export function fetchReviewsBySalonIdSuccess(data) {
    return {type: GET_REVIEWS_BY_SALONID_SUCCESS, data};
}

export function postReviewSuccess(data) {
    return {type: POSTING_REVIEW_SUCCESS, data};
}

export function getReviewsBySalonId(salonId) {
    return async(dispatch) => {
        dispatch(fetchReviewsBySalonId());
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getReviewsBySalonId(salonId), 'GET')
            dispatch(fetchReviewsBySalonIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function postNewReview(review) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createNewReview, 'POST', review)
            dispatch(postReviewSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}