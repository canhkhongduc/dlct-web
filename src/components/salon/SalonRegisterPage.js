import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import { ToastContainer, toast } from 'mdbreact';
import {postDataSalon} from '../../actions/salon';
import {fetchDataAllCities} from '../../actions/city';
import {fetchDataDistrictsByCityId} from '../../actions/district';
import { NavLink } from 'react-router-dom';

class SalonRegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cityId: 0,
            districtId: 0,
            allCities: [],
            allDistricts: [],
            salon: [],
            registerSuccess: true,
            registerMessage: ''
        };

    }
    componentWillMount = async(e) => {
        await this
            .props
            .handleFetchDataAllCities()
        this.setState({allCities: this.props.cityReducer.allCities})
    }

    handleSubmit = async(event) => {
        event.preventDefault();
        let pattern = /^[a-zA-Z0-9\s]+$/;
        let name = event.target.salonName.value;
        
        let password = event.target.password.value;
        let confirmPassword = event.target.confirmPassword.value;

        let errorMessage = document.getElementsByClassName("alert")[0];
        if(pattern.test(name.normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace("Đ", "D"))){
            if(this.state.districtId === 0){
                errorMessage.innerHTML = "Vui lòng chọn quận/thành phố";
                errorMessage.classList.remove("invisible");
            } else {
                if (password === confirmPassword) {
                    let data = {
                        password: event.target.password.value,
                        name: event.target.salonName.value,
                        phoneNumber: event.target.salonPhoneNumber.value,
                        address: event.target.addressDetail.value,
                        districtId: this.state.districtId,
                        status: 0
                    }
                    await this
                        .props
                        .handlePostDataSalon(data);
                    if(this.props.salonReducer.salon.length !== 0){
                        this.setState({salon: this.props.salonReducer.salon});
                        toast.success("Đăng ký thành công. Vui lòng đăng nhập.");
                        setTimeout(function() {
                            window.location.href = `/salon/login`;
                        }, 1200);
                        
                    } else {
                        errorMessage.innerHTML = "Số điện thoại đã tồn tại.";
                        errorMessage.classList.remove("invisible");
                    }
                    
                } else {
                    errorMessage.innerHTML = "Mật khẩu và mật khẩu xác nhận không trùng khớp";
                    errorMessage.classList.remove("invisible");
                }
            }
        } else {
            errorMessage.innerHTML = "Tên tiệm của bạn không thể chứa ký tự đặc biệt";
            errorMessage.classList.remove("invisible");
        }
        

        

    }
    handleChooseCity = async(cityId, cityName) => {
        if (cityId !== 0) {
            document
                .getElementById('district-dropdown')
                .disabled = false;
            document
                .getElementById('district-dropdown')
                .innerHTML = "Quận/Huyện";
            document
                .getElementById('city-dropdown')
                .innerHTML = cityName;
            await this
                .props
                .handleFetchDataDistrictsByCityId(cityId);
            this.setState({cityId: cityId, allDistricts: this.props.districtReducer.allDistricts})
        }
    }

    handleChooseDistrict = async(districtId, cityId, districtName) => {
        if (cityId !== 0 && districtId !== 0) {
            document
                .getElementById('district-dropdown')
                .innerHTML = districtName;
            // await this.props.handleFetchDataSalonsByCityIdAndDistrictId(districtId,
            // cityId);
            this.setState({districtId: districtId, allSalons: this.props.salonReducer.allSalons})
        }
    }

    render() {
        const {allCities, allDistricts} = this.state
        return (
            <div>
                <Header/>
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Đăng ký trở thành tiệm cắt tóc</h3>
                            </div>
                        </div>
                        <form className="salon-register-form" onSubmit={this.handleSubmit}>
                            <div className="form-group customer-register">
                                <label htmlFor="phoneNumber">Số điện thoại *</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="phoneNumber"
                                    name="salonPhoneNumber"
                                    placeholder="0123 456 789"
                                    required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Mật khẩu *</label>
                                <input 
                                type="password" 
                                className="form-control" 
                                id="password"
                                name="password"
                                pattern=".{8,}" 
                                title="Mật khẩu phải nhiều hơn 8 ký tự"
                                required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="confirm-password">Xác nhận mật khẩu *</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="confirm-password"
                                    name="confirmPassword"
                                    required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="salon-name">Tên tiệm *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="salon-name"
                                    placeholder="ABC Hair Salon"
                                    name="salonName"
                                    required/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="address-detail">Địa chỉ chi tiết (Lưu ý: chỉ điền đến cấp xã, phường hoặc số nhà, tên đường)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="address-detail"
                                    placeholder="144 Xuân Thủy"
                                    name="addressDetail"
                                    required/>
                            </div>
                            <div className="row dropdown text-center salon-address-dropdown">
                                <div className="dropdown city-district-dropdown">

                                    <button
                                        className="btn btn-primary dropdown-toggle btn-block"
                                        id="city-dropdown"
                                        type="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">Tỉnh/Thành Phố</button>

                                    <ul className="dropdown-menu dropdown-primary">
                                        {this.displayCities(allCities)}
                                    </ul>
                                </div>

                                <div className="dropdown city-district-dropdown">
                                    <button
                                        className="btn btn-primary dropdown-toggle btn-block btn-district"
                                        type="button"
                                        id="district-dropdown"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        disabled>Quận/Huyện</button>
                                    <ul className="dropdown-menu dropdown-primary">
                                        {this.displayDistricts(allDistricts)}
                                    </ul>
                                </div>
                            </div>
                            <div className="alert alert-danger invisible">
                                
                            </div>
                            <div className="row action-button">
                                <button type="submit" className="btn btn-primary btn-salon-continue">Tiếp tục</button>
                            </div>
                        </form>
                        <div className="row text-center">
                            <p className="text-center">Đã có tài khoản?
                            <NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/login'}} activeClassName="">Đăng nhập ngay</NavLink>
                            </p>
                        </div>
                    </div>
                    <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />
                </section>
            </div>
        );
    }
    displayCities = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <li>
                    <a
                        className="dropdown-item"
                        onClick={() => this.handleChooseCity(element.id, element.name)}
                        key={index}>{element.name}</a>
                </li>
            ))
        }
        return result
    }
    displayDistricts = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <li>
                    <a
                        className="dropdown-item"
                        onClick={() => this.handleChooseDistrict(element.id, element.cityId, element.name)}
                        key={index}>{element.name}</a>
                </li>
            ))
        }
        return result
    }
}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer, cityReducer: state.cityReducer, districtReducer: state.districtReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleFetchDataAllCities: async() => {
            await dispatch(fetchDataAllCities())
        },
        handleFetchDataDistrictsByCityId: async(cityId) => {
            await dispatch(fetchDataDistrictsByCityId(cityId))
        },
        handlePostDataSalon: async(data) => {
            await dispatch(postDataSalon(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SalonRegisterPage)