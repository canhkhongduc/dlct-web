import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import {getSalonByNormalizedName} from '../../actions/salon';
import {fetchDataCityById} from '../../actions/city';
import {fetchDataDistrictById} from '../../actions/district';
import dateFormat from 'dateformat';
import moment from 'moment';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import { getCustomerByPhoneNumber } from '../../actions/customer';
import { createBooking, getBookingsByCustomerId, updateBooking, getOpenBookingsBySalonIdAndDate, createBookingService } from '../../actions/booking';
import Dialog from 'react-bootstrap-dialog'
import { getUnavailableSlotsBySalonId } from '../../actions/unavailableSlot';
import DayPicker from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/vi';
import 'react-day-picker/lib/style.css';
import { getSchedulesBySalonIdAndWeekDay } from '../../actions/schedule';
const cookies = new Cookies();


const MONTHS = [
    'Tháng Một',
    'Tháng Hai',
    'Tháng Ba',
    'Tháng Tư',
    'Tháng Năm',
    'Tháng Sáu',
    'Tháng Bảy',
    'Tháng Tám',
    'Tháng Chín',
    'Tháng Mười',
    'Tháng Mười Một',
    'Tháng Mười Hai',
  ];
  const WEEKDAYS_LONG = [
    'Chủ Nhật',
    'Thứ Hai',
    'Thứ Ba',
    'Thứ Tư',
    'Thứ Năm',
    'Thứ Sáu',
    'Thứ Bảy',
  ];
const WEEKDAYS_SHORT = ['C', 'H', 'B', 'T', 'N', 'S', 'B'];

class SlotPage extends Component {
    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.state = {
            salon: [],
            city: [],
            district: [],
            today: new Date(),
            chosenSlot: 0,
            normalizedName: '',
            customer: [],
            bookings: [],
            salonBookings: [],
            salonUnavailableSlots: [],
            locale: 'vi',
            chosenServices: [],
            slots: [],
            chosenDay: new Date(),

        };
    }


    handleDayClick(day) {
        let today = new Date();
        today.setHours(0,0,0,0);
        if(day >= today) {
            this.setState({ chosenDay: day });
        }
        this.checkPastSlots(this.state.slots, day);
    }
    componentWillMount = async() => {
        const {salonNormalizedName} = this.props.match.params;
        await this.props.handleGetSalonByNormalizedName(salonNormalizedName);
        let salon = this.props.salonReducer.salon[0];
        await this.props.handleGetSchedulesBySalonIdAndWeekDay(salon.id, new Date().getDay())
        
        let schedule = this.props.scheduleReducer.schedules[0];
        let slots = scheduleToSlots(schedule);
        this.checkPastSlots(slots, new Date());
        let chosenServices = cookies.get("chosenServices");
        this.setState({
            chosenServices: chosenServices,
            slots: slots,
            normalizedName: salonNormalizedName,
            salon: salon
        });
    }

    checkPastSlots = (slots, date) => {
        for( let i = 0; i < slots.length; i++) {
            let slotDate = new Date(`${dateFormat(date, "yyyy-mm-dd")} ${slots[i].time}`);
            if(slotDate < new Date()) {
                slots[i].past = true;
            } else {
                slots[i].past = false;
            }
        }
    }
    
    handleBack = (e) => {
        window.location.href = "/";
    }

    handleChooseSlot = async(slot) => {
        let slotTimes = document.getElementsByClassName("slot-time");
        for(let i = 0; i < slotTimes.length; i++) {
            slotTimes[i].classList.remove("chosen-slot-time");
        }
        let chosenSlot = document.getElementById(`${slot.time}`);
        chosenSlot.classList.add("chosen-slot-time");
        this.setState({
            chosenSlot: slot.time
        });
    }
    handleContinueBooking = async(e) => {
        let chosenDay = this.state.chosenDay;
        let chosenSlot = this.state.chosenSlot;
        if(chosenSlot) {
            let hour = chosenSlot.split(":")[0];
            let minute = chosenSlot.split(":")[1];
            chosenDay.setHours(hour);
            chosenDay.setMinutes(minute);
            chosenDay.setSeconds(0);
            let booking = {
                time: chosenDay,
                status: 0,
                dlctUserId: cookies.get("userId"),
                salonId: this.state.salon.id
            }
            let chosenServices = cookies.get("chosenServices");
            await this.props.handleCreateBooking(booking);
            if(this.props.bookingReducer.booking) {
                cookies.remove("chosenServices");
                chosenServices.forEach( async(service) => {
                    let bookingService = {
                        bookingId: this.props.bookingReducer.booking.id,
                        serviceId: service.id
                    }
                    await this.props.handleCreateBookingService(bookingService);
                });
            }
            console.log(this.props.bookingReducer.booking);
        }
    }
    handleRemoveService = (service) => {
        let chosenServices = this.state.chosenServices;
        for(let i = 0; i < chosenServices.length; i++) {
            if(chosenServices[i].serviceId === service.serviceId) {
                chosenServices.splice(i, 1);
            }
        }
        this.setState({
            chosenServices: chosenServices
        })
        cookies.set('chosenServices', chosenServices, {
            maxAge: 24 * 3600,
            path: '/'
        });
        console.log(chosenServices);
    }
    render() {
        const modifiers = {
            selectedDays: this.state.chosenDay,
        };
        const modifiersStyles = {
            selectedDays: {
                color: 'white',
                backgroundColor: '#fed136',
            }
        };
        const {chosenServices, slots} = this.state;
        let totalPrice = 0, totalDuration = 0;
        
        for(let i = 0; i < chosenServices.length; i++) {
            totalPrice += chosenServices[i].price;
            totalDuration += chosenServices[i].duration;
        }
        return (
            <div>
                <Header/>
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Chọn ngày giờ</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <p>Hãy chọn ngày giờ phù hợp nhất với anh/chị</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                    <DayPicker 
                                    onDayClick={this.handleDayClick}
                                    disabledDays={{ before: new Date() }}
                                    selectedDays={this.state.selectedDay}
                                    locale="vi"
                                    months={MONTHS}
                                    weekdaysLong={WEEKDAYS_LONG}
                                    weekdaysShort={WEEKDAYS_SHORT}
                                    modifiers={modifiers}
                                    modifiersStyles={modifiersStyles}/>
                            </div>
                            <div className="col-md-4 slot-table">
                                <div className="row">
                                    {this.displaySlots(slots)}
                                </div>
                            </div>
                            <div className="col-md-4 chosen-services">
                                <div className="chosen-salon">
                                    Most Hair
                                </div>
                                {this.displayChosenService(chosenServices)}
                                <NavLink className="row chosen-item add-button" data-target="#link1" to={{pathname: `/${this.state.normalizedName}`}}>
                                    <div className="col-md-1">
                                        <i className="fas fa-plus"></i>
                                    </div>
                                    <div className="col-md-9">Thêm dịch vụ</div>
                                </NavLink>
                                <div className="row chosen-total">
                                    <div className="col-md-4 chosen-total-name">
                                        Tổng
                                    </div>
                                    <div className="col-md-4 chosen-service-time">
                                        {totalDuration} phút
                                    </div>
                                    <div className="col-md-2 chosen-service-price">
                                        {totalPrice/1000}k
                                    </div>
                                </div>
                                <div className="row pull-center">
                                    <button type="button" className="btn btn-primary next-button" onClick={this.handleContinueBooking}>Tiếp tục</button>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </section>
                <Dialog ref={(component) => { this.dialog = component }} />
            </div>
        );
    }

    displayChosenService = (arr) => {
        let result = null;
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div className="row chosen-item">
                    <div className="col-md-4 chosen-service-name">
                        {element.service.name}
                    </div>
                    <div className="col-md-4 chosen-service-time">
                        {element.duration} phút
                    </div>
                    <div className="col-md-2 chosen-service-price">
                        {element.price/1000}k
                    </div>
                    <div className="col-md-2 chosen-service-price" onClick={() => this.handleRemoveService(element)}>
                        <i className="fas fa-times"></i>
                    </div>
                </div>
            ))
        }
        return result;
    }
    displaySlots = (arr) => {
        let result = null;
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div className="slot-item col-md-4" onClick={() => this.handleChooseSlot(element)}>
                    <div className={`slot-time ${element.past ? "is-past": ""}`} id={element.time}>
                        {element.time}
                    </div>
                </div>
            ))
        }
        return result;
    }
}

function formatTime(hour) {
    const minute = Number((hour % 1).toFixed(2)) * 60;
    hour = Math.floor(hour);

    const hourString = hour < 10
        ? '0' + hour
        : hour;
    const minuteString = minute < 10
        ? '0' + minute
        : minute;

    var timeString = hourString + ':' + minuteString;
    return timeString;
}

function scheduleToSlots(schedule, chosenDate) {
    const slots = [];
    let startAMs = schedule.startAM.split(':');
    let startAM = Number(startAMs[0]) + Number((startAMs[1] / 60).toFixed(2));
    let finishAMs = schedule.finishAM.split(':');
    const finishAM = Number(finishAMs[0]) + Number((finishAMs[1] / 60).toFixed(2));
    let startPMs =  schedule.startPM.split(':');
    let startPM = Number(startPMs[0]) + Number((startPMs[1] / 60).toFixed(2));
    let finishPMs = schedule.finishPM.split(':');
    const finishPM = Number(finishPMs[0]) + Number((finishPMs[1] / 60).toFixed(2));

    while (startAM <= finishAM) {
        const timeString = formatTime(startAM);
        slots.push({time: timeString});
        startAM += 0.5;
    }

    while (startPM <= finishPM) {
        const timeString = formatTime(startPM);
        slots.push({time: timeString});
        startPM += 0.5;
    }
    return slots;
}

function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        cityReducer: state.cityReducer, 
        districtReducer: state.districtReducer, 
        scheduleReducer: state.scheduleReducer,
        authenticationReducer: state.authenticationReducer,
        bookingReducer: state.bookingReducer,
        unavailableSlotReducer: state.unavailableSlotReducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetSalonByNormalizedName: async(salonNormalizedName) => {
            await dispatch(getSalonByNormalizedName(salonNormalizedName));
        },
        handleGetSchedulesBySalonIdAndWeekDay: async(salonId, weekDay) => {
            await dispatch(getSchedulesBySalonIdAndWeekDay(salonId, weekDay));
        },
        handleCreateBooking: async(booking) => {
            await dispatch(createBooking(booking))
        },
        handleCreateBookingService: async(booking) => {
            await dispatch(createBookingService(booking))
        },
        handleGetBookingsByCustomerId: async(customerId) => {
            await dispatch(getBookingsByCustomerId(customerId))
        },
        handleUpdateBooking: async(booking) => {
            await dispatch(updateBooking(booking))
        },
        getOpenBookingsBySalonIdAndDate: async(salonId, dateStart, dateEnd) => {
            await dispatch(getOpenBookingsBySalonIdAndDate(salonId, dateStart, dateEnd))
        },
        handleGetUnavailableSlotBySalonId: async(salonId) => {
            await dispatch(getUnavailableSlotsBySalonId(salonId))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SlotPage)