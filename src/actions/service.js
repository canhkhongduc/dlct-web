import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const GET_SERVICES_BY_SALONID = 'GET_REVIEWS_BY_SALONID';
export const GET_SERVICES_BY_SALONID_SUCCESS = 'GET_REVIEWS_BY_SALONID_SUCCESS';

export function fetchServicesBySalonId() {
    return {type: GET_SERVICES_BY_SALONID};
}
export function fetchServicesBySalonIdSuccess(data) {
    return {type: GET_SERVICES_BY_SALONID_SUCCESS, data};
}

export function getServicesBySalonId(salonId) {
    return async(dispatch) => {
        dispatch(fetchServicesBySalonId());
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getServicesBySalonId(salonId), 'GET')
            dispatch(fetchServicesBySalonIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}