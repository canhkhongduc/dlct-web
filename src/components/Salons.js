import React from 'react'
import Salon from '../containers/Salon'

const Salons = () => (

  <div className="row" >
    <Salon salonName="30-shine" salonId="1" />
    <Salon salonName="Liêm Barber" salonId="2" />
    <Salon salonName="ABC" salonId="3" />
    <Salon salonName="XYZ" salonId="4" />
    <Salon salonName="DEF" salonId="5" />
    <Salon salonName="GHI" salonId="6" />
  </div>
);

export default Salons;

