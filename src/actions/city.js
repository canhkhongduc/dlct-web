import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const GET_CITY_BY_ID = 'GET_CITY_BY_ID';
export const GET_CITY_BY_ID_SUCCESS = 'GET_CITY_BY_ID_SUCCESS';

export const GET_ALL_CITIES = 'GET_ALL_CITIES';
export const GET_ALL_CITIES_SUCCESS = 'GET_ALL_CITIES_SUCCESS';

export const GET_CITIES_HAS_SALON = 'GET_CITIES_HAS_SALON';
export const GET_CITIES_HAS_SALON_SUCCESS = 'GET_CITIES_HAS_SALON_SUCCESS';

const getCityById = () => ({type: GET_CITY_BY_ID});
const getCityByIdSuccess = (data) => ({type: GET_CITY_BY_ID_SUCCESS, data});

const getAllCities = () => ({type: GET_ALL_CITIES});
const getAllCitiesSuccess = (data) => ({type: GET_ALL_CITIES_SUCCESS, data});

const getCitiesHasSalon = () => ({type: GET_CITIES_HAS_SALON});
const getCitiesHasSalonSuccess = (data) => ({type: GET_CITIES_HAS_SALON_SUCCESS, data});

export function fetchDataCityById(cityId) {
    return async(dispatch) => {
        dispatch(getCityById())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getCityById(cityId), 'GET')
            dispatch(getCityByIdSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function fetchDataAllCities() {
    return async(dispatch) => {
        dispatch(getAllCities())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getAllCities, 'GET')
            dispatch(getAllCitiesSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function fetchDataAllCitiesHasSalon() {
    return async(dispatch) => {
        dispatch(getCitiesHasSalon())
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getCitiesHasSalon, 'GET')
            dispatch(getCitiesHasSalonSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}
