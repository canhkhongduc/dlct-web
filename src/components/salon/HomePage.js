import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import { ToastContainer, toast } from 'mdbreact';
import SideNavBar from '../layout/SideNavBar';
import {getSalonByPhoneNumber} from '../../actions/salon';
import {getSchedulesBySalonIdAndWeekDay} from '../../actions/schedule';
import {getBookingsBySalonId, createBooking, updateBooking, getOpenBookingsBySalonIdAndDate, getAllBookingsBySalonIdAndSlotTime} from '../../actions/booking';
import {getCustomerByPhoneNumber, createCustomer} from '../../actions/customer';
import { deleteUnavailableSlot, createUnavailableSlot, getUnavailableSlotsBySalonId, getUnavailableSlotBySalonIdAndSlotTime } from '../../actions/unavailableSlot';
import dateFormat from 'dateformat';
import moment from 'moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const statusTypes = {
    0: 'Đang chờ',
    1: 'Hoàn thành',
    2: 'Đã hủy'
};


function enumFormatter(cell, row, enumObject) {
    return enumObject[cell];
}

function phoneFormatter(cell, row) {
    return (
        <a href={`tel:${cell}`} className="phone-number">{cell}</a>
    );
  }


class HomePage extends Component {
    constructor(props) {
        super(props);
        this.sideNavId = 1;
        this.state = {
            sideNavId: 1,
            salon: [],
            schedules: [],
            bookings: [],
            customer: [],
            chosenDay: dateFormat(new Date(), "yyyy-mm-dd"),
            chosenSlotBookings: [],
            salonUnavailableSlots: [],
        };

    }

    componentWillMount = async(e) => {
        let phoneNumber = cookies.get('phoneNumber');
        let role = cookies.get('role');
        let token = cookies.get('token');
        if (!token || role !== "salon") {
            window.location.href = `/salon/login`;
        } else {
            let todayDate, tmrDate, twoDayDate;
            if(new Date().getDay() === 5) {
                todayDate = 5;
                tmrDate = 6;
                twoDayDate = 0;
            } else if (new Date().getDay() === 6) {
                todayDate = 6;
                tmrDate = 0;
                twoDayDate = 1;
            } else {
                todayDate = new Date().getDay();
                tmrDate = new Date().getDay() + 1;
                twoDayDate = new Date().getDay() + 2;
            }
            this.setState({
                todayDate: todayDate,
                tmrDate: tmrDate,
                twoDayDate: twoDayDate
            });
            await this.props.handleFetchSalonByPhoneNumber(phoneNumber);
            await this.props.handleFetchSchedulesBySalonId(this.props.salonReducer.salon[0].id);
            if (this.props.scheduleReducer.schedules[0].length !== 0) {
                let dateStart = `${dateFormat(new Date(), "yyyy-mm-dd")}T00%3A00%3A00Z`;
                let dateEnd = `${dateFormat(new Date(), "yyyy-mm-dd")}T23%3A59%3A59Z`;
                await this.props.handleFetchBookingsBySalonId(this.props.salonReducer.salon[0].id, dateStart, dateEnd);
                await this.props.handleGetUnavailableSlotBySalonId(this.props.salonReducer.salon[0].id);
                this.setState({
                    schedules: this.props.scheduleReducer.schedules, 
                    bookings: this.props.bookingReducer.bookings, 
                    salon: this.props.salonReducer.salon[0],
                    salonUnavailableSlots: this.props.unavailableSlotReducer.unavailableSlots
                });
                let chosenDay;
                if(new Date(this.state.chosenDay).getDay() === 0) {
                    chosenDay = 6;
                } else {
                    chosenDay = new Date(this.state.chosenDay).getDay() - 1;
                }
                let salonSlots = scheduleToSlots(this.props.scheduleReducer.schedules[0][chosenDay]);
                this.checkSlotAvailability(salonSlots).then((slots)=> {
                    this.setState({
                        slots: slots
                    });
                });

            } else {
                window.location.href = `/salon/setting`;
            }
            let dropdowns = document.getElementsByClassName("react-bs-table-sizePerPage-dropdown");
            let paginations = document.getElementsByClassName("pagination");
            paginations[0].parentNode.style.width = "50%";
            dropdowns[0].parentNode.style.width = "50%";
        }
    }

    changeActiveStyle = (element) => {
        if (element.parentNode.classList.contains("active")) {
            //do nothing
        } else {
            let dayItems = document.getElementsByClassName("salon-day-item");
            for (let i = 0; i < dayItems.length; i++) {
                dayItems[i]
                    .classList
                    .remove("active");
            }
            if (element.classList.contains("salon-day-item")) {
                element
                    .classList
                    .add("active");
            } else {
                element
                    .parentNode
                    .classList
                    .add("active");
            }
        }
    }

    checkSlotAvailability = async(salonSlots) => {
        for(let i = 0; i < salonSlots.length; i++){
            // await this.props.handleGetBookingsBySalonIdAndSlotTime(this.state.salon.id, `${this.state.chosenDay}T${salonSlots[i].time.replace(':','%3A')}%3A00Z`);
            // await this.props.handleGetUnavailableSlotBySalonIdAndSlotTime(this.state.salon.id, `${this.state.chosenDay}T${salonSlots[i].time.replace(':','%3A')}%3A00Z`);
            let slotDate = new Date(`${this.state.chosenDay} ${salonSlots[i].time}`);
            if(slotDate < new Date()) {
                salonSlots[i].past = true;
            } else {
                salonSlots[i].past = false;
            }
            let chosenDay;
            if(new Date(this.state.chosenDay).getDay() === 0) {
                chosenDay = 6;
            } else {
                chosenDay = new Date(this.state.chosenDay).getDay() - 1;
            }
            if(this.state.bookings.length !== 0) {
                let numberOfOpenBookingsInASlot = 0;
                for(let k = 0; k < this.state.bookings.length; k++) {
                    if(this.state.bookings[k].status === 0 && `${this.state.chosenDay}T${salonSlots[i].time}:00Z` === this.state.bookings[k].time) {
                        numberOfOpenBookingsInASlot++;
                    }
                }
                if(numberOfOpenBookingsInASlot < this.props.scheduleReducer.schedules[0][chosenDay].slotCapacity) {
                    salonSlots[i].status = true;
                } else {
                    salonSlots[i].status = false;
                }
            } else {
                salonSlots[i].status = true;
            }
            if(this.state.salonUnavailableSlots !== 0) {
                for(let m = 0; m < this.state.salonUnavailableSlots.length; m++) {
                    if(`${this.state.chosenDay}T${salonSlots[i].time}:00Z` === this.state.salonUnavailableSlots[m].time) {
                        salonSlots[i].status = false;
                    }
                }
            }
        }
        return salonSlots;
    }

    handleOnTodayClick = async(e) => {
        this.changeActiveStyle(e.target);
        let dateStart = `${dateFormat(new Date(), "yyyy-mm-dd")}T00%3A00%3A00Z`;
        let dateEnd = `${dateFormat(new Date(), "yyyy-mm-dd")}T23%3A59%3A59Z`;
        await this
            .props
            .handleFetchBookingsBySalonId(this.props.salonReducer.salon[0].id, dateStart, dateEnd);
        this.setState({
            chosenDay: dateFormat(new Date(), "yyyy-mm-dd"),
            bookings:this.props.bookingReducer.bookings
        });
        let chosenDay;
        if(new Date().getDay() === 0) {
            chosenDay = 6;
        } else {
            chosenDay = new Date().getDay() - 1;
        }
        this.checkSlotAvailability(
            scheduleToSlots(
                this.props.scheduleReducer.schedules[0][chosenDay]
            )
        ).then((slots) => {
            this.setState({
                slots: slots
            });
        });
    }

    handleTommorrowClick = async(e) => {
        this.changeActiveStyle(e.target);
        let dateStart = `${dateFormat(moment(new Date()).add(1, 'days'), "yyyy-mm-dd")}T00%3A00%3A00Z`;
        let dateEnd = `${dateFormat(moment(new Date()).add(1, 'days'), "yyyy-mm-dd")}T23%3A59%3A59Z`;
        let today = new Date();
        await this
            .props
            .handleFetchBookingsBySalonId(this.props.salonReducer.salon[0].id, dateStart, dateEnd);
        this.setState({
            chosenDay: dateFormat(moment(new Date()).add(1, 'days'), "yyyy-mm-dd"),
            bookings:this.props.bookingReducer.bookings
        });
        let chosenDay;
        if(new Date(dateFormat(moment(addDays(today, 1)), "yyyy-mm-dd")).getDay() === 0) {
            chosenDay = 6;
        } else {
            chosenDay = new Date(dateFormat(moment(addDays(today, 1)), "yyyy-mm-dd")).getDay() - 1;
        }
        this.checkSlotAvailability(
            scheduleToSlots(
                this.props.scheduleReducer.schedules[0][chosenDay]
            )
        ).then((slots) => {
            this.setState({
                slots: slots
            });
        });
    }

    handleTwoDaysClick = async(e) => {
        this.changeActiveStyle(e.target);
        let dateStart = `${dateFormat(moment(new Date()).add(2, 'days'), "yyyy-mm-dd")}T00%3A00%3A00Z`;
        let dateEnd = `${dateFormat(moment(new Date()).add(2, 'days'), "yyyy-mm-dd")}T23%3A59%3A59Z`;
        let today = new Date();
        await this
            .props
            .handleFetchBookingsBySalonId(this.props.salonReducer.salon[0].id, dateStart, dateEnd);
        this.setState({
            chosenDay: dateFormat(moment(new Date()).add(2, 'days'), "yyyy-mm-dd"),
            bookings:this.props.bookingReducer.bookings
        });
        let chosenDay;
        if(new Date(dateFormat(moment(addDays(today, 2)), "yyyy-mm-dd")).getDay() === 0) {
            chosenDay = 6;
        } else {
            chosenDay = new Date(dateFormat(moment(addDays(today, 2)), "yyyy-mm-dd")).getDay() - 1;
        }
        this.checkSlotAvailability(
            scheduleToSlots(
                this.props.scheduleReducer.schedules[0][chosenDay]
            )
        ).then((slots) => {
            this.setState({
                slots: slots
            });
        });
    }
    handleSearchByDate = async(e) => {
        let dateStart = `${e.target.value}T00%3A00%3A00Z`;
        let dateEnd = `${e.target.value}T23%3A59%3A59Z`;
        let searchDate = e.target.value;
        await this
            .props
            .handleFetchBookingsBySalonId(this.props.salonReducer.salon[0].id, dateStart, dateEnd);
        this.setState({
            chosenDay: dateFormat(new Date(searchDate), "yyyy-mm-dd"),
            bookings: this.props.bookingReducer.bookings
        });
        let chosenDay;
        if(new Date(searchDate).getDay() === 0) {
            chosenDay = 6;
        } else {
            chosenDay = new Date(searchDate).getDay() - 1;
        }
        this.checkSlotAvailability(scheduleToSlots(this.props.scheduleReducer.schedules[0][chosenDay])).then((slots) => {
            this.setState({
                slots: slots,
            });
        });
    }
    handleCreateNewBooking = async(event) => {
        let customerName = event.target.customerName.value;
        let customerPhoneNumber = event.target.customerPhoneNumber.value;
        await this
            .props
            .handleGetCustomerByPhoneNumber(customerPhoneNumber);
        if (this.props.authenticationReducer.getCustomer.length !== 0) {
            let booking = {
                customerId: this.props.authenticationReducer.getCustomer[0].id,
                salonId: this.state.salon.id,
                status: 0,
                time: `${this.state.chosenDay}T${this.state.chosenSlot}:00Z`
            }
            await this
                .props
                .handleCreateBooking(booking);
            if (this.props.bookingReducer.booking) {
                toast.success("Thêm khách hàng thành công.")
            } else {
                toast.error("Thêm khách hàng thất bại. Kiểm tra kết nối mạng.")
            }

        } else {
            let customer = {
                name: customerName,
                activated: true,
                phoneNumber: customerPhoneNumber
            };
            await this.props.handleCreateCustomer(customer);
            if(this.props.authenticationReducer.customer) {
                let booking = {
                    customerId: this.props.authenticationReducer.customer.id,
                    salonId: this.state.salon.id,
                    status: 0,
                    time: `${this.state.chosenDay}T${this.state.chosenSlot}:00Z`
                }
                await this
                .props
                .handleCreateBooking(booking);
                if (this.props.bookingReducer.booking) {
                    toast.success("Thêm khách hàng thành công.");
                } else {
                    toast.error("Thêm khách hàng thất bại. Kiểm tra kết nối mạng.")
                }
            }
        }
    }
    handleSlotClick = async(index, element) => {
        let slotItems = document.getElementsByClassName("salon-slot-item");
        for(let i = 0; i < slotItems.length; i++) {
            slotItems[i].classList.remove("chosen");
        }
        document.getElementById(`slot-item-${index}`).classList.add("chosen");

        this.setState({
            chosenSlot: element.time
        });
        await this.props.handleGetAllBookingsBySalonIdAndSlotTime(this.state.salon.id, `${this.state.chosenDay}T${element.time.replace(":","%3A")}%3A00Z`);
        if(this.props.bookingReducer.allBookings) {
            this.setState({
                chosenSlotBookings: this.props.bookingReducer.allBookings
            });
        }
        let cbWorkingStatus = document.getElementById("working-status-switch");
        await this.props.handleGetUnavailableSlotBySalonIdAndSlotTime(this.state.salon.id, `${this.state.chosenDay}T${element.time.replace(':','%3A')}%3A00Z`);
        
        if(this.props.unavailableSlotReducer.unavailableSlot.length >= 1) {
            cbWorkingStatus.checked = false;
        } else {
            cbWorkingStatus.checked = true;
        }
        if(!element.status || element.past || !cbWorkingStatus.checked) {
            document.getElementById("insert-customer-form").classList.add("invisible");
        } else {
            document.getElementById("insert-customer-form").classList.remove("invisible");
        }

    }

    onAfterSaveCell = async(row) => {
        toast.success("Lưu thành công.");
    }

    onBeforeSaveCell = async(row, cellName, cellValue) => {
        if(cellName === "slotTime" && cellValue){
            row.time = `${this.state.chosenDay}T${cellValue}:00Z`;
        }
        await this.props.handleUpdateBooking(row);
    }

    handleChangeSlotWorkingStatus = async(e) => {
        let cbWorkingStatus = document.getElementById("working-status-switch").checked;

        if(cbWorkingStatus){
            document.getElementById("insert-customer-form").classList.remove("invisible");
            if(this.props.unavailableSlotReducer.unavailableSlot.length >= 1) {
                await this.props.handleDeleteUnAvailableSlot(this.props.unavailableSlotReducer.unavailableSlot[0].id);
            }
        } else {
            document.getElementById("insert-customer-form").classList.add("invisible");
            if(this.props.unavailableSlotReducer.unavailableSlot.length === 0) {
                let unavailableSlots = {
                    salonId: this.state.salon.id,
                    time: `${this.state.chosenDay}T${this.state.chosenSlot}:00Z`
                };
                await this.props.handleCreateUnAvailableSlot(unavailableSlots);
            }
        }
    }
    

    render() {
        const {bookings, slots, chosenSlotBookings, chosenDay, chosenSlot, todayDate, tmrDate, twoDayDate} = this.state;
        if (bookings) {
            for (let i = 0; i < bookings.length; i++) {
                let timezone = -(new Date().getTimezoneOffset() / 60);
                let dateTime = new Date(bookings[i].time);
                if(dateTime.getHours() < timezone) {
                    bookings[i].slotTime = (((dateTime.getHours() + 24 - timezone) < 10)
                    ? "0" + (dateTime.getHours() + 24 - timezone)
                    : (dateTime.getHours() + 24 - timezone)) + ":" + ((dateTime.getMinutes() < 10)
                    ? "0" + dateTime.getMinutes()
                    : dateTime.getMinutes());
                } else {
                    bookings[i].slotTime = (((dateTime.getHours() - timezone) < 10)
                    ? "0" + (dateTime.getHours() - timezone)
                    : (dateTime.getHours() - timezone)) + ":" + ((dateTime.getMinutes() < 10)
                    ? "0" + dateTime.getMinutes()
                    : dateTime.getMinutes());
                }
                
            }
        }
        let workingHours = [];
        if(slots) {
            for(let i = 0; i < slots.length; i++) {
                if(slots[i].status && !slots[i].past) {
                    workingHours.push(slots[i].time);
                }
            }
        }
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell

        };
        
        return (
            <div>
                <Header/>
                <div className="row">
                    <SideNavBar/>
                    <div className="col-md-10 main-salon-content">
                        <div className="row">
                            <h3 className="text-center">Quản lý đặt lịch</h3>
                        </div>
                        <div className="row text-center date-heading">
                            <div className="salon-day-item active" onClick={this.handleOnTodayClick}>
                                <p className="day-title">{getDayOfWeek(todayDate)}</p>
                                <p className="date-title">{dateFormat(new Date(), "dd/mm/yyyy")}</p>
                            </div>
                            <div className="salon-day-item" onClick={this.handleTommorrowClick}>
                                <p className="day-title">{getDayOfWeek(tmrDate)}</p>
                                <p className="date-title">{dateFormat(moment(new Date()).add(1, 'days'), "dd/mm/yyyy")}</p>
                            </div>
                            <div className="salon-day-item" onClick={this.handleTwoDaysClick}>
                                <p className="day-title">{getDayOfWeek(twoDayDate)}</p>
                                <p className="date-title">{dateFormat(moment(new Date()).add(2, 'days'), "dd/mm/yyyy")}</p>
                            </div>
                        </div>
                        <div className="row text-center date-search">
                            <form id="form-date-search">
                                <div className="row">
                                    <input
                                        type="date"
                                        id="searchDateInput"
                                        className="form-control validate"
                                        onChange={this.handleSearchByDate}/>
                                </div>
                            </form>
                        </div>
                        <div className="row text-center salon-slot-table">
                            {(slots || []).map((element, index) => {
                                return <div className={`salon-slot-item slot-item-available-${element.status} is-past-${element.past}`} 
                                id={`slot-item-${index}`} 
                                onClick={() => this.handleSlotClick(index, element)}
                                data-toggle="modal"
                                data-target= "#insertCustomerModal">
                                    <p className="slot-time">{element.time}</p>
                                    <p className="slot-status">{element.status? "Còn chỗ" : "Hết chỗ"}</p>
                                </div>
                            })}
                        </div>
                        <div className="row">
                            <p className="text-center salon-table-title">Kích chuột vào ca làm việc để xem chi tiết khách đặt hoặc thêm khách hàng</p>
                        </div>
                        <div className="row">
                            <BootstrapTable
                                data={bookings}
                                striped
                                hover
                                pagination
                                cellEdit={cellEditProp}>
                                <TableHeaderColumn
                                    width={'32%'}
                                    dataField='customerName'
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>Tên khách hàng</TableHeaderColumn>
                                <TableHeaderColumn
                                    width={'28%'}
                                    isKey
                                    dataFormat = {phoneFormatter}
                                    dataField='customerPhoneNumber'
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>Số điện thoại</TableHeaderColumn>
                                <TableHeaderColumn 
                                width={'20%'}
                                dataField='slotTime' 
                                dataSort={true}
                                editable={{
                                    type: 'select',
                                    options: {
                                        values: workingHours
                                    }
                                    }}>
                                    Ca<br/>làm<br/>việc
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    width={'22%'}
                                    dataField='status'
                                    filterFormatted dataFormat={ enumFormatter }
                                    formatExtraData={ statusTypes }
                                    filter={ { type: 'SelectFilter', options: statusTypes }}
                                    editable={{
                                    type: 'select',
                                    options: {
                                        values: [0,1,2]
                                    }
                                    }}>
                                    Trạng thái
                                </TableHeaderColumn>
                            </BootstrapTable>
                        </div>

                    </div>
                    <div className="alert alert-success invisible" id="alert-success"></div>

                    <div
                        className="modal fade"
                        id="insertCustomerModal"
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content form-elegant">
                                <div className="modal-header text-center">
                                    <h3
                                        className="modal-title w-100 dark-grey-text font-weight-bold my-3"
                                        id="myModalLabel">
                                        <strong>Chi tiết ca làm việc</strong>
                                    </h3>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="row text-center">
                                    <h4>{chosenSlot} Ngày {chosenDay}</h4>
                                </div>
                                <div className="row text-center">
                                    <h5>Có {chosenSlotBookings.length} khách</h5>
                                </div>
                                <div className="row text-center cb-working-status">
                                    <label className="form-check-label" htmlFor="working-status-switch">Bạn có làm việc ca này không? (Tích là có)</label>
                                    <input type="checkbox" id="working-status-switch"
                                    onChange={this.handleChangeSlotWorkingStatus} className="form-check-input"/> 
                                </div>
                                <div className="modal-body mx-4">
                                    <div className="row" id="chosen-slot-bookings-table">
                                        <BootstrapTable
                                            data={chosenSlotBookings}
                                            striped
                                            hover
                                            cellEdit={cellEditProp}>
                                            <TableHeaderColumn
                                                dataField='customerName'
                                                editable={false}>Tên khách hàng</TableHeaderColumn>
                                            <TableHeaderColumn
                                                isKey
                                                dataField='customerPhoneNumber'
                                                editable={false}>Số điện thoại</TableHeaderColumn>
                                            <TableHeaderColumn
                                                dataField='status'
                                                filterFormatted dataFormat={ enumFormatter }
                                                formatExtraData={ statusTypes }
                                                editable={{
                                                type: 'select',
                                                options: {
                                                    values: [0,1,2]
                                                }
                                                }}>
                                                Trạng thái
                                            </TableHeaderColumn>
                                        </BootstrapTable>
                                    </div>
                                    

                                    <form onSubmit={this.handleCreateNewBooking} id="insert-customer-form">
                                        <div className="md-form">
                                                <h4>Thêm khách hàng</h4>
                                        </div>
                                        <div className="md-form pb-3">
                                            <input
                                                type="text"
                                                id="Form-email1"
                                                className="form-control validate"
                                                name="customerName"/>
                                            <label data-error="wrong" data-success="right" htmlFor="Form-email1">Tên khách hàng</label>
                                        </div>

                                        <div className="md-form pb-3">
                                            <input
                                                type="number"
                                                id="Form-pass1"
                                                className="form-control validate"
                                                name="customerPhoneNumber"/>
                                            <label data-error="wrong" data-success="right" htmlFor="Form-pass1">Số điện thoại</label>
                                        </div>
                                        <div className="text-center mb-3">
                                            <button
                                                type="submit"
                                                className="btn btn-primary btn-block btn-rounded z-depth-1a">Lưu</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />                            
                </div>
            </div>
        );
    }

}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function scheduleToSlots(schedule) {
    const slots = [];
    let startAMs = schedule.startAM.split(':');
    let startAM = Number(startAMs[0]) + Number((startAMs[1] / 60).toFixed(2));
    let finishAMs = schedule.finishAM.split(':');
    const finishAM = Number(finishAMs[0]) + Number((finishAMs[1] / 60).toFixed(2));
    let startPMs =  schedule.startPM.split(':');
    let startPM = Number(startPMs[0]) + Number((startPMs[1] / 60).toFixed(2));
    let finishPMs = schedule.finishPM.split(':');
    const finishPM = Number(finishPMs[0]) + Number((finishPMs[1] / 60).toFixed(2));
    const slotDuration = Number((schedule.slotDuration / 60).toFixed(2));
    

    while (startAM <= finishAM) {
        const timeString = formatTime(startAM);
        slots.push({time: timeString});
        startAM += slotDuration;
    }

    while (startPM <= finishPM) {
        const timeString = formatTime(startPM);
        slots.push({time: timeString});
        startPM += slotDuration;
    }
    return slots;
}

function getDayOfWeek(dayOfWeek) {
    switch (dayOfWeek) {
        case 1:
            return 'Thứ hai';
        case 2:
            return 'Thứ ba';
        case 3:
            return 'Thứ tư';
        case 4:
            return 'Thứ năm';
        case 5:
            return 'Thứ sáu';
        case 6:
            return 'Thứ bảy';
        case 0:
            return 'Chủ nhật';
        default:
            return '';
    }
}

function formatTime(hour) {
    const minute = Number((hour % 1).toFixed(2)) * 60;
    hour = Math.floor(hour);

    const hourString = hour < 10
        ? '0' + hour
        : hour;
    const minuteString = minute < 10
        ? '0' + minute
        : minute;

    var timeString = hourString + ':' + minuteString;
    return timeString;
}

function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        scheduleReducer: state.scheduleReducer, 
        bookingReducer: state.bookingReducer, 
        authenticationReducer: state.authenticationReducer,
        unavailableSlotReducer: state.unavailableSlotReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleFetchSalonByPhoneNumber: async(phoneNumber) => {
            await dispatch(getSalonByPhoneNumber(phoneNumber))
        },
        handleFetchSchedulesBySalonId: async(salonId) => {
            await dispatch(getSchedulesBySalonIdAndWeekDay(salonId))
        },
        handleFetchBookingsBySalonId: async(salonId, dateStart, dateEnd) => {
            await dispatch(getBookingsBySalonId(salonId, dateStart, dateEnd))
        },
        handleGetCustomerByPhoneNumber: async(phoneNumber) => {
            await dispatch(getCustomerByPhoneNumber(phoneNumber))
        },
        handleCreateBooking: async(booking) => {
            await dispatch(createBooking(booking))
        },
        handleCreateCustomer: async(customer) => {
            await dispatch(createCustomer(customer))
        },
        handleUpdateBooking: async(booking) => {
            await dispatch(updateBooking(booking))
        },
        getOpenBookingsBySalonIdAndDate: async(salonId, slotTime) => {
            await dispatch(getOpenBookingsBySalonIdAndDate(salonId, slotTime))
        },
        handleGetUnavailableSlotBySalonId: async(salonId) => {
            await dispatch(getUnavailableSlotsBySalonId(salonId))
        },
        handleGetAllBookingsBySalonIdAndSlotTime: async(salonId, slotTime) => {
            await dispatch(getAllBookingsBySalonIdAndSlotTime(salonId, slotTime))
        },
        handleDeleteUnAvailableSlot: async(id) => {
            await dispatch(deleteUnavailableSlot(id))
        },
        handleCreateUnAvailableSlot: async(unavailableSlot) => {
            await dispatch(createUnavailableSlot(unavailableSlot))
        },
        handleGetUnavailableSlotBySalonIdAndSlotTime: async(salonId, slotTime) => {
            await dispatch(getUnavailableSlotBySalonIdAndSlotTime(salonId, slotTime))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)

