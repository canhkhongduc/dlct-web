import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const RECEIVE_SCHEDULE = 'RECEIVE_SCHEDULE';
export const CREATE_SCHEDULE_SUCCESS = 'CREATE_SCHEDULE_SUCCESS';

export const receiveSchedules = (schedules) => ({type: RECEIVE_SCHEDULE, schedules});

export const createScheduleSuccess = (schedule) => ({type: CREATE_SCHEDULE_SUCCESS, schedule});


export const getSchedulesBySalonIdAndWeekDay = (salonId, weekDay) => async dispatch => {
    const res = await apiRequest.requestWithoutToken(endpoints.getSchedulesBySalonIdAndWeekDay(salonId, weekDay), 'GET');
    dispatch(receiveSchedules(JSON.stringify(res)));
};

export function createSchedule(schedule) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createSchedule, 'POST', schedule);
            dispatch(createScheduleSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function updateSchedule(schedule) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createSchedule, 'PUT', schedule);
            dispatch(createScheduleSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

