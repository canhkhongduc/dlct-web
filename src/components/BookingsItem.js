import React from 'react';

const BookingsItem = ({ booking }) => (
  <booking >
    <div className="booking-wrapper">
      <h3 className="text-center">customer: {booking.customerId}</h3>
      <h4 className="text-center">status: {booking.status}</h4>
      <p className="text-center">time: {booking.time}</p>
    </div>
  </booking>
);

export default BookingsItem ;


