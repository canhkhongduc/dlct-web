import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import { salonAuthenticate, getSalonByPhoneNumber } from '../../actions/salon';
import Cookies from 'universal-cookie';
import { createAndSendPinCode, verifyPinCode } from '../../actions/customer';
import { toast, ToastContainer } from '../../../node_modules/mdbreact';
import { NavLink } from 'react-router-dom';
const cookies = new Cookies();

class ForgetPasswordPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCities: [],
            allDistricts: [],
            allSalons: [],
            token: [],
        };

    }
    componentWillMount = async(e) => {
    }
   

    handleLogin = async(event) => {
        window.location.href = "/salon/login";
    }

    handlePhoneNumberChange = (e) => {
        this.setState({salonPhoneNumber: e.target.value});
    }

    handleOtpCodeChange = (e) => {
        this.setState({otpCode: e.target.value});
    }

    handleForgetPwdContinue = async(e) => {
        const salonPhoneNumber = this.state.salonPhoneNumber;
        await this.props.handleGetSalonByPhoneNumber(salonPhoneNumber);
        if(this.props.salonReducer.salon[0]) {
            await this.props.handleSendOTPMessage(salonPhoneNumber);
            this.setState({smsResponse: this.props.authenticationReducer.smsResponse, allSalons: []})
            let registerForm = document.getElementsByClassName("customer-register");
            let otpConfirm = document.getElementsByClassName("otp-confirm");
            if (this.state.smsResponse.status === "success") {
                toast.success("Gửi mã xác nhận thành công.");
                document.getElementById("not-existed-phone").classList.add("invisible");
                for (let i = 0; i < registerForm.length; i++) {
                    registerForm[i].style.display = "none";
                }
                for (let i = 0; i < otpConfirm.length; i++) {
                    otpConfirm[i].style.display = "block";
                }
            }
        } else {
            toast.error("Số điện thoại không tồn tại");
        }
        
        
    }

    handleOTPConfirmContinue = async(e) => {
        e.preventDefault();
        this.setState({smsResponse: this.props.authenticationReducer.smsResponse, allSalons: []})
        await this.props.handleVerifyOTPMessage(this.state.salonPhoneNumber, this.state.otpCode);
        if(this.props.authenticationReducer.verifyResponse.data.verified){
            cookies.set('phoneNumber', this.state.salonPhoneNumber, {
                maxAge: 30 * 24 * 3600,
                path: '/'
            });
            cookies.set('verified', true, {
                maxAge: 30 * 24 * 3600,
                path: '/'
            });
            window.location.href = "/salon/chg-pwd"
        } else {
            toast.error(`Mã xác nhận không đúng. Bạn còn ${this.props.authenticationReducer.verifyResponse.data.remainingAttempts} lần nhập.`)
        }
    }

    render() {
        return (
            <div>
                <Header/>
                <section id="login">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Nhập số điện thoại</h3>
                            </div>
                        </div>
                        <form className="salon-login-form">
                            <div className="form-group customer-register">
                                <label htmlFor="phoneNumber">Số điện thoại *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="phoneNumber"
                                    name="salonPhoneNumber"
                                    value={this.state.salonPhoneNumber}
                                    onChange={this.handlePhoneNumberChange}
                                    required/>
                                <div className="alert alert-danger invisible" id="not-existed-phone">
                                Số điện thoại không tồn tại
                                </div>
                            </div>
                            <div className="form-group otp-confirm">
                                <label htmlFor="inputOTP">Mã xác nhận</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="inputOTP"
                                    value={this.state.otpCode}
                                    onChange={this.handleOtpCodeChange}
                                    placeholder="1234"/>
                                <div id="otpAlert" className="alert alert-danger">
                                
                                </div>
                            </div>
                           
                            
                            <div className="alert alert-danger invisible">
                                Số điện thoại hoặc mật khẩu của bạn không đúng.
                            </div>
                            
                            
                            <div className="row action-button">
                                <button
                                    type="button"
                                    className="btn btn-primary btn-continue customer-register"
                                    onClick={this.handleForgetPwdContinue}>Tiếp tục</button>
                                <button
                                    type="button"
                                    className="btn btn-primary btn-back otp-confirm"
                                    onClick={this.handleOTPConfirmBack}>Quay lại</button>
                                    <button
                                    type="submit"
                                    className="btn btn-primary btn-salon-continue otp-confirm"
                                    onClick={this.handleOTPConfirmContinue}>Xác nhận</button>
                            </div>
                        </form>
                        <div className="row text-center">
                            <p className="text-center">Quay lại
                                <b> </b><NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/login'}} activeClassName="">Đăng nhập</NavLink>
                            </p>
                        </div>
                    </div>
                    <ToastContainer hideProgressBar={true} newestOnTop={true} autoClose={5000}/>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer, authenticationReducer: state.authenticationReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleSalonAuthenticate: async(credentials) => {
            await dispatch(salonAuthenticate(credentials))
        },
        handleSendOTPMessage: async(phones) => {
            await dispatch(createAndSendPinCode(phones))
        },
        handleVerifyOTPMessage: async(phone, pin_code) => {
            await dispatch(verifyPinCode(phone, pin_code))
        },
        handleGetSalonByPhoneNumber: async(phoneNumber) => {
            await dispatch(getSalonByPhoneNumber(phoneNumber))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordPage)