import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';
export const RECEIVE_BOOKING = 'RECEIVE_BOOKING';
export const RECEIVE_ALL_BOOKING = 'RECEIVE_ALL_BOOKING';
export const CREATE_BOOKING_SUCCESS = 'CREATE_BOOKING_SUCCESS';
export const CREATE_BOOKING_SERVICE_SUCCESS = 'CREATE_BOOKING_SERVICE_SUCCESS';
export const RECEIVE_SALON_BOOKING = 'RECEIVE_SALON_BOOKING';

export const receiveBookings = (bookings) => ({type: RECEIVE_BOOKING, bookings});
export const receiveSalonBookings = (salonBookings) => ({type: RECEIVE_SALON_BOOKING, salonBookings});
export const receiveAllBookings = (allBookings) => ({type: RECEIVE_ALL_BOOKING, allBookings});
export const createBookingSuccess = (booking) => ({type: CREATE_BOOKING_SUCCESS, booking});
export const createBookingServiceSuccess = (bookingService) => ({type: CREATE_BOOKING_SERVICE_SUCCESS, bookingService});

export function getBookingsBySalonId(salonId, dateStart, dateEnd) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getBookingsBySalonId(salonId, dateStart, dateEnd), 'GET')
            dispatch(receiveBookings(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getOpenBookingsBySalonIdAndDate(salonId, dateStart, dateEnd) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getOpenBookingsBySalonIdAndDate(salonId, dateStart, dateEnd), 'GET')
            dispatch(receiveSalonBookings(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getAllBookingsBySalonIdAndSlotTime(salonId, slotTime) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getAllBookingsBySalonIdAndSlotTime(salonId, slotTime), 'GET')
            dispatch(receiveAllBookings(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getBookingsByCustomerId(customerId, token) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithToken(token, endpoints.getAllBookingsByCustomerId(customerId), 'GET')
            dispatch(receiveBookings(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getAllBookingsByCustomerId(customerId, token) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getAllBookingsByCustomerId(customerId, token), 'GET')
            dispatch(receiveBookings(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getAllBookingsSpecifiedByCustomerAndSalonId(salonId) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getAllBookingsSpecifiedByCustomerAndSalonId(salonId), 'GET')
            dispatch(receiveBookings(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}


export function createBooking(booking) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createBooking, 'POST', booking);
            dispatch(createBookingSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function createBookingService(bookingService) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createBookingService, 'POST', bookingService);
            dispatch(createBookingServiceSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function updateBooking(booking) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createBooking, 'PUT', booking);
            dispatch(createBookingSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}