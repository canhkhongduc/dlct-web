import {
    ADMIN_AUTHENTICATE_SUCCESS,
} from '../actions/admin'

const initialState = {
    token: [],
}

export default function adminReducer(state = initialState, action) {
    switch (action.type) {
        case ADMIN_AUTHENTICATE_SUCCESS:
            return {
                ...state,
                token: JSON.parse(action.data),
                isFetching: false
            }
        default:
            return state;
    }
}
