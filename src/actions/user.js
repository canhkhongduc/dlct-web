import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';

export function registerUserSuccess(data) {
    return {type: REGISTER_USER_SUCCESS, data};
}

export function loginUserSuccess(data) {
    return {type: LOGIN_USER_SUCCESS, data};
}
export function getUserSuccess(data) {
    return {type: GET_USER_SUCCESS, data};
}
export function updateUserSuccess(data) {
    return {type: UPDATE_USER_SUCCESS, data};
}


export function registerNewUser(user) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.registerNewUser, 'POST', user);
            dispatch(registerUserSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function loginUser(credentials) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.loginUser, 'POST', credentials);
            dispatch(loginUserSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getUserById(id, token) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getUserById(id, token), 'GET');
            dispatch(getUserSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function updateUserInformation(user, token) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.updateUser(token), 'PATCH', user);
            dispatch(updateUserSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}