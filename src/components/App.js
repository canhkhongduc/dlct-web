import React, {Component} from 'react';
import Home from './homepage/Home'
import Footer from './layout/Footer'
import {withRouter} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
import NavBar from './layout/NavBar';
import SlotPage from './customer/SlotPage';
import CustomerRegisterPage from './customer/CustomerRegisterPage';
import SalonRegisterPage from './salon/SalonRegisterPage';
import BookingSuccessPage from './customer/BookingSuccessPage';
import LoginPage from './salon/LoginPage';
import HomePage from './salon/HomePage';
import AdminHomePage from './admin/Home';
import AdminLoginPage from './admin/Login';
import SalonSetting from './salon/SalonSetting';
import ForgetPassword from './salon/ForgetPassword';
import ChangePassword from './customer/ChangePassword';
import SalonProfile from './customer/SalonProfile';
import FloatingButton from './layout/FloatingButton';
import Login from './customer/Login';
import Register from './customer/Register';
import SearchResult from '../containers/SearchResultContainer';
import Blogs from './customer/Blogs';
import BlogDetail from './customer/BlogDetail';
import Profile from './customer/Profile';
import HistoryPage from './customer/History';
import Favorite from './customer/Favorite';
import NotificationPage from './customer/Notification';

export class App extends Component {
    render() {
        return (
            <div>
                <NavBar/>
                <FloatingButton />
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/blogs" exact component={Blogs}/>
                    <Route path="/blogs/:blogNormalizedName" component={BlogDetail}/>
                    <Route path="/account/profile" exact component={Profile}/>
                    <Route path="/account/history" component={HistoryPage}/>
                    <Route path="/account/favorite" component={Favorite}/>
                    <Route path="/account/notification" component={NotificationPage}/>
                    <Route path="/account/change-password" component={ChangePassword}/>
                    <Route path="/customer/:customerPhoneNumber" component={BookingSuccessPage}/>
                    <Route path="/salon/register" exact component={SalonRegisterPage}/>
                    <Route path="/salon/login" exact component={LoginPage}/>
                    <Route path="/salon/home" exact component={HomePage}/>
                    <Route path="/salon/booking" component={HomePage}/>
                    <Route path="/salon/setting" component={SalonSetting}/>
                    <Route path="/salon/forget-password" component={ForgetPassword}/>
                    <Route path="/salon/chg-pwd" component={ChangePassword}/>
                    <Route path="/admin/login" component={AdminLoginPage}/>
                    <Route path="/admin/salon" component={AdminHomePage}/>
                    <Route path="/search/:query" exact component={SearchResult}/>
                    <Route path="/:salonNormalizedName" exact component={SalonProfile}/>
                    <Route path="/:salonNormalizedName/date-time" component={SlotPage}/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}

export default withRouter(App);
