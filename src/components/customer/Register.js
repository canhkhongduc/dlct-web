import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import { ToastContainer, toast } from 'mdbreact';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import SocialButton from '../layout/SocialButton'
import { registerNewUser } from '../../actions/user';
const cookies = new Cookies();

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allSalons: [],
            token: [],
            loginSuccess: true,
            user: []
        };
    }
    componentWillMount = async(e) => {
        if(cookies.get("signedIn")) {
            window.location.href = "/";
        }
    }

    handleChooseCity = async(cityId, cityName) => {
        if (cityId !== 0) {
            document
                .getElementById('district-dropdown')
                .disabled = false;
            document
                .getElementById('city-dropdown')
                .innerHTML = cityName;
            await this
                .props
                .handleFetchDataDistrictsByCityId(cityId);
            this.setState({allDistricts: this.props.salonReducer.allDistricts, allSalons: []})
        }
    }

    handleChooseDistrict = async(districtId, cityId, districtName) => {
        if (cityId !== 0 && districtId !== 0) {
            document
                .getElementById('district-dropdown')
                .innerHTML = districtName;
            await this
                .props
                .handleFetchDataSalonsByCityIdAndDistrictId(districtId, cityId);
            this.setState({allSalons: this.props.salonReducer.allSalons})
        }
    }

    handleRegister = async(event) => {
        event.preventDefault();
        let pattern = /^[a-zA-Z0-9\s]+$/;
        let username = event.target.username.value;
        let password = event.target.password.value;
        let confirmPassword = event.target.confirmPassword.value;

        let errorMessage = document.getElementsByClassName("alert")[0];
        
        if (password === confirmPassword) {
            let data = {
                username: event.target.username.value,
                password: event.target.password.value,
                name: event.target.fullName.value,
                phoneNumber: event.target.phoneNumber.value,
                email: event.target.email.value,
            }
            console.log(data);
            await this
                .props
                .handleRegisterUser(data);
            if(this.props.authenticationReducer.user.length !== 0){
                cookies.set('role', "customer", {
                    maxAge: 24 * 3600,
                    path: '/'
                });
                cookies.set('username', username, {
                    maxAge: 24 * 3600,
                    path: '/'
                });
                cookies.set('signedIn', true, {
                    maxAge: 24 * 3600,
                    path: '/'
                });
                window.location.href = "/";
                
            } else {
                
            }
            
        } else {
            errorMessage.innerHTML = "Mật khẩu và mật khẩu xác nhận không trùng khớp";
            errorMessage.classList.remove("invisible");
        }
    }

    render() {
        const {loginSuccess} = this.state
        return (
            <div>
                <Header/>
                <section id="login">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Đăng ký tài khoản</h3>
                            </div>
                        </div>
                        <div className="row social-login-buttons">
                            <SocialButton
                            className="facebook-login"
                            provider='facebook'
                            appId='299958884238381'
                            onLoginSuccess={this.handleSocialLogin}
                            onLoginFailure={this.handleSocialLoginFailure}
                            >
                            Đăng nhập với Facebook
                            </SocialButton>
                        </div>
                        <form className="salon-login-form" onSubmit={this.handleRegister}>
                            
                            <div className="form-group">
                                <label htmlFor="username">Tài khoản *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="username"
                                    name="username"
                                    required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Mật khẩu</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="confirmPassword">Xác nhận mật khẩu</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email liên hệ *</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    id="email"
                                    name="email"
                                    required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="fullName">Họ và tên</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="fullName"
                                    name="fullName"
                                    required/>
                            </div>
                            <div className="form-group customer-register">
                                <label htmlFor="phoneNumber">Số điện thoại *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="phoneNumber"
                                    name="phoneNumber"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền số điện thoại
                                </div>
                            </div>

                            
                            
                            <div className="row action-button">
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-salon-continue"
                                    >Tiếp tục</button>
                            </div>
                        </form>
                        
                        <div className="row text-center">
                            <p className="text-center">Đã có tài khoản? 
                                <b> </b><NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/register'}} activeClassName="">Đăng nhập ngay</NavLink>
                            </p>
                        </div>
                    </div>
                    <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {authenticationReducer: state.authenticationReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleRegisterUser: async(user) => {
            await dispatch(registerNewUser(user))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)