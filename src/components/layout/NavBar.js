import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getSalonByPhoneNumber} from '../../actions/salon';
import Cookies from 'universal-cookie';
import { getCustomerByPhoneNumber } from '../../actions/customer';
import { NavLink } from 'react-router-dom';
import SocialButton from './SocialButton'
const cookies = new Cookies();

class NavBar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            salon: [],
            role:'',
            showModal: false,
            loading: false,
            error: null

        }
    }

    componentWillMount = async(e) => {
        let signedIn = cookies.get("signedIn");
        console.log(signedIn);
        let loginNavs = document.getElementsByClassName("sign-in-nav");
        
        let loggedinNavs = document.getElementsByClassName("signed-in-nav");
        
        if(signedIn) {
            document.querySelectorAll('.sign-in-nav').forEach((signinNav) => {
                signinNav.style.display = "none";
            });
            document.querySelectorAll('.signed-in-nav').forEach((signedInNav) => {
                signedInNav.style.display = "block";
            });
        } else {
            document.querySelectorAll('.sign-in-nav').forEach((signinNav) => {
                signinNav.style.display = "block";
            });
            document.querySelectorAll('.signed-in-nav').forEach((signedInNav) => {
                signedInNav.style.display = "none";
            });
        }  
    }

    componentDidMount = async(e) => {
        let signedIn = cookies.get("signedIn");
        console.log(signedIn);
        let loginNavs = document.getElementsByClassName("sign-in-nav");
        
        let loggedinNavs = document.getElementsByClassName("signed-in-nav");
        
        if(signedIn) {
            document.querySelectorAll('.sign-in-nav').forEach((signinNav) => {
                signinNav.style.display = "none";
            });
            document.querySelectorAll('.signed-in-nav').forEach((signedInNav) => {
                signedInNav.style.display = "inherit";
            });
        } else {
            document.querySelectorAll('.sign-in-nav').forEach((signinNav) => {
                signinNav.style.display = "block";
            });
            document.querySelectorAll('.signed-in-nav').forEach((signedInNav) => {
                signedInNav.style.display = "none";
            });
        }  
    }
    handleNavBooking = () => {
        window.location.href = `/#booking`;
    }
    handleNavSalon = () => {
        window.location.href = `/salon/login`;
    }
    handleLogoClick = () => {
        if(this.state.role !== "salon") {
            window.location.href = "/";
        }
    }
    handleNavSalonHome = () => {
        if(this.state.role !== "salon") {
            window.location.href = "/";
        }
    }
    handleCustomerBooking = (e) => {
        let phoneNumber = cookies.get('phoneNumber');
        if(phoneNumber) {
            window.location.href = `/customer/${phoneNumber}`;
        }
    }
    showProfileMenu = (e) => {
        document.getElementById("profile-menu").style.display = "block";
    }
    hideProfileMenu = (e) => {
        document.getElementById("profile-menu").style.display = "none";
    }
    handleLogout = (e) => {
        cookies.remove('role', { path: '/' });
        cookies.remove('phoneNumber', { path: '/' });
        cookies.remove('username', { path: '/' });
        cookies.remove('signedIn', { path: '/' });
        cookies.remove('token', { path: '/' });
        window.location.href = "/";
    }
    render() {
        const profileUserNameStyle = {
            paddingLeft: "0px",
            marginTop: "22px"
        }
        const username = cookies.get("username");
        return (
            <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
                <div className="container">
                <NavLink className="js-scroll-trigger margin-top" data-target="#link1" to={{pathname: '/'}} activeClassName="">
                <img src="/img/logo/logo.png" className="logo-image"/>
                </NavLink>
                    <button
                        className="navbar-toggler navbar-toggler-right"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarResponsive"
                        aria-controls="navbarResponsive"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        DLCT
                        <i className="fas fa-bars"></i>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent" id="navbarResponsive">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <NavLink className="nav-link" data-target="#link1" to={{pathname: '/blogs'}} activeClassName="active">Bài viết </NavLink>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Liên hệ <span className="sr-only">(current)</span></a>
                        </li>
                    </ul>

                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item margin-top">
                            <NavLink className="sign-in-nav nav-link js-scroll-trigger" data-target="#link1" to={{pathname: '/login'}} activeClassName="active">Đăng nhập</NavLink>
                        </li>
                        <li className="nav-item margin-top">
                            <NavLink className="sign-in-nav nav-link js-scroll-trigger" data-target="#link1" to={{pathname: '/register'}} activeClassName="active">Đăng ký</NavLink>
                        </li>
                        <li className="nav-item margin-top">
                            <NavLink className="sign-in-nav nav-link js-scroll-trigger salon-register-nav" data-target="#link1" to={{pathname: '/salon/login'}} activeClassName="active">Trở thành chủ salon</NavLink>
                        </li>
                        <li className="signed-in-nav">
                            <img onMouseOver={this.showProfileMenu} src="/img/user/user.png" className="nav-link js-scroll-trigger profile-image"/><span onMouseOver={this.showProfileMenu} className="profile-username" style={profileUserNameStyle}>{username ? username : "tentaikhoan"}</span>
                            <ul className="dropdown-menu profile-dropdown" id="profile-menu" onMouseOver={this.showProfileMenu} onMouseOut={this.hideProfileMenu}>
                                <li className="profile-item"><NavLink data-target="#link1" to={{pathname: '/account/profile'}}>Trang cá nhân </NavLink></li>
                                <li className="profile-item"><NavLink data-target="#link1" to={{pathname: '/account/notification'}}>Thông báo của tôi </NavLink></li>
                                <li className="profile-item"><NavLink data-target="#link1" to={{pathname: '/account/history'}}>Lịch sử đặt lịch </NavLink></li>
                                <li className="profile-item"><NavLink data-target="#link1" to={{pathname: '/account/favorite'}}>Yêu thích </NavLink></li>
                                <li className="profile-item"><NavLink data-target="#link1" to={{pathname: '/account/change-password'}}>Đổi mật khẩu </NavLink></li>
                                <li className="profile-item"><a href="#" onClick={this.handleLogout}>Đăng xuất</a></li>
                            </ul>
                            
                        </li>
                  </ul>
                </div>
                </div>
                
            </nav>
        )
    }
}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer, authenticationReducer: state.authenticationReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleFetchSalonByPhoneNumber: async(phoneNumber) => {
            await dispatch(getSalonByPhoneNumber(phoneNumber))
        },
        handleGetCustomerByPhoneNumber: async(phoneNumber) => {
            await dispatch(getCustomerByPhoneNumber(phoneNumber))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar)
