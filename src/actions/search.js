import axios from 'axios';
import {SEARCH_SALONS_URL} from '../constants/endpoints'; 

export const SEARCH_SALONS_SUCCESS = 'SEARCH_SALONS_SUCCESS';

export const searchSalonsSuccess = (results) => ({
    type: SEARCH_SALONS_SUCCESS,
    payload: {
        results
    }
})

export const searchForSalons = (query) => {
    return (dispatch) => {
      return axios.get(`${SEARCH_SALONS_URL}?query=${query}`)
        .then(response => {
          dispatch(searchSalonsSuccess(response.data.result))
        })
        .catch(error => {
          throw(error)
        });
    }
}