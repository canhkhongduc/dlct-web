import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import {getSalonByNormalizedName} from '../../actions/salon';
import {fetchDataCityById} from '../../actions/city';
import {fetchDataDistrictById} from '../../actions/district';
import dateFormat from 'dateformat';
import moment from 'moment';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import { ToastContainer, toast } from 'mdbreact';
import { getCustomerByPhoneNumber } from '../../actions/customer';
import { createBooking, getBookingsByCustomerId, updateBooking, getOpenBookingsBySalonIdAndDate, createBookingService } from '../../actions/booking';
import Dialog from 'react-bootstrap-dialog'
import { getUnavailableSlotsBySalonId } from '../../actions/unavailableSlot';
import DayPicker from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/vi';
import 'react-day-picker/lib/style.css';
import { getSchedulesBySalonIdAndWeekDay } from '../../actions/schedule';
import ProfileSideNav from '../layout/ProfileSideNav';
import { getUserById, updateUserInformation } from '../../actions/user';
const cookies = new Cookies();

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.input = React.createRef();
        this.state = {
            user: [],
            updateUser: [],

        };
    }


    
    componentWillMount = async() => {
        let userId = cookies.get("userId");
        let token = cookies.get("token");
        await this.props.handleGetUserById(userId, token);
        if(this.props.authenticationReducer.user.length !== 0) {
            this.setState({
                user: this.props.authenticationReducer.user
            })
        }
    }

    handleUpdateUserInformation = async(event) => {
        event.preventDefault();
        let phoneNumber = event.target.phoneNumber.value;
        let user = this.state.user;
        user.username = event.target.username.value;
        user.email = event.target.email.value;
        user.name = event.target.name.value;
        user.phoneNumber = event.target.phoneNumber.value;
        await this.props.handleUpdateUserInformation(user, cookies.get("token"));
        if(this.props.authenticationReducer.updateUser.length !== 0) {
            toast.success("Cập nhật thành công");
            this.setState({
                user: user
            })
        }
    }
    render() {
        const {user} = this.state;
        const currentNav = "password-nav";
        return (
            <div>
                <Header/>
                <section id="booking">
                    <div className="container">
                        <div className="row">
                            <ProfileSideNav currentNav={currentNav}/>
                            <div className="col-md-8">
                            <div className="row margin-bottom">
                                Đổi mật khẩu  
                            </div>
                            <form onSubmit={this.handleSavePassword}>
                                <div className="form-group">
                                    <label htmlFor="password">Mật khẩu mới *</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        name="password"
                                        pattern=".{8,}" 
                                        title="Mật khẩu phải nhiều hơn 8 ký tự"
                                        required/>
                                    <div className="invalid-feedback">
                                    Vui lòng điền mật khẩu
                                    </div>
                                </div>
                            
                                <div className="form-group">
                                    <label htmlFor="confirmPassword">Xác nhận Mật khẩu *</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="confirmPassword"
                                        name="confirmPassword"
                                        pattern=".{8,}" 
                                        title="Mật khẩu phải nhiều hơn 8 ký tự"
                                        required/>
                                    <div className="invalid-feedback">
                                    Vui lòng điền mật khẩu
                                    </div>
                                </div>
                                <div className="row action-button">
                                    <button
                                        type="submit"
                                        className="btn btn-primary btn-salon-continue"
                                        >Tiếp tục</button>
                                </div>
                            </form>
                            <div className="row text-center">
                                <p className="text-center">Quay trở lại 
                                    <b> </b><a className="login-text" onClick={this.handleBack}>Đăng nhập</a>
                                </p>
                            </div>
                            </div>
                            
                        </div>
                        
                        
                    </div>
                </section>
                <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />  
                <Dialog ref={(component) => { this.dialog = component }} />
            </div>
        );
    }

}


function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        cityReducer: state.cityReducer, 
        districtReducer: state.districtReducer, 
        scheduleReducer: state.scheduleReducer,
        authenticationReducer: state.authenticationReducer,
        bookingReducer: state.bookingReducer,
        unavailableSlotReducer: state.unavailableSlotReducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetUserById: async(id, token) => {
            await dispatch(getUserById(id, token));
        },
        handleUpdateUserInformation: async(user, token) => {
            await dispatch(updateUserInformation(user, token));
        },
        handleCreateBooking: async(booking) => {
            await dispatch(createBooking(booking))
        },
        handleCreateBookingService: async(booking) => {
            await dispatch(createBookingService(booking))
        },
        handleGetBookingsByCustomerId: async(customerId) => {
            await dispatch(getBookingsByCustomerId(customerId))
        },
        handleUpdateBooking: async(booking) => {
            await dispatch(updateBooking(booking))
        },
        getOpenBookingsBySalonIdAndDate: async(salonId, dateStart, dateEnd) => {
            await dispatch(getOpenBookingsBySalonIdAndDate(salonId, dateStart, dateEnd))
        },
        handleGetUnavailableSlotBySalonId: async(salonId) => {
            await dispatch(getUnavailableSlotsBySalonId(salonId))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)