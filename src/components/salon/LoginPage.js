import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import { salonAuthenticate } from '../../actions/salon';
import { ToastContainer, toast } from 'mdbreact';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
const cookies = new Cookies();

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCities: [],
            allDistricts: [],
            allSalons: [],
            token: [],
            loginSuccess: true,
        };
    }
    componentWillMount = async(e) => {
        this.setState({allCities: this.props.salonReducer.allCities, allSalons: []})
    }

    handleChooseCity = async(cityId, cityName) => {
        if (cityId !== 0) {
            document
                .getElementById('district-dropdown')
                .disabled = false;
            document
                .getElementById('city-dropdown')
                .innerHTML = cityName;
            await this
                .props
                .handleFetchDataDistrictsByCityId(cityId);
            this.setState({allDistricts: this.props.salonReducer.allDistricts, allSalons: []})
        }
    }

    handleChooseDistrict = async(districtId, cityId, districtName) => {
        if (cityId !== 0 && districtId !== 0) {
            document
                .getElementById('district-dropdown')
                .innerHTML = districtName;
            await this
                .props
                .handleFetchDataSalonsByCityIdAndDistrictId(districtId, cityId);
            this.setState({allSalons: this.props.salonReducer.allSalons})
        }
    }

    handleLogin = async(event) => {
        event.preventDefault();
        let phoneNumber = event.target.phoneNumber.value;
        let credentials = {
            phoneNumber: phoneNumber,
            password: event.target.password.value,
        };
        await this.props.handleSalonAuthenticate(credentials);
        if(this.props.salonReducer.token.length !== 0){
            cookies.set('token', this.props.salonReducer.token, {
                maxAge: 24 * 3600,
                path: '/'
            });
            cookies.set('role', "salon", {
                maxAge: 24 * 3600,
                path: '/'
            });
            cookies.set('phoneNumber', phoneNumber, {
                maxAge: 24 * 3600,
                path: '/'
            });
            toast.success("Đăng nhập thành công.");
            setTimeout(function() {
                window.location.href=`/salon/home`;
            }, 1000);
            
        } else {
            console.clear();
            this.setState({
                loginSuccess: false
            });
        }
    }

    render() {
        const {loginSuccess} = this.state
        return (
            <div>
                <Header/>
                <section id="login">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Đăng nhập</h3>
                            </div>
                        </div>
                        <form className="salon-login-form" onSubmit={this.handleLogin}>
                            <div className="form-group customer-register">
                                <label htmlFor="phoneNumber">Số điện thoại *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="phoneNumber"
                                    name="salonPhoneNumber"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền số điện thoại
                                </div>
                            </div>
                           
                            <div className="form-group">
                                <label htmlFor="password">Mật khẩu *</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền mật khẩu
                                </div>
                            </div>
                            
                            
                            <div className={loginSuccess? "alert alert-danger invisible":"alert alert-danger"}>
                                Số điện thoại hoặc mật khẩu của bạn không đúng.
                            </div>
                            
                            
                            <div className="row action-button">
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-salon-continue"
                                    >Tiếp tục</button>
                            </div>
                        </form>
                        <div className="row text-center">
                            <p className="text-center">Chưa có tài khoản? 
                                <b> </b><NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/register'}} activeClassName="">Đăng ký ngay</NavLink>
                            </p>
                        </div>
                        <div className="row text-center">
                            <p className="text-center">
                            <NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/forget-password'}} activeClassName="">Quên mật khẩu</NavLink>
                            </p>
                        </div>
                    </div>
                    <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />
                </section>
            </div>
        );
    }
    displayCities = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <a
                    className="dropdown-item"
                    onClick={() => this.handleChooseCity(element.id, element.name)}
                    key={index}>{element.name}</a>
            ))
        }
        return result
    }
    displayDistricts = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <a
                    className="dropdown-item"
                    onClick={() => this.handleChooseDistrict(element.id, element.cityId, element.name)}
                    key={index}>{element.name}</a>
            ))
        }
        return result
    }
    displaySalons = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div
                    className="salon-item"
                    onClick={() => this.handleChooseSalon(element.id)}
                    key={index}>
                    <p className="salon-name">{element.name}</p>
                    <p className="salon-address">{element.address}</p>
                </div>
            ))
        }
        return result
    }
}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleSalonAuthenticate: async(credentials) => {
            await dispatch(salonAuthenticate(credentials))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)