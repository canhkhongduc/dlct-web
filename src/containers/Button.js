import React from 'react'
import { connect } from 'react-redux'
import { fetchPosts } from '../actions'

let Button = ({ getPosts, salon }) => (
  <button
    onClick={() => { getPosts(salon) }}
    className="btn btn-primary btn-lg btn-block" >
    Get bookings
  </button>
);

const mapStateToProps = (state) => ({
  salon: state.salon
})

const mapDispatchToProps = {
  getPosts: fetchPosts
}

Button = connect(
  mapStateToProps,
  mapDispatchToProps
)(Button)

export default Button;
