import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header';
import {getSalonByNormalizedName} from '../../actions/salon';
import {fetchDataCityById} from '../../actions/city';
import {fetchDataDistrictById} from '../../actions/district';
import {createAndSendPinCode, verifyPinCode, getCustomerByPhoneNumber, createCustomer} from '../../actions/customer';
import {createBooking} from '../../actions/booking';
import {ToastContainer, toast} from 'mdbreact';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class CustomerRegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            smsResponse: [],
            registeredDate: '',
            salonNormalizedName: '',
        };
    }

    componentWillMount = async(e) => {
        const {salonNormalizedName} = this.props.match.params;
        await this
            .props
            .handleGetSalonByNormalizedName(salonNormalizedName);
        await this
            .props
            .handleFetchDistrictById(this.props.salonReducer.salon[0].districtId);
        if (this.props.districtReducer.district) {
                await this
                    .props
                    .handleFetchCityById(this.props.districtReducer.district[0].cityId);
            }
        const {date, time} = this.props.match.params;
        this.setState({
            chosenDay: date,
            chosenSlot: time.replace("%3A", ":"),
            registeredDate: `${date}T${time.replace("%3A", ":")}:00Z`,
            salon: this.props.salonReducer.salon[0],
            salonNormalizedName: salonNormalizedName,
            city: this.props.cityReducer.city[0],
            district: this.props.districtReducer.district[0]
        });
        document.getElementById("order-information").innerHTML = "<b>Tiệm " + this.state.salon.name + "</b>, " + this.state.salon.address + ", " + this.state.district.name + ", " + this.state.city.name;
    }

    handleNameChange = (e) => {
        this.setState({customerName: e.target.value});
    }
    handlePhoneNumberChange = (e) => {
        this.setState({customerPhoneNumber: e.target.value});
    }
    handleOtpCodeChange = (e) => {
        this.setState({otpCode: e.target.value});
    }
    handleRegisterContinue = async(e) => {
        // const customerName = this.state.customerName;
        const customerPhoneNumber = this.state.customerPhoneNumber;
        await this
            .props
            .handleSendOTPMessage(customerPhoneNumber);
        console.clear();
        if (this.props.authenticationReducer.smsResponse.status === "error") {
            toast.error('Số điện thoại không hợp lệ.');
            
        } else {
            toast.success('Gửi mã xác nhận thành công. Vui lòng kiểm tra tin nhắn của bạn.', {position: "top-right"});
            this.setState({smsResponse: this.props.authenticationReducer.smsResponse, allSalons: []})
            let registerForm = document.getElementsByClassName("customer-register");
            let otpConfirm = document.getElementsByClassName("otp-confirm");
            if (this.state.smsResponse.status === "success") {
                for (let i = 0; i < registerForm.length; i++) {
                    registerForm[i].style.display = "none";
                }
                for (let i = 0; i < otpConfirm.length; i++) {
                    otpConfirm[i].style.display = "block";
                }
            }
        }

    }
    handleRegisterBack = async(e) => {
        window.location.href = `/${this.state.salonNormalizedName}`;
    }
    handleOTPConfirmBack = async(e) => {
        this.setState({smsResponse: this.props.authenticationReducer.smsResponse, allSalons: []})
        let registerForm = document.getElementsByClassName("customer-register");
        let otpConfirm = document.getElementsByClassName("otp-confirm");
        for (let i = 0; i < registerForm.length; i++) {
            registerForm[i].style.display = "block";
        }
        for (let i = 0; i < otpConfirm.length; i++) {
            otpConfirm[i].style.display = "none";
        }
    }
    handleOTPConfirmContinue = async(e) => {
        await this
            .props
            .handleVerifyOTPMessage(this.state.customerPhoneNumber, this.state.otpCode);
        
        if (this.props.authenticationReducer.verifyResponse.data.verified) {
            toast.success("Xác nhận thành công!");
            this.saveCustomerToCookie(this.state.customerName, this.state.customerPhoneNumber);
            await this
                .props
                .handleGetCustomerByPhoneNumber(this.state.customerPhoneNumber);
            if (this.props.authenticationReducer.getCustomer.length === 0) {
                let customer = {
                    name: this.state.customerName,
                    activated: true,
                    phoneNumber: this.state.customerPhoneNumber
                }
                await this
                    .props
                    .handleCreateCustomer(customer);
                if (this.props.authenticationReducer.customer) {
                    let booking = {
                        customerId: this.props.authenticationReducer.customer.id,
                        salonId: this.state.salon.id,
                        status: 0,
                        time: this.state.registeredDate
                    }
                    await this
                        .props
                        .handleCreateBooking(booking);
                    if (this.props.bookingReducer.booking) {
                        window.location.href = `/customer/${this.state.customerPhoneNumber}`;
                    }
                }
            } else {
                let booking = {
                    customerId: this.props.authenticationReducer.getCustomer[0].id,
                    salonId: this.state.salon.id,
                    status: 0,
                    time: this.state.registeredDate
                }
                await this
                    .props
                    .handleCreateBooking(booking);
                if (this.props.bookingReducer.booking) {
                    window.location.href = `/customer/${this.state.customerPhoneNumber}`;
                }
            }
        } else {
            toast.error(`Mã xác nhận không đúng. Bạn còn ${this.props.authenticationReducer.verifyResponse.data.remainingAttempts} lần nhập.`);
        }
    }

    saveCustomerToCookie = (customerName, customerPhoneNumber) => {
        cookies.set('customerName', customerName, {
            maxAge: 30 * 24 * 3600,
            path: '/'
        });
        cookies.set('phoneNumber', customerPhoneNumber, {
            maxAge: 30 * 24 * 3600,
            path: '/'
        });
        cookies.set('role', "customer", {
            maxAge: 30 * 24 * 3600,
            path: '/'
        });
    }

    render() {
        const {chosenDay, chosenSlot} = this.state;
        return (
            <div>
                <Header/>
                <section id="">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Hoàn thiện thông tin để xác nhận đặt lịch</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <p id="order-information"></p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <p>{chosenSlot}, {getDayOfWeek(new Date(chosenDay).getDay())}, ngày {chosenDay}</p>
                            </div>
                        </div>
                        <form className="register-form">
                            <div className="form-group customer-register">
                                <label htmlFor="inputName">Tên của bạn *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="inputName"
                                    value={this.state.customerName}
                                    onChange={this.handleNameChange}
                                    placeholder="Nguyễn Văn A"
                                    required/>
                            </div>
                            <div className="form-group customer-register">
                                <label htmlFor="inputPhoneNumber">Số điện thoại *</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="inputPhoneNumber"
                                    value={this.state.customerPhoneNumber}
                                    onChange={this.handlePhoneNumberChange}
                                    placeholder="0123456789"
                                    required/>
                            </div>
                            <div className="form-group otp-confirm">
                                <label htmlFor="inputOTP">Mã xác nhận</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="inputOTP"
                                    value={this.state.otpCode}
                                    onChange={this.handleOtpCodeChange}
                                    placeholder="1234"/>
                                <div id="otpAlert" className="alert alert-danger invisible"></div>
                            </div>
                            <p className="text-center otp-confirm">
                                <a id="otp-confirm-text">Gửi lại mã xác nhận</a>
                            </p>
                            <div className="row action-button">
                                <button
                                    type="button"
                                    className="btn btn-primary btn-back customer-register"
                                    onClick={this.handleRegisterBack}>Quay lại</button>
                                <button
                                    type="button"
                                    className="btn btn-primary btn-continue customer-register"
                                    onClick={this.handleRegisterContinue}>Tiếp tục</button>
                                <button
                                    type="button"
                                    className="btn btn-primary btn-back otp-confirm"
                                    onClick={this.handleOTPConfirmBack}>Quay lại</button>
                                <button
                                    type="button"
                                    className="btn btn-primary btn-continue otp-confirm"
                                    onClick={this.handleOTPConfirmContinue}>Tiếp tục</button>
                            </div>
                        </form>
                    </div>
                    <ToastContainer hideProgressBar={true} newestOnTop={true} autoClose={5000}/>
                </section>
            </div>
        );
        
    }
}

function getDayOfWeek(dayOfWeek) {
    switch (dayOfWeek) {
        case 1:
            return 'Thứ hai';
        case 2:
            return 'Thứ ba';
        case 3:
            return 'Thứ tư';
        case 4:
            return 'Thứ năm';
        case 5:
            return 'Thứ sáu';
        case 6:
            return 'Thứ bảy';
        case 0:
            return 'Chủ nhật';
        default:
            return '';
    }
}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer, authenticationReducer: state.authenticationReducer, bookingReducer: state.bookingReducer, cityReducer: state.cityReducer, districtReducer: state.districtReducer};
}

function mapDispatchToProps(dispatch) {
    return {
        handleSendOTPMessage: async(phones) => {
            await dispatch(createAndSendPinCode(phones))
        },
        handleVerifyOTPMessage: async(phone, pin_code) => {
            await dispatch(verifyPinCode(phone, pin_code))
        },
        handleGetCustomerByPhoneNumber: async(phoneNumber) => {
            await dispatch(getCustomerByPhoneNumber(phoneNumber))
        },
        handleCreateCustomer: async(customer) => {
            await dispatch(createCustomer(customer))
        },
        handleCreateBooking: async(booking) => {
            await dispatch(createBooking(booking))
        },
        handleGetSalonByNormalizedName: async(salonNormalizedName) => {
            await dispatch(getSalonByNormalizedName(salonNormalizedName));
        },
        handleFetchCityById: async(cityId) => {
            await dispatch(fetchDataCityById(cityId));
        },
        handleFetchDistrictById: async(districtId) => {
            await dispatch(fetchDataDistrictById(districtId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerRegisterPage)