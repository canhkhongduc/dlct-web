import * as apiRequest from '../util/apiRequest';
import * as endpoints from '../constants/endpoints';
export const RECEIVE_UNVAILABLE_SLOTS = 'RECEIVE_UNVAILABLE_SLOTS';
export const RECEIVE_UNVAILABLE_SLOT = 'RECEIVE_UNVAILABLE_SLOT';
export const CREATE_UNVAILABLE_SLOTS = 'CREATE_UNVAILABLE_SLOTS';

export const receiveUnavailableSlots = (unavailableSlots) => ({type: RECEIVE_UNVAILABLE_SLOTS, unavailableSlots});
export const receiveUnavailableSlot = (unavailableSlot) => ({type: RECEIVE_UNVAILABLE_SLOT, unavailableSlot});
export const createUnavailableSlotsSuccess = (unavailableSlot) => ({type: CREATE_UNVAILABLE_SLOTS, unavailableSlot});

export function getUnavailableSlotsBySalonId(salonId) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getUnavailableSlotsBySalonId(salonId), 'GET')
            dispatch(receiveUnavailableSlots(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function getUnavailableSlotBySalonIdAndSlotTime(salonId, slotTime) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.getUnavailableSlotBySalonIdAndSlotTime(salonId, slotTime), 'GET')
            dispatch(receiveUnavailableSlot(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function createUnavailableSlot(unavailableSlot) {
    return async(dispatch) => {
        try {
            const res = await apiRequest.requestWithoutToken(endpoints.createUnavailableSlot, 'POST', unavailableSlot);
            dispatch(createUnavailableSlotsSuccess(JSON.stringify(res)));
        } catch (err) {
            // console.log(err)
        }
    }
}

export function deleteUnavailableSlot(id) {
    return async(dispatch) => {
        try {
            await apiRequest.requestWithoutToken(endpoints.deleteUnavailableSlot(id), 'DELETE');
        } catch (err) {
            // console.log(err)
        }
    }
}