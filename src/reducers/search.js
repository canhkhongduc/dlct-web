import { SEARCH_SALONS_SUCCESS } from '../actions/search';

const searchResults = (state = [], action) => {
    switch (action.type) {
      case SEARCH_SALONS_SUCCESS:
        return action.payload
      default:
        return state
    }
  }
  
  export default searchResults