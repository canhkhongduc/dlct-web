import React, {Component} from 'react';
import HeaderHome from '../layout/HeaderHome';
import PropTypes from 'prop-types';

class SearchResult extends Component {

    constructor(props) {
        super(props);
        this.searchRef = React.createRef();
    }

    componentDidMount() {
        this.props.searchForSalons(this.props.match.params.query);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.match.params.query !== this.props.match.params.query) {
            this.props.searchForSalons(newProps.match.params.query);
        }
    }

    componentDidUpdate() {
        if (this.props.results) {
            this.scrollToSearchRef();
        }
    }

    scrollToSearchRef() {
        this.searchRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' })
    }

    render() {
        const {results} = this.props;
        return (
            <div>
                <HeaderHome/>
                <section className="bg-light" id="portfolio">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h2 className="section-heading text-uppercase" ref={this.searchRef}>Kết quả tìm kiếm cho "{this.props.match.params.query}"</h2>
                            </div>
                        </div>
                        <div className="row">
                            {results && results.map((salon) => {
                                return (
                                    <div className="col-md-4 col-sm-6 portfolio-item" key={salon.id}>
                                        <a className="portfolio-link" data-toggle="modal" href={`/salon/${salon.normalizedName}`}>
                                            <div className="portfolio-hover">
                                            <div className="portfolio-hover-content">
                                                <i className="fas fa-info-circle fa-3x"></i>
                                            </div>
                                            </div>
                                            <img className="img-fluid" src="/img/portfolio/01-thumbnail.jpg" alt="" />
                                        </a>
                                        <div className="portfolio-caption">
                                            <h4>{salon.name}</h4>
                                            <p className="text-muted">{salon.address}</p>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

SearchResult.propTypes = {
    results: PropTypes.array
}

export default SearchResult