import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import SideNavBar from '../layout/SideNavBar';
import {getAllSalons, updateSalon} from '../../actions/salon';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const statusTypes = {
    0: 'Chưa kích hoạt',
    1: 'Đã kích hoạt',
    2: 'Bị cấm'
};

function dateFormatter(cell, row) {
    return `${('0' + new Date(cell).getDate()).slice(-2)}/${('0' + (new Date(cell).getMonth() + 1)).slice(-2)}/${new Date(cell).getFullYear()}`;
}

function enumFormatter(cell, row, enumObject) {
    return enumObject[cell];
}


class AdminHomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allSalons: [],

        };

    }

    componentWillMount = async(e) => {
        let role = cookies.get("role");
        if(role === "admin") {
            await this.props.handleGetAllSalons();
            if(this.props.salonReducer.allSalons) {
                this.setState({
                    allSalons: this.props.salonReducer.allSalons
                });
            }
        } else {
            window.location.href = "/";
        }
    }

    onAfterSaveCell = async(row) => {
        // window.location.reload();
    }

    onBeforeSaveCell = async(row, cellName, cellValue) => {
        await this.props.handleUpdateSalon(row);
    }

    


    render() {
        const {allSalons} = this.state;
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell

        };
        return (
            <div>
                <Header/>
                <div className="row">
                    <SideNavBar/>
                    <div className="col-md-10 main-salon-content">
                        <div className="row">
                            <h3 className="text-center">Manage Salons</h3>
                        </div>
                        
                        <div className="row">
                            <BootstrapTable
                                data={allSalons}
                                striped
                                hover
                                pagination
                                cellEdit={cellEditProp}>
                                <TableHeaderColumn
                                    isKey
                                    width={'5%'}
                                    dataField='id'
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>ID</TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField='name'
                                    width={'15%'}
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>Tên tiệm</TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField='phoneNumber'
                                    width={'12%'}
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>Số điện thoại</TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField='email'
                                    width={'15%'}
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>Email</TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField='address'
                                    width={'20%'}
                                    editable={false}
                                    filter={{
                                    type: 'TextFilter',
                                    delay: 100
                                }}>Địa chỉ</TableHeaderColumn>
                                <TableHeaderColumn
                                    width={'20%'}
                                    dataField='createdDate'
                                    editable={false}
                                    dataFormat={dateFormatter}
                                    filter={ { type: 'DateFilter' } }>Ngày đăng ký</TableHeaderColumn>
                                
                                <TableHeaderColumn
                                    width={'15%'}
                                    dataField='status'
                                    filterFormatted dataFormat={ enumFormatter }
                                    formatExtraData={ statusTypes }
                                    filter={ { type: 'SelectFilter', options: statusTypes }}
                                    editable={{
                                    type: 'select',
                                    options: {
                                        values: [0,1,2]
                                    }
                                    }}>
                                    Trạng thái
                                </TableHeaderColumn>
                            </BootstrapTable>
                        </div>

                    </div>
                    <div className="alert alert-success invisible" id="alert-success"></div>
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        salonReducer: state.salonReducer, 
        scheduleReducer: state.scheduleReducer, 
        bookingReducer: state.bookingReducer, 
        authenticationReducer: state.authenticationReducer,
        unavailableSlotReducer: state.unavailableSlotReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetAllSalons: async() => {
            await dispatch(getAllSalons())
        },
        handleUpdateSalon: async(salon) => {
            await dispatch(updateSalon(salon))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminHomePage)

