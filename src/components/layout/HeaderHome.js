import React, {Component} from 'react'
import {connect} from 'react-redux'
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
class HeaderHome extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            searchQuery: ''
        }
        this.handleSearch = this.handleSearch.bind(this)
        this.handleSearchChange = this.handleSearchChange.bind(this)
    }

    handleSearch(event) {
        this.props.history.push(`/search/${this.state.searchQuery}`)
        event.preventDefault();
    }

    handleSearchChange(event) {
        this.setState({ searchQuery: event.target.value })
    }

    render() {
        return (
            <div>
                <header className="masthead">
                    <div className="container">
                        <div className="intro-text">
                            <div className="search-form">
                                <form onSubmit={this.handleSearch}>
                                    <fieldset>
                                    <legend>Tìm kiếm salon và đặt lịch trực tuyến</legend>
                                    </fieldset>
                                    <div className="inner-form">
                                    <div className="input-field first-wrap">
                                        <input id="search" type="text" value={this.state.searchQuery} placeholder="Tên salon, địa chỉ, số điện thoại, thành phố,..." onChange={this.handleSearchChange} />
                                    </div>
                                    <div className="input-field third-wrap">
                                        <button className="btn-search btn-primary" type="submit">Tìm kiếm</button>
                                    </div>
                                    </div>
                                </form>
                            </div>
                            {/* <NavLink className="btn btn-primary btn-xl text-uppercase js-scroll-trigger" data-target="#link1" to={{pathname: '/salon/register'}} activeClassName="">Đăng ký ngay</NavLink> */}
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

export default connect(null, null)(withRouter(HeaderHome))
