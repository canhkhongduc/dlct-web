import { connect } from 'react-redux'
import { searchForSalons } from '../actions/search'
import SearchResult from '../components/salon/SearchResult';


const mapStateToProps = state => ({
    results: state.search.results
})

const mapDispatchToProps = dispatch => ({
  searchForSalons: query => dispatch(searchForSalons(query))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResult)