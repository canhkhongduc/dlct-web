import {RECEIVE_UNVAILABLE_SLOT, RECEIVE_UNVAILABLE_SLOTS, CREATE_UNVAILABLE_SLOTS} from '../actions/unavailableSlot';

const initialState = {};

export default function unavailableSlotReducer(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_UNVAILABLE_SLOT:
            return {
                ...state,
                unavailableSlot: JSON.parse(action.unavailableSlot)
            }
        case RECEIVE_UNVAILABLE_SLOTS:
            return {
                ...state,
                unavailableSlots: JSON.parse(action.unavailableSlots)
            }
        case CREATE_UNVAILABLE_SLOTS:
            return {
                ...state,
                unavailableSlot: JSON.parse(action.unavailableSlot)
            }
        default:
            return state;
    }
}