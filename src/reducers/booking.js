import {RECEIVE_BOOKING, CREATE_BOOKING_SUCCESS, RECEIVE_ALL_BOOKING, RECEIVE_SALON_BOOKING, CREATE_BOOKING_SERVICE_SUCCESS} from '../actions/booking';

const initialState = {};

export default function bookingReducer(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_BOOKING:
            return {
                ...state,
                bookings: JSON.parse(action.bookings)
            }
        case RECEIVE_ALL_BOOKING:
            return {
                ...state,
                allBookings: JSON.parse(action.allBookings)
            }
        case CREATE_BOOKING_SUCCESS:
            return {
                ...state,
                booking: JSON.parse(action.booking)
            }
        case CREATE_BOOKING_SERVICE_SUCCESS:
            return {
                ...state,
                bookingService: JSON.parse(action.bookingService)
            }
        case RECEIVE_SALON_BOOKING:
            return {
                ...state,
                salonBookings: JSON.parse(action.salonBookings)
            }
        default:
            return state;
    }
}