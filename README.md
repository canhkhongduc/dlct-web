A web application for hair salon owners to manage their salon, customers and bookings using React-Redux

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your application is ready to be deployed!


## Project structure
1. actions
    Request data from server through API and send to store
2. reducers
    Define how application's state change when actions send data to store
3. components
    Small, separated module to display data in UI
4. store
    Hold all the state of the application
5. constants
6. containers
7. util
    API Request Util