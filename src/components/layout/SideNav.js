import React, {Component} from 'react'
import {connect} from 'react-redux'
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import Sidebar from 'react-bootstrap-sidebar';
import {Navbar, Nav, NavItem, Button} from 'react-bootstrap';
const cookies = new Cookies();
class SideNav extends Component {

    constructor(props) {
        super(props);
 
        this.state = {
          isVisible: false,
        };
    }

    componentDidMount = async(e) => {
        let role = cookies.get("role");
        if(role === "salon") {
            let salonSideNavs = document.getElementsByClassName("salon-side-nav");
            for(let i = 0; i < salonSideNavs.length; i++) {
                salonSideNavs[i].style.display = "block";
            } 
            let adminSideNavs = document.getElementsByClassName("admin-side-nav");
            for(let i = 0; i < adminSideNavs.length; i++) {
                adminSideNavs[i].style.display = "none";
            }
        } else if(role === "admin") {
            let salonSideNavs = document.getElementsByClassName("salon-side-nav");
            for(let i = 0; i < salonSideNavs.length; i++) {
                salonSideNavs[i].style.display = "none";
            } 
            let adminSideNavs = document.getElementsByClassName("admin-side-nav");
            for(let i = 0; i < adminSideNavs.length; i++) {
                adminSideNavs[i].style.display = "block";
            }
        }

        let sideNavs = document.getElementsByClassName("side-nav-item");
        if(window.location.href.includes("setting")){
            for(let i = 0; i < sideNavs.length; i++){
                sideNavs[i].classList.remove("nav-active");
            }
            document.getElementById("salon-setting").classList.add("nav-active");
        } else if(window.location.href.includes("salon/booking")) {
            for(let i = 0; i < sideNavs.length; i++){
                sideNavs[i].classList.remove("nav-active");
            }
            document.getElementById("salon-booking").classList.add("nav-active");
        } else if(window.location.href.includes("admin/salon")) {
            for(let i = 0; i < sideNavs.length; i++){
                sideNavs[i].classList.remove("nav-active");
            }
            document.getElementById("admin-salon").classList.add("nav-active");
        } else if(window.location.href.includes("admin/customer")){
            for(let i = 0; i < sideNavs.length; i++){
                sideNavs[i].classList.remove("nav-active");
            }
            document.getElementById("admin-customer").classList.add("nav-active");
        } else {
            for(let i = 0; i < sideNavs.length; i++){
                sideNavs[i].classList.remove("nav-active");
            }
            document.getElementById("admin-booking").classList.add("nav-active");
        }
    }
    handleBookingClick = (e) => {
        window.location.href = `/salon/booking`;
    }
    handleSettingClick = (e) => {
        window.location.href = `/salon/setting`;
    }
    handleAdminSalonClick = (e) => {
        window.location.href = `/admin/salon`;
    }
    handleAdminCustomerClick = (e) => {
        window.location.href = `/admin/customer`;
    }
    handleAdminBookingClick = (e) => {
        window.location.href = `/admin/booking`;
    }
    handleLogOutClick = (e) => {
        cookies.remove('token', { path: '/' });
        cookies.remove('role', { path: '/' });
        cookies.remove('phoneNumber', { path: '/' });
        cookies.remove('username', { path: '/' });
        window.location.href = `/salon/login`;
    }
    closeNav = () => {
        document.getElementById("mySidenav").style.width = "0";
    }
    openNav = () => {
        document.getElementById("mySidenav").style.width = "250px";
    }

    updateModal(isVisible) {
    	this.state.isVisible = isVisible;
      this.forceUpdate();
    }

    render() {
        return (
            <div>
                <Button bsStyle="primary" onClick={ () => this.updateModal(true) }><i class="fa fa-bars"></i></Button>
                <Sidebar side='left' isVisible={ this.state.isVisible } onHide={ () => this.updateModal(false) }>
                <Nav>
                    <NavItem href="#">Link 1</NavItem>
                    <NavItem href="#">Link 2</NavItem>
                    <NavItem href="#">Link 3</NavItem>
                    <NavItem href="#">Link 4</NavItem>
                    <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
                    <NavLink className="side-nav-item salon-side-nav" id="salon-booking" data-target="#link1" to={{pathname: '/salon/booking'}}>Lịch đặt</NavLink>
                    <NavLink className="side-nav-item salon-side-nav" id="salon-setting" data-target="#link1" to={{pathname: '/salon/setting'}}>Cài đặt lịch làm việc</NavLink>
                    <a className="side-nav-item admin-side-nav" id="admin-salon" onClick={this.handleAdminSalonClick}>Quản lý tiệm</a>
                    <a className="side-nav-item admin-side-nav" id="admin-customer" onClick={this.handleAdminCustomerClick}>Quản lý khách hàng</a>
                    <a className="side-nav-item admin-side-nav" id="admin-booking" onClick={this.handleAdminBookingClick}>Quản lý đặt lịch</a>
                    <a className="side-nav-item logout" onClick={this.handleLogOutClick}>Đăng xuất</a>
                </Nav>
                </Sidebar>
            </div>
        )
    }
}

export default connect(null, null)(SideNav)
