import {CREATING_PIN_CODE_SUCCESS, VERIFYING_PIN_CODE_SUCCESS, GETTING_CUSTOMER_BY_PHONE_SUCCESS} from '../actions/customer';
import {REGISTER_USER_SUCCESS, LOGIN_USER_SUCCESS, GET_USER_SUCCESS, UPDATE_USER_SUCCESS} from '../actions/user';
const initialState = {
    smsResponse: [],
    verifyResponse: [],
    customer: [],
    getCustomer: [],
    user: [],
    loginResult: [],
    updateUser: [],
}

export default function authenticationReducer(state = initialState, action) {
    switch (action.type) {
        case CREATING_PIN_CODE_SUCCESS:
            return {
                ...state,
                smsResponse: JSON.parse(action.data),
            }
        case VERIFYING_PIN_CODE_SUCCESS:
            return {
                ...state,
                verifyResponse: JSON.parse(action.data),
            }
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                user: JSON.parse(action.data),
            }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                loginResult: JSON.parse(action.data),
            }
        case GET_USER_SUCCESS:
            return {
                ...state,
                user: JSON.parse(action.data),
            }
        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                updateUser: JSON.parse(action.data),
            }
        case GETTING_CUSTOMER_BY_PHONE_SUCCESS:
        return {
                ...state,
                getCustomer: JSON.parse(action.data),
            }
        default:
            return state;
    }
}
