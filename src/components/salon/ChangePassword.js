import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import { getSalonByPhoneNumber, updateSalon } from '../../actions/salon';
import Cookies from 'universal-cookie';
import { toast, ToastContainer } from '../../../node_modules/mdbreact';

class ChangePasswordPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            salon: []
        };

    }
    componentWillMount = async(e) => {
        // let verified = cookies.get("verified");
        // let phoneNumber = cookies.get("phoneNumber");
        // if(verified) {
            await this.props.handleGetSalonByPhoneNumber("0989284470");
            if(this.props.salonReducer.salon) {
                this.setState({
                    salon: this.props.salonReducer.salon[0]
                });
            }
        // } else {
        //     window.location.href = "/salon/login";
        // }
    }

    handleSavePassword = async(event) => {
        event.preventDefault();
        let salon = this.state.salon;
        let password = event.target.password.value;
        let confirmPassword = event.target.confirmPassword.value;
        if(password === confirmPassword) {
            salon.password = password;
            await this.props.handleSaveSalon(salon);
            if(this.props.salonReducer.salon) {
                toast.success("Cập nhật mật khẩu thành công. Quay trở lại đăng nhập.");
                window.location.href = "/salon/login";
            } else {
                toast.error("Cập nhật mật khẩu không thành công.")
            }
        } else {
            toast.error("Mật khẩu không trùng nhau.")
        }
        
    }
    handleBack = () => {
        window.location.href = "/salon/login"
    }

    render() {
        return (
            <div>
                <Header/>
                <section id="login">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Thay đổi mật khẩu</h3>
                            </div>
                        </div>
                        <form className="salon-login-form" onSubmit={this.handleSavePassword}>
                            <div className="form-group">
                                <label htmlFor="password">Mật khẩu mới *</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    pattern=".{8,}" 
                                    title="Mật khẩu phải nhiều hơn 8 ký tự"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền mật khẩu
                                </div>
                            </div>
                           
                            <div className="form-group">
                                <label htmlFor="confirmPassword">Xác nhận Mật khẩu *</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    pattern=".{8,}" 
                                    title="Mật khẩu phải nhiều hơn 8 ký tự"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền mật khẩu
                                </div>
                            </div>
                            <div className="row action-button">
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-salon-continue"
                                    >Tiếp tục</button>
                            </div>
                        </form>
                        <div className="row text-center">
                            <p className="text-center">Quay trở lại 
                                <b> </b><a className="login-text" onClick={this.handleBack}>Đăng nhập</a>
                            </p>
                        </div>
                    </div>
                    <ToastContainer hideProgressBar={true} newestOnTop={true} autoClose={5000}/>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {salonReducer: state.salonReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleGetSalonByPhoneNumber: async(phoneNumber) => {
            await dispatch(getSalonByPhoneNumber(phoneNumber))
        },
        handleSaveSalon: async(salon) => {
            await dispatch(updateSalon(salon))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordPage)