import * as urls from '../constants/RestAPIEndpoints'
export const SELECT_SALON = 'SELECT_SALON';
export const REQUEST_POSTS = 'REQUEST_POSTS';
export const RECEIVE_POSTS = 'RECEIVE_POSTS';

export const getSalon = salon => ({
  type: SELECT_SALON,
  salon,
});

export const requestPosts = () => ({
  type: REQUEST_POSTS,
});

export const receivedPosts = json => ({
  type: RECEIVE_POSTS,
  json: json,
});

export function fetchPosts(salon) {
  return function (dispatch) {
    dispatch(requestPosts());
    return fetch(urls.GET_ALL_BOOKINGS + `?salonId.equals=${salon}`)
      .then(
      response => response.json(),
      error => console.log('An error occurred.', error),
    )
      .then((json) => {
        dispatch(receivedPosts(json));
      },
    );
  };
}
