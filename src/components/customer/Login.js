import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from '../layout/Header'
import { ToastContainer, toast } from 'mdbreact';
import Cookies from 'universal-cookie';
import { NavLink } from 'react-router-dom';
import SocialButton from '../layout/SocialButton'
import { loginUser, getUserById } from '../../actions/user';
const cookies = new Cookies();

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginResult: [],
            loginSuccess: true
        };
    }
    componentWillMount = async(e) => {
        if(cookies.get("signedIn")) {
            window.location.href = "/";
        }
    }

    handleLogin = async(event) => {
        event.preventDefault();
        let username = event.target.username.value;
        let credentials = {};
        credentials.password = event.target.password.value;
        if(validateEmail(username)) {
            credentials.email = username;
        } else {
            credentials.username = username;
        }

        await this.props.handleUserLogin(credentials);
        if(this.props.authenticationReducer.loginResult.length !== 0){
            await this.props.handleGetUserById(this.props.authenticationReducer.loginResult.userId, this.props.authenticationReducer.loginResult.id);
            cookies.set('token', this.props.authenticationReducer.loginResult.id, {
                maxAge: 24 * 3600,
                path: '/'
            });
            cookies.set('role', "customer", {
                maxAge: 24 * 3600,
                path: '/'
            });

            cookies.set('username', this.props.authenticationReducer.user.username, {
                maxAge: 24 * 3600,
                path: '/'
            });

            cookies.set('userId', this.props.authenticationReducer.loginResult.userId, {
                maxAge: 24 * 3600,
                path: '/'
            });
            
            cookies.set('signedIn', true, {
                maxAge: 24 * 3600,
                path: '/'
            });
            let previousPath = cookies.get("previousPath");
            if(!previousPath) {
                previousPath = "/";
            }
            window.location.href = `${previousPath}`;
            
        } else {
            console.clear();
            this.setState({
                loginSuccess: false
            });
        }
    }
    handleSocialLogin = async(user) => {
        console.log(user);
        
        cookies.set('signedIn', true, {
            maxAge: 24 * 3600,
            path: '/'
        });
    }
      
    handleSocialLoginFailure = async(err) => {
        console.error(err)
    }

    render() {
        const {loginSuccess} = this.state
        return (
            <div>
                <Header/>
                <section id="login">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h3 className="section-heading">Đăng nhập</h3>
                            </div>
                        </div>
                        <form className="salon-login-form" onSubmit={this.handleLogin}>
                            <div className="form-group customer-register">
                                <label htmlFor="username">Tài khoản/Email *</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="username"
                                    name="username"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền tài khoản
                                </div>
                            </div>
                           
                            <div className="form-group">
                                <label htmlFor="password">Mật khẩu *</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    required/>
                                <div className="invalid-feedback">
                                Vui lòng điền mật khẩu
                                </div>
                            </div>
                            
                            
                            <div className={loginSuccess? "alert alert-danger invisible":"alert alert-danger"}>
                                Số điện thoại hoặc mật khẩu của bạn không đúng.
                            </div>
                            
                            
                            <div className="row action-button">
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-salon-continue"
                                    >Tiếp tục</button>
                            </div>
                        </form>
                        <div className="row social-login-buttons">
                                <SocialButton
                                className="facebook-login"
                                provider='facebook'
                                appId='299958884238381'
                                onLoginSuccess={this.handleSocialLogin}
                                onLoginFailure={this.handleSocialLoginFailure}
                                >
                                Đăng nhập với Facebook
                                </SocialButton>
                        </div>
                        
                        <div className="row text-center">
                            <p className="text-center">Chưa có tài khoản? 
                                <b> </b><NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/register'}} activeClassName="">Đăng ký ngay</NavLink>
                            </p>
                        </div>
                        <div className="row text-center">
                            <p className="text-center">
                            <NavLink className="login-text" data-target="#link1" to={{pathname: '/salon/forget-password'}} activeClassName="">Quên mật khẩu</NavLink>
                            </p>
                        </div>
                    </div>
                    <ToastContainer
                    hideProgressBar={true}
                    newestOnTop={true}
                    autoClose={5000}
                    />
                </section>
            </div>
        );
    }
    displayCities = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <a
                    className="dropdown-item"
                    onClick={() => this.handleChooseCity(element.id, element.name)}
                    key={index}>{element.name}</a>
            ))
        }
        return result
    }
    displayDistricts = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <a
                    className="dropdown-item"
                    onClick={() => this.handleChooseDistrict(element.id, element.cityId, element.name)}
                    key={index}>{element.name}</a>
            ))
        }
        return result
    }
    displaySalons = (arr) => {
        let result = null
        if (arr.length > 0) {
            result = arr.map((element, index) => (
                <div
                    className="salon-item"
                    onClick={() => this.handleChooseSalon(element.id)}
                    key={index}>
                    <p className="salon-name">{element.name}</p>
                    <p className="salon-address">{element.address}</p>
                </div>
            ))
        }
        return result
    }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function mapStateToProps(state) {
    return {authenticationReducer: state.authenticationReducer}
}

function mapDispatchToProps(dispatch) {
    return {
        handleUserLogin: async(credentials) => {
            await dispatch(loginUser(credentials))
        },
        handleGetUserById: async(id, token) => {
            await dispatch(getUserById(id, token))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)