import {
    GET_DISTRICT_BY_ID,
    GET_DISTRICT_BY_ID_SUCCESS,
    GET_DISTRICTS,
    GET_DISTRICTS_SUCCESS,
    GET_DISTRICTS_HAS_SALON_SUCCESS,
    GET_DISTRICTS_HAS_SALON
} from '../actions/district';

const initialState = {
    allDistricts: [],
    allDistrictsHasSalon: [],
    district: [],
    isFetching: false
};

export default function districtReducer(state = initialState, action) {
    switch (action.type) {
        case GET_DISTRICT_BY_ID:
            return {
                ...state,
                isFetching: false
            }
        case GET_DISTRICT_BY_ID_SUCCESS:
            return {
                ...state,
                district: JSON.parse(action.data),
                isFetching: false
            }
        case GET_DISTRICTS:
            return {
                ...state,
                isFetching: false
            }
        case GET_DISTRICTS_SUCCESS:
            return {
                ...state,
                allDistricts: JSON.parse(action.data),
                isFetching: false
            }
        case GET_DISTRICTS_HAS_SALON:
            return {
                ...state,
                isFetching: false
            }
        case GET_DISTRICTS_HAS_SALON_SUCCESS:
            return {
                ...state,
                allDistrictsHasSalon: JSON.parse(action.data),
                isFetching: false
            }
        default:
            return state;
    }
}